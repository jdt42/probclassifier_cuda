# probclassifier_cuda
 
A CUDA based classifier. The associated publication to this code can be 
found here: [DOI:10.1186/1758-2946-6-29](http://dx.doi.org/10.1186/1758-2946-6-29)

# Quick Start

This will compile and run unit tests for probclassifier_cuda.

## [Ubuntu Precise (12.04.4)](http://releases.ubuntu.com/precise/)
Preamble : Install the CUDA drivers/libs from the NVIDIA repository

    $ cd /tmp
    $ wget http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1204/x86_64/cuda-repo-ubuntu1204_5.5-0_amd64.deb
    $ sudo dpkg -i cuda-repo-ubuntu1204_5.5-0_amd64.deb
    $ sudo apt-get update
    $ sudo apt-get install cuda

Note, a reboot may be required after this step

    # Ensure you have what is needed to build the code
    $ sudo apt-get install git cmake g++ doxygen libboost-dev

    # Clone the repository
    $ git clone https://bitbucket.org/jdt42/probclassifier_cuda.git

    # Create directories for the out-of-source build
    $ mkdir probclassifier_cuda_build ; cd probclassifier_cuda_build

    # Build, compile and test
    $ cmake ../probclassifier_cuda ; make ; make test

A template RunSpecification file with descriptions of the input variables is provided in RunSpecification_template.

The data files should be in the format: itemId, classId, feature1, feature2, ...