// See http://code.google.com/p/googletest/wiki/Primer#Writing_the_main%28%29_Function
#include <cuda.h>
#include <cuda_runtime.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include "gtest/gtest.h"
#include "DataHolder.hpp"
#include "ResultsHolder.hpp"
#include "ProbClassifierManager.hpp"
#include "printCudaErrorCode.hpp"
#include "TestConfig.hpp"

using namespace std;

namespace {

// The fixture for testing class Foo.
class NbSerialClassifierTest: public ::testing::Test {
protected:
	// You can remove any or all of the following functions if its body
	// is empty.
	static const double abs_error = 0.00001;
	map<string, ResultsHolder> resultsHolderMap;
	ResultsHolder nbResults;
	typedef std::map<std::string, ResultsHolder>::iterator it_type;

	NbSerialClassifierTest() {
		// You can do set-up work for each test here.
		DataHolder dataHolder("empty");
		dataHolder.testCase = true;
		dataHolder.verbose = 1;
		dataHolder.runNb = true;

		dataHolder.classifierMethod = 0;
		dataHolder.implementation = 0;
		dataHolder.samplingMethod = 0;

		dataHolder.q_min = 2;
		dataHolder.q_max = 2;
		dataHolder.q_step = 0;

		dataHolder.j_min = 6;
		dataHolder.j_max = 6;
		dataHolder.j_step = 0;

		dataHolder.dataPath = (std::string(DATADIR)
				+ std::string("/BalloonFormatted.dat")).c_str();
		dataHolder.importData();

		ProbClassifierManager probClassifierManager(dataHolder);
		probClassifierManager.runClassifier();

		resultsHolderMap = probClassifierManager.resultsHolderMap;

		it_type mapIter;
		mapIter = resultsHolderMap.find("nb");
		nbResults = (*mapIter).second;
	}

	virtual ~NbSerialClassifierTest() {
		// You can do clean-up work that doesn't throw exceptions here.
	}

	// If the constructor and destructor are not enough for setting up
	// and cleaning up each test, you can define the following methods:

	virtual void SetUp() {
		// Code here will be called immediately after the constructor (right
		// before each test).
	}

	virtual void TearDown() {
		// Code here will be called immediately after each test (right
		// before the destructor).
	}

	// Objects declared here can be used by all tests in the test case for Foo.

};

TEST_F(NbSerialClassifierTest, TestNb) {
	ASSERT_NEAR(nbResults.classProbBest[0], 0, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[1], 1, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[2], 0.5, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[3], 0.5, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[4], 0.5, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[5], 0.5, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[6], 0.833333, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[7], 0.166666, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[8], 0.5, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[9], 0.5, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[10], 0.166666, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[11], 0.833333, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[12], 0.166666, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[13], 0.833333, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[14], 0.5, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[15], 0.5, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[16], 0.5, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[17], 0.5, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[18], 0.166666, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[19], 0.833333, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[20], 0.166666, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[21], 0.833333, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[22], 0.5, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[23], 0.5, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[24], 0.833333, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[25], 0.166666, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[26], 0.5, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[27], 0.5, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[28], 0.5, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[29], 0.5, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[30], 1, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[31], 0, abs_error);

}

}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
