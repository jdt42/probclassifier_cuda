// See http://code.google.com/p/googletest/wiki/Primer#Writing_the_main%28%29_Function
#include <cuda.h>
#include <cuda_runtime.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include "gtest/gtest.h"
#include "DataHolder.hpp"
#include "Classifier.hpp"
#include "ResultsHolder.hpp"
#include "ProbClassifierManager.hpp"
#include "printCudaErrorCode.hpp"
#include "TestConfig.hpp"
#include <CL/cl.h>

using namespace std;

namespace {

// The fixture for testing class Foo.
class OpenClTest: public ::testing::Test {
protected:
	// You can remove any or all of the following functions if its body
	// is empty.
	static const double abs_error = 0.00001;

	OpenClTest() {
		// You can do set-up work for each test here.
		cl_platform_id platform_id = NULL;
		cl_uint ret_num_platforms;
		cl_int ret;
		ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);
		cout << "JT: " << ret << " ***** " << endl;


	}

	virtual ~OpenClTest() {
		// You can do clean-up work that doesn't throw exceptions here.
	}

	// If the constructor and destructor are not enough for setting up
	// and cleaning up each test, you can define the following methods:

	virtual void SetUp() {
		// Code here will be called immediately after the constructor (right
		// before each test).
	}

	virtual void TearDown() {
		// Code here will be called immediately after each test (right
		// before the destructor).
	}

	// Objects declared here can be used by all tests in the test case for Foo.

};

TEST_F(OpenClTest, TestNb) {
	ASSERT_NEAR(0, 0, abs_error);


}

}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
