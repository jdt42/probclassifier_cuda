// See http://code.google.com/p/googletest/wiki/Primer#Writing_the_main%28%29_Function
#include <cuda.h>
#include <cuda_runtime.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include "gtest/gtest.h"
#include "DataHolder.hpp"
#include "ResultsHolder.hpp"
#include "ProbClassifierManager.hpp"
#include "printCudaErrorCode.hpp"
#include "TestConfig.hpp"

using namespace std;

namespace {

// The fixture for testing class Foo.
class ScenarioTest2C9: public ::testing::Test {
protected:
	// You can remove any or all of the following functions if its body
	// is empty.
	static const double abs_error = 0.00001;
	map<string, ResultsHolder> resultsHolderMap;
	ResultsHolder nbResults;
	ResultsHolder pcrasdResults;
	ResultsHolder pzwResults;
	typedef std::map<std::string, ResultsHolder>::iterator it_type;

	ScenarioTest2C9() {
		// You can do set-up work for each test here.
		DataHolder dataHolder("empty");
		dataHolder.testCase = true;
		dataHolder.verbose = 0;
		dataHolder.implementation = 0;
		dataHolder.runNb = false;
		dataHolder.runRascal = false;
		dataHolder.runPrw = false;
		dataHolder.prwParam = 0.2;

		dataHolder.classifierMethod = 0;

		dataHolder.samplingMethod = 1;
		dataHolder.kfoldNum = 10;
		dataHolder.usePredefinedKfoldMember=true;

		dataHolder.q_min = 40;
		dataHolder.q_max = 40;
		dataHolder.q_step = 0;

		dataHolder.j_min = 201;
		dataHolder.j_max = 201;
		dataHolder.j_step = 0;

		dataHolder.dataPath = (std::string(DATADIR)
				+ std::string("/2C9_full_4.dat")).c_str();
		dataHolder.dataTrain.kfoldSplitPath=(std::string(DATADIR)
				+ std::string("/2C9_kfoldSplit")).c_str();

		dataHolder.importData();

		ProbClassifierManager probClassifierManager(dataHolder);
		probClassifierManager.runClassifier();

		resultsHolderMap = probClassifierManager.resultsHolderMap;

		it_type mapIter;
		mapIter = resultsHolderMap.find("nb");
		nbResults = (*mapIter).second;

		mapIter = resultsHolderMap.find("pzw");
		pzwResults = (*mapIter).second;

		mapIter = resultsHolderMap.find("pcrasd");
		pcrasdResults = (*mapIter).second;
		//cout << "pcrasd MCC: " << pcrasdResults.mccBest << endl;
		//cout << "pzw MCC: " << pzwResults.mccBest << endl;
		//cout << "nb MCC: " << nbResults.mccBest << endl;
	}

	virtual ~ScenarioTest2C9() {
		// You can do clean-up work that doesn't throw exceptions here.
	}

	// If the constructor and destructor are not enough for setting up
	// and cleaning up each test, you can define the following methods:

	virtual void SetUp() {
		// Code here will be called immediately after the constructor (right
		// before each test).
	}

	virtual void TearDown() {
		// Code here will be called immediately after each test (right
		// before the destructor).
	}

	// Objects declared here can be used by all tests in the test case for Foo.

};

TEST_F(ScenarioTest2C9, TestOne) {
	EXPECT_EQ(1, 1);
}

}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
