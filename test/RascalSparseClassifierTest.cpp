// See http://code.google.com/p/googletest/wiki/Primer#Writing_the_main%28%29_Function
#include <cuda.h>
#include <cuda_runtime.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include "gtest/gtest.h"
#include "DataHolder.hpp"
#include "ResultsHolder.hpp"
#include "ProbClassifierManager.hpp"
#include "TestConfig.hpp"

using namespace std;

namespace {

// The fixture for testing class Foo.
class RascalSparseClassifierTest: public ::testing::Test {
protected:
	// You can remove any or all of the following functions if its body
	// is empty.
	static const double abs_error = 0.00001;
	map<string, ResultsHolder> resultsHolderMap;
	ResultsHolder rascalSparseResults;

	typedef std::map<std::string, ResultsHolder>::iterator it_type;

	RascalSparseClassifierTest() {
		// You can do set-up work for each test here.
		DataHolder dataHolder("empty");
		dataHolder.testCase = true;
		dataHolder.verbose = 1;
		dataHolder.runRascal = true;
		dataHolder.implementation = 1;
		dataHolder.classifierMethod = 0;
		dataHolder.dataFormat = 1;
		dataHolder.dataTrain.dataFormat = 1;
		dataHolder.dataTest.dataFormat = 1;

		dataHolder.samplingMethod = 0;

		dataHolder.q_min = 2;
		dataHolder.q_max = 2;
		dataHolder.q_step = 0;

		dataHolder.j_min = 6;
		dataHolder.j_max = 6;
		dataHolder.j_step = 0;

		dataHolder.dataPath = (std::string(DATADIR)
				+ std::string("/BalloonFormattedSparse.dat")).c_str();
		dataHolder.importData();

		ProbClassifierManager probClassifierManager(dataHolder);
		probClassifierManager.runClassifier();

		resultsHolderMap = probClassifierManager.resultsHolderMap;

		it_type mapIter;

		mapIter = resultsHolderMap.find("rascal");
		rascalSparseResults = (*mapIter).second;

	}

	virtual ~RascalSparseClassifierTest() {
		// You can do clean-up work that doesn't throw exceptions here.
	}

	// If the constructor and destructor are not enough for setting up
	// and cleaning up each test, you can define the following methods:

	virtual void SetUp() {
		// Code here will be called immediately after the constructor (right
		// before each test).
	}

	virtual void TearDown() {
		// Code here will be called immediately after each test (right
		// before the destructor).
	}

	// Objects declared here can be used by all tests in the test case for Foo.

};

TEST_F(RascalSparseClassifierTest, TestRascalSparse) {

	ASSERT_NEAR(rascalSparseResults.classProbBest[0], 0, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[1], 1, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[2], 0.5, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[3], 0.5, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[4], 0.5, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[5], 0.5, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[6], 0.833333, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[7], 0.166666, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[8], 0.5, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[9], 0.5, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[10], 0.5, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[11], 0.5, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[12], 0.5, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[13], 0.5, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[14], 0.666666, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[15], 0.333333, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[16], 0.5, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[17], 0.5, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[18], 0.5, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[19], 0.5, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[20], 0.5, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[21], 0.5, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[22], 0.666666, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[23], 0.333333, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[24], 0.833333, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[25], 0.166666, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[26], 0.666666, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[27], 0.333333, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[28], 0.666666, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[29], 0.333333, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[30], 1, abs_error);
	ASSERT_NEAR(rascalSparseResults.classProbBest[31], 0, abs_error);

}

}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
