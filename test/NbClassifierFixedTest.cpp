// See http://code.google.com/p/googletest/wiki/Primer#Writing_the_main%28%29_Function
#include <cuda.h>
#include <cuda_runtime.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include "gtest/gtest.h"
#include "DataHolder.hpp"
#include "ResultsHolder.hpp"
#include "ProbClassifierManager.hpp"
#include "printCudaErrorCode.hpp"
#include "TestConfig.hpp"

using namespace std;

namespace {

// The fixture for testing class Foo.
class NbSerialClassifierFixedTest: public ::testing::Test {
protected:
	// You can remove any or all of the following functions if its body
	// is empty.
	static const double abs_error = 0.00001;
	map<string, ResultsHolder> resultsHolderMap;
	ResultsHolder nbResults;
	typedef std::map<std::string, ResultsHolder>::iterator it_type;

	NbSerialClassifierFixedTest() {
		// You can do set-up work for each test here.
		DataHolder dataHolder("empty");
		dataHolder.testCase = true;
		dataHolder.verbose = 1;
		dataHolder.runNb = true;

		dataHolder.classifierMethod = 0;
		dataHolder.implementation = 1;
		dataHolder.samplingMethod = 2;

		dataHolder.q_min = 2;
		dataHolder.q_max = 2;
		dataHolder.q_step = 0;

		dataHolder.j_min = 6;
		dataHolder.j_max = 6;
		dataHolder.j_step = 0;

		dataHolder.dataPath = (std::string(DATADIR) + std::string("/BalloonFormatted_tr.dat")).c_str();
		dataHolder.testPath = (std::string(DATADIR) + std::string("/BalloonFormatted_ts.dat")).c_str();
		dataHolder.importData();

		ProbClassifierManager probClassifierManager(dataHolder);
		probClassifierManager.runClassifier();

		resultsHolderMap = probClassifierManager.resultsHolderMap;

		it_type mapIter;
		mapIter = resultsHolderMap.find("nb");
		nbResults = (*mapIter).second;
	}

	virtual ~NbSerialClassifierFixedTest() {
		// You can do clean-up work that doesn't throw exceptions here.
	}

	// If the constructor and destructor are not enough for setting up
	// and cleaning up each test, you can define the following methods:

	virtual void SetUp() {
		// Code here will be called immediately after the constructor (right
		// before each test).
	}

	virtual void TearDown() {
		// Code here will be called immediately after each test (right
		// before the destructor).
	}

	// Objects declared here can be used by all tests in the test case for Foo.

};

TEST_F(NbSerialClassifierFixedTest, TestNb) {
	ASSERT_NEAR(nbResults.classProbBest[0], 0.83333, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[1], 0.16666, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[2], 0.83333, abs_error);
	ASSERT_NEAR(nbResults.classProbBest[3], 0.16666, abs_error);

}

}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
