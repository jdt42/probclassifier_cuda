// See http://code.google.com/p/googletest/wiki/Primer#Writing_the_main%28%29_Function
#include <cuda.h>
#include <cuda_runtime.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include "gtest/gtest.h"
#include "DataHolder.hpp"
#include "ResultsHolder.hpp"
#include "ProbClassifierManager.hpp"
#include "TestConfig.hpp"

using namespace std;

namespace {

// The fixture for testing class Foo.
class PrwClassifierFixedTest: public ::testing::Test {
protected:
	// You can remove any or all of the following functions if its body
	// is empty.
	static const double abs_error = 0.00001;
	map<string, ResultsHolder> resultsHolderMap;
	ResultsHolder prwResults;

	typedef std::map<std::string, ResultsHolder>::iterator it_type;

	PrwClassifierFixedTest() {
		// You can do set-up work for each test here.
		DataHolder dataHolder("empty");
		dataHolder.testCase = true;
		dataHolder.verbose = 0;
		dataHolder.runPrw = true;
		dataHolder.prwParam = 0.4;
		dataHolder.classifierMethod = 0;
		dataHolder.implementation = 1;
		dataHolder.dataFormat = 0;
		dataHolder.samplingMethod = 2;

		dataHolder.q_min = 2;
		dataHolder.q_max = 2;
		dataHolder.q_step = 0;

		dataHolder.j_min = 6;
		dataHolder.j_max = 6;
		dataHolder.j_step = 0;

		dataHolder.dataPath = (std::string(DATADIR) + std::string("/BalloonFormatted_tr.dat")).c_str();
		dataHolder.testPath = (std::string(DATADIR) + std::string("/BalloonFormatted_ts.dat")).c_str();
		dataHolder.importData();

		ProbClassifierManager probClassifierManager(dataHolder);
		probClassifierManager.runClassifier();

		resultsHolderMap = probClassifierManager.resultsHolderMap;

		it_type mapIter;

		mapIter = resultsHolderMap.find("prw");
		prwResults = (*mapIter).second;

	}

	virtual ~PrwClassifierFixedTest() {
		// You can do clean-up work that doesn't throw exceptions here.
	}

	// If the constructor and destructor are not enough for setting up
	// and cleaning up each test, you can define the following methods:

	virtual void SetUp() {
		// Code here will be called immediately after the constructor (right
		// before each test).
	}

	virtual void TearDown() {
		// Code here will be called immediately after each test (right
		// before the destructor).
	}

	// Objects declared here can be used by all tests in the test case for Foo.

};

TEST_F(PrwClassifierFixedTest, TestPrw) {
	ASSERT_NEAR(prwResults.classProbBest[0], 0.5, abs_error);
	ASSERT_NEAR(prwResults.classProbBest[1], 0.5, abs_error);
	ASSERT_NEAR(prwResults.classProbBest[2], 0.66666, abs_error);
	ASSERT_NEAR(prwResults.classProbBest[3], 0.33333, abs_error);

}

}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
