/*
 * DataStructure.cpp
 *
 *  Created on: 29 Nov 2013
 *      Author: jdt42
 */

#include "DataStructure.hpp"
#include <stdio.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <vector>
#include <set>
#include <list>
#include <map>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <algorithm>


using namespace std;
using namespace boost;

void DataStructure::createkfoldMember(int samplingMethod, int kfoldNum, bool usePredefinedKfoldMember) {

	h_kfoldMember = (int*) malloc(CLASS_ARRAY_BYTES);

	if(samplingMethod==2){
		if(trainOrTest==0){
			for(int i=0; i<N; i++){
				h_kfoldMember[i]=0;
			}
		}else if(trainOrTest==1){
			for(int i=0; i<N; i++){
				h_kfoldMember[i]=1;
			}
		}
	}else{
		for(int i=0; i<N; i++){
			h_kfoldMember[i]=0;
		}

		if (samplingMethod==1) {
			if (usePredefinedKfoldMember==true) {
				readKfoldDataMember();

				int itemCount = 0;
				std::map<string, int>::iterator mapIter;
				for (vector<string>::iterator it = h_itemIdVector.begin();
						it != h_itemIdVector.end(); it++) {
					string itemId = *it;

					mapIter = h_kfoldMemberMap.find(itemId);
					int kfoldRef = (*mapIter).second;

					h_kfoldMember[itemCount]=kfoldRef;
					//cout << "itemId: " << itemId << ", " << "kfoldRef: " << kfoldRef << endl;
					itemCount++;
				}
			} else if (usePredefinedKfoldMember == false) {
				generateKfoldMember(kfoldNum);
			}
		} else if (samplingMethod == 0) {
			for (int i=0; i<N; i++) {
				h_kfoldMember[i] = i;
			}
		}
	}
}

void DataStructure::generateKfoldMember(int kfoldNum) {
	for (std::map<string, std::vector<int> >::iterator it =
			h_classIdMap.begin(); it != h_classIdMap.end(); it++) {
		int kfoldTracker = kfoldNum;

		string classId = (*it).first;
		std::vector<int> positionsForClass = (*it).second;
		cout << classId << ": " << positionsForClass.size() << endl;

		for (int k = 0; k < kfoldNum; k++) {
			srand(time(NULL));
			int dataSize = positionsForClass.size();
			int sampleSize = dataSize / kfoldTracker;
			int sampleCount = 0;

			while (sampleCount < sampleSize) {
				sampleCount++;
				int r = rand() % positionsForClass.size();
				int position = positionsForClass[r];
				h_kfoldMember[position] = k;
				positionsForClass.erase(positionsForClass.begin() + r);

			}
			kfoldTracker--;
		}
	}
}


void DataStructure::readKfoldDataMember() {

	string line;
	ifstream myfile(kfoldSplitPath.c_str());

	if (myfile.is_open()) {

		while (myfile.good()) {
			getline(myfile, line);
			if (strlen(line.c_str()) > 1) {
				//cout <<line << endl;

				istringstream iss(line);
				string kfoldRefString;
				string classId;
				string itemId;

				getline(iss, kfoldRefString, ',');
				getline(iss, classId, ',');
				getline(iss, itemId, ',');

				int kfoldRef = boost::lexical_cast<int>(kfoldRefString);

				//cout << "kfoldRef: " << kfoldRef << endl;
				//cout << "itemId: " << itemId << endl;

				h_kfoldMemberMap.insert(
						std::map<string, int>::value_type(itemId, kfoldRef));

				iss.clear();
			}
		}
	}
	myfile.close();
}

void DataStructure::printKfoldMember(string kfoldMemberPath) {
	ofstream kfoldMemberFile;
	kfoldMemberFile.open(kfoldMemberPath.c_str());

	for (int n=0; n<N; n++) {
		kfoldMemberFile << h_kfoldMember[n] << ","
				<< h_classIdVector[n] << "," << h_itemIdVector[n]	<< endl;
	}

	kfoldMemberFile.close();
}


void DataStructure::generateMinAndMaxFeatureArrays(){

	std::vector<int> inVectorMin;
	std::vector<int> inVectorMax;

	if(dataFormat==0)
	{
		// set Min and Max Vectors to first vector
		for (int i=0; i<L; i++) {
			inVectorMin.push_back(inVector[i]);
			inVectorMax.push_back(inVector[i]);
		}

		//calculate Min and Max Vector by comparing to remaining Vectors
		for (int i=L; i<N*L; i++) {
			int pos = i % L;
			if (inVector[i] < inVectorMin[pos]) {
				inVectorMin[pos] = inVector[i];
			}
			if (inVector[i] > inVectorMax[pos]) {
				inVectorMax[pos] = inVector[i];
			}
		}

		//declare and populate min and max
		h_inMin = (int*) malloc(MIN_MAX_ARRAY_BYTES);
		h_inMax = (int*) malloc(MIN_MAX_ARRAY_BYTES);
		for (int i=0; i<L; i++) {
			h_inMin[i] = inVectorMin[i];
			h_inMax[i] = inVectorMax[i];
		}

	}

}

void DataStructure::generateClassMemberData(){

	h_classMember = (int*) malloc(CLASS_ARRAY_BYTES);
	//map classId strings to classRef ints
	int classRef = 0;
	//for (set<string>::iterator it = data.h_classIdSet.begin();	it != data.h_classIdSet.end(); it++) {
	for (map<string,vector<int> >::iterator it = h_classIdMap.begin();	it != h_classIdMap.end(); it++) {
		string classId = (*it).first;
		h_classIdToRefMap.insert(std::map<string, int>::value_type(classId, classRef));
		h_classIdKeySetVector.push_back(classId);
		int size = (*it).second.size();
		h_classSizeKeySetVector.push_back(size);
		classRef++;
	}

	//generate h_classMember which lists classRef for each dataItem
	std::map<string, int>::iterator mapIter;

	for (uint i=0; i<h_classIdVector.size(); i++) {
		string classId = h_classIdVector[i];
		mapIter = h_classIdToRefMap.find(classId);
		int classRef = (*mapIter).second;
		h_classMember[i] = classRef;
	}
}

void DataStructure::populate_h_in(){
	//declare and populate h_in
	h_in = (int*) malloc(IN_ARRAY_BYTES);
	for (uint i=0; i<inVector.size(); i++) {
		h_in[i] = inVector[i];
	}
}

void DataStructure::populate_h_inSparseVectorBreakPoints(){
	if(dataFormat==1){
		h_inSparseVectorBreakPoints = (int*) malloc(SPARSE_ARRAY_BYTES);
		for(int i=0; i<(N+1); i++)	{
			h_inSparseVectorBreakPoints[i]=inSparseVectorBreakPoints[i];
		}
	}
}

void DataStructure::analyseDataLine(string& line){

	istringstream iss(line);
	std::vector<int> featureVector;
	string token;

	int tokenCounter = 0;
	int currentVectorLength = 0;

	while (getline(iss, token, ',')) {

		if (tokenCounter == 0) {
			h_itemIdVector.push_back(token);
			//} else if (tokenCounter==1 && (data.trainOrTest==0 || (data.trainOrTest==1 && data.classesKnown))) {
		} else if (tokenCounter==1){
			h_classIdVector.push_back(token);
			h_classIdSet.insert(token);
			//if new classId detected add empty vector to map
			if (h_classIdMap.find(token) == h_classIdMap.end()) {
				std::vector<int> itemRefsForClass;
				h_classIdMap.insert(std::map<string, std::vector<int> >::value_type(
					token, itemRefsForClass));
				//cout << "found new class: " << token << endl;
			}
			//add a ref to classIdMap
			h_classIdMap[token].push_back(N);

		}else {
			//cout <<"JT *** " << token << endl;
			trim(token);
			int tokenValue = boost::lexical_cast<int>(token);
			//for sparse implementation the number of features is the highest feature number
			//insert into intermediate vector as need to sort
			if(dataFormat==1){
				featureVector.push_back(tokenValue);
				L=max(L,tokenValue+1);
			}else{
				inVector.push_back(tokenValue);
			}
			// count the number of features
			currentVectorLength++;
		}
		tokenCounter++;
	}
	iss.clear();

	if(dataFormat==0 && N==1){
		L=currentVectorLength;
	}else if(dataFormat==1){
		sort(featureVector.begin(), featureVector.end());
		for(uint i=0; i<featureVector.size(); i++){
			inVector.push_back(featureVector[i]);
		}
		inSparseVectorBreakPoints.push_back(inSparseVectorBreakPoints[N]+currentVectorLength);
	}
}

void DataStructure::readData(string path) {
	string line;
	inSparseVectorBreakPoints.push_back(0);
	string test = "test";
	ifstream myfile(path.c_str());

	if (myfile.is_open()) {
		while (myfile.good()) {
			getline(myfile, line);
			if(line.length()>=3){
				//cout <<"line: " << line << endl;
				analyseDataLine(line);
				N++;
				//cout << "N: " << data.N << endl;
			}
		}
	}

	myfile.close();
	classes = h_classIdSet.size();
	setConstants();
	populate_h_in();
	populate_h_inSparseVectorBreakPoints();
	generateMinAndMaxFeatureArrays();
	generateClassMemberData();

	inVector.clear();
	inSparseVectorBreakPoints.clear();
}




void DataStructure::setConstants() {
	MIN_MAX_ARRAY_BYTES = L * sizeof(int);
	IN_ARRAY_SIZE = inVector.size();
	IN_ARRAY_BYTES = IN_ARRAY_SIZE * sizeof(int);
	IN_ARRAY_NORMALISED_BYTES = IN_ARRAY_SIZE * sizeof(double);
	CLASS_PROB_ARRAY_BEST_SIZE = N * classes;
	CLASS_PROB_ARRAY_BEST_BYTES = CLASS_PROB_ARRAY_BEST_SIZE * sizeof(float);
	CLASS_ARRAY_BYTES = N * sizeof(int);
	SPARSE_ARRAY_BYTES = (N + 1) * sizeof(int);
	NUM_COMPARES_ARRAY_SIZE = N * classes;
	NUM_COMPARES_ARRAY_BYTES = NUM_COMPARES_ARRAY_SIZE * sizeof(int);
	FEATURE_COUNT_ARRAY_BY_CLASS_SIZE = classes * L;
	FEATURE_COUNT_ARRAY_BY_CLASS_BYTES= FEATURE_COUNT_ARRAY_BY_CLASS_SIZE * sizeof(int);
}




void DataStructure::print_h_feature_count_array(){
	cout << endl;
	cout << "h_feature_count" << endl;
	for (int i = 0; i<classes; i++) {
		for (int j = 0; j<L; j++) {
			cout << h_feature_count_array[i*L + j] << " ";
		}
		cout << endl;
	}
}

void DataStructure::print_h_in() {
	cout << endl;
	cout << "h_in" << endl;
	if(dataFormat==0){
		for (int i=0; i<N; i++) {
			for (int j = 0; j < L; j++) {
				cout << h_in[i*L + j] << " ";
			}
			cout << endl;
		}
	}
	else if(dataFormat==1)	{
		for (int i = 0; i < N; i++) {
			int startPos = h_inSparseVectorBreakPoints[i];
			int endPos = h_inSparseVectorBreakPoints[i+1];
			for(int j=startPos; j<endPos; j++){
				cout << h_in[j] << " ";
			}
			cout << endl;
		}
	}
}

void DataStructure::print_h_inNormalised() {
	cout << endl;
	cout << "h_inNormalised" << endl;
	for (int i = 0; i<N; i++) {
		for (int j = 0; j<L; j++) {
			cout << h_inNormalised[i*L + j] << " ";
		}
		cout << endl;
	}
}

void DataStructure::print_h_numOfComparesByClass() {
	cout << endl;
	cout << "h_numOfComparesByClass" << endl;
	for (int i=0; i<N; i++) {
		cout << "item: " << i << ", class: " << h_classMember[i] << ": ";
		for (int j = 0; j<classes; j++) {
			cout << h_numOfComparesByClass[i * classes + j] << " ";
		}
		cout << endl;
	}
}


DataStructure::DataStructure() {
	// TODO Auto-generated constructor stub
	dataFormat=0;

	classesKnown=true;

	MIN_MAX_ARRAY_BYTES=0;
	IN_ARRAY_SIZE=0;
	IN_ARRAY_SIZE_TEST=0;
	IN_ARRAY_BYTES=0;
	IN_ARRAY_BYTES_TEST=0;
	IN_ARRAY_NORMALISED_BYTES=0;
	IN_ARRAY_NORMALISED_BYTES_TEST=0;
	CLASS_PROB_ARRAY_BEST_SIZE=0;
	CLASS_PROB_ARRAY_BEST_BYTES=0;
	CLASS_ARRAY_BYTES=0;
	SPARSE_ARRAY_BYTES=0;
	NUM_COMPARES_ARRAY_SIZE=0;
	NUM_COMPARES_ARRAY_BYTES=0;

	N=0;
	L=0;

}



DataStructure::~DataStructure() {
	// TODO Auto-generated destructor stub
}

