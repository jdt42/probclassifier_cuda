/*
 * SerialKernels.h
 *
 *  Created on: 2 Sep 2013
 *      Author: jdt42
 */

#include "DataHolder.hpp"
#include "ResultsHolder.hpp"

#ifndef SERIALKERNELS_H_
#define SERIALKERNELS_H_

class SerialKernels {
public:

	double prw_param;
	int N_tr;
	int N_ts;
	int L;
	int classes;
	int batchSize;
	int batchStartPosition;
	int Q;
	int J;
	int J_min;
	int J_step;
	int J_numOfSteps;
	double prw_lookupTable[256];

	/**
	 * Sets the constant for the run;
	 */
	void setConstants(DataHolder&);

	/**
	 * Sets q independent constants
	 */
	void setQindependentConstants(DataHolder&);

	/**
	 * Calculates the number of times each feature vector is compared to data points in each class
	 */
	void calculateComparesByClass(DataHolder&);
	/**
	 * Calculates the number of times a feature has the same value in each of the data classes for each
	 * data point
	 */
	void calculateCommonFeaturesNb(DataHolder&);
	/**
	 * Calculates the scores using nb for each data point for each combination
	 */
	void calculateScoresNb(DataHolder&, ResultsHolder&);
	/**
	 * Weights the score by the number of compares for each class to make the results comparable
	 */
	void scaleScoresForClassSize(DataHolder&, ResultsHolder&);
	/**
	 * Weights the score by the number of compares for each class to make the results comparable
	*/
	void scaleFloatScoresForClassSize(DataHolder&, ResultsHolder&);
	/**
	 * Calculates the votes for each class on a prediction count basis.  I.e. each random subsample
	 * assessed over all other data points contributes one vote.  The voting system is like first past
	 * the post.
	 */
	void calculateClassVotesPredictionCount(DataHolder&, ResultsHolder&);

	/**
	 * Calculates the votes for each class on a prediction count basis.  I.e. each random subsample
	 * assessed over all other data points contributes one vote.  The voting system is like first past
	 * the post.
	 */
	void calculateClassVotesFloatPredictionCount(DataHolder&, ResultsHolder&);

	/**
	 * Calculates the votes for each class on a cummulative score basis.  I.e. each random subsample
	 * assessed over all other data points the ratio of the votes to each class.  The voting system is
	 * like proportional representation.  This method is better where there are many classes as it enables
	 * all classes to be ranked.
	 */
	void calculateClassVotesCummulativeScores(DataHolder&, ResultsHolder&);

	/**
	 * Calculates the votes for each class on a cummulative score basis.  I.e. each random subsample
	 * assessed over all other data points the ratio of the votes to each class.  The voting system is
	 * like proportional representation.  This method is better where there are many classes as it enables
	 * all classes to be ranked.
	 */
	void calculateClassVotesFloatCummulativeScores(DataHolder&, ResultsHolder&);

	/**
	 * Makes the votes cummulative so that the different j step values can be assessed
	 */
	void makeClassVotesCummulative(DataHolder&, ResultsHolder&);
	/**
	 * Makes the votes cummulative so that the different j step values can be assessed
	 */
	void makeClassVotesFloatCummulative(DataHolder&, ResultsHolder&);
	/**
	 * Calculates the probability of each data point belonging to each class as the votes for each class
	 * divided by the total votes.
	 */
	void calculateClassProbabilities(DataHolder&, ResultsHolder&);
	/**
	 * Calculates the probability of each data point belonging to each class as the votes for each class
	 * divided by the total votes.
	 */
	void calculateClassProbabilitiesFloat(DataHolder&, ResultsHolder&);
	/**
	 * Calculates the confusion matrix for each of the j steps sampled
	 */
	void calculateConfusionMatrix(DataHolder&, ResultsHolder&);

	/**
	 * Calculates the scores using prw and rascal for each data point for each combination
	 */
	void calculateScores(DataHolder&, ResultsHolder&);

	void calculateScoresRascalSparse(DataHolder&, ResultsHolder&);

	void calculateScoresRascal(DataHolder&, ResultsHolder&);

	void calculateScoresPrwSparse(DataHolder&, ResultsHolder&);

	void calculateScoresPrw(DataHolder&, ResultsHolder&);

	SerialKernels();
	virtual ~SerialKernels();
};

#endif /* SERIALKERNELS_H_ */
