/*
 * ResultsHolder.h
 *
 *  Created on: 10 Jul 2013
 *      Author: jdt42
 */

#include <string>
#include "DataHolder.hpp"

#ifndef RESULTSHOLDER_H_
#define RESULTSHOLDER_H_

using namespace std;

class ResultsHolder {
public:

	/**
	 * Stores the classifier method, rascal, nb or prw
	 */
	string method;
	/**
	 * Stores the current bet mcc value
	 */
	double mccBest;
	/**
	 * stores the q value for the best classifier
	 */
	double qBest;
	/**
	 * Stores the j value for the best classifier
	 */
	double jBest;
	/**
	 * Pointer to the array storing the probability values by class and j step for each
	 * feature vector for the best classifier.  This is used to print the best classifier results
	 * once all q and j combinations requested by the user have been run.
	 */
	float *classProbBest;

	/**
	 * Host pointer to array containing MCC results for the various scenarios of j values
	 */
	double *h_mcc_results;

	/**
	 * Host pointer to array containing the kernel scores for all N data items across all
	 * classes and feature combinations
	 */
	double *h_scores;

	/**
	* Host pointer to single precision array containing the kernel scores for all N data items across all
	* classes and feature combinations
	*/
	float *h_scores_float;

	/**
	 * Host pointer to array containing the votes for either class for each data item
	 * across all the sub-samples of features.  The total is either first-past-the-post for
	 * prediction count or proportional representation for cummulative scores
	 */
	double *h_class_votes;
	/**
		 * Host pointer to array containing the votes for either class for each data item
		 * across all the sub-samples of features.  The total is either first-past-the-post for
		 * prediction count or proportional representation for cummulative scores
		 */
	float *h_class_votes_float;

	/**
	 * Host pointer to array containing probability of class membership for each data
	 * item based on the ratio of votes for each class
	 */
	float *h_class_prob;

	/**
	 * Host pointer to array containing probability of class membership for each data
	 * item based on the ratio of votes for each class
	 */
	float *h_class_prob_master;

	/**
	 * Host pointer to array detailing the confusion matrix for the various scenarios of
	 * j values
	 */
	int *h_confusion_matrix;







	/**
	* Device pointer to array containing the kernel scores for all N data items across all
	* classes and feature combinations
	*/
	double *d_scores;

	/**
	* Opencl Device pointer to array containing the kernel scores for all N data items across all
	* classes and feature combinations
	*/
	cl_mem d_scores_ocl;

	/**
	* Device pointer to array containing the kernel scores for all N data items across all
	* classes and feature combinations
	*/
	float *d_scores_float;

	/**
	* Opencl Device pointer to array containing the kernel scores for all N data items across all
	* classes and feature combinations
	*/
	cl_mem d_scores_float_ocl;

	/**
	* Device pointer to array containing the votes for either class for each data item
	* across all the sub-samples of features.  The total is either first-past-the-post for
	* prediction count or proportional representation for cummulative scores
	*/
	double *d_class_votes;

	/**
	* Opencl device pointer to array containing the votes for either class for each data item
	* across all the sub-samples of features.  The total is either first-past-the-post for
	* prediction count or proportional representation for cummulative scores
	*/
	cl_mem d_class_votes_ocl;

	/**
	* Device pointer to array containing the votes for either class for each data item
	* across all the sub-samples of features.  The total is either first-past-the-post for
	* prediction count or proportional representation for cummulative scores
	*/
	float *d_class_votes_float;

	/**
	* Opencl device pointer to array containing the votes for either class for each data item
	* across all the sub-samples of features.  The total is either first-past-the-post for
	* prediction count or proportional representation for cummulative scores
	*/
	cl_mem d_class_votes_float_ocl;


	/**
	* Device pointer to array containing probability of class membership for each data
	* item based on the ratio of votes for each class
	*/
	float *d_class_prob;

	/**
	* Opencl device pointer to array containing probability of class membership for each data
	* item based on the ratio of votes for each class
	*/
	cl_mem d_class_prob_ocl;



	/**
	* Device pointer to array detailing the confusion matrix for the various scenarios of
	* j values
	*/
	int *d_confusion_matrix;

	/**
	* Opencl device pointer to array detailing the confusion matrix for the various scenarios of
	* j values
	*/
	cl_mem d_confusion_matrix_ocl;


	void print_h_scores(DataHolder&);
	void print_h_class_votes(DataHolder&);
	void print_h_class_prob(DataHolder&);
	void print_confusion_matrix(DataHolder&);
	void printMccBest(DataHolder&);

	/**
	 * updates the ResultsHolder for this combination of q and j and stored the best scenario
	 */
	void updateResultsHolder(DataHolder&);

	ResultsHolder();
	virtual ~ResultsHolder();
};

#endif /* RESULTSHOLDER_H_ */
