/*
 * CorrelationCalculator.h
 *
 *  Created on: 1 Jul 2013
 *      Author: jdt42
 */

#ifndef CORRELATIONCALCULATOR_H_
#define CORRELATIONCALCULATOR_H_

#include "DataHolder.hpp"
#include "ResultsHolder.hpp"

class CorrelationCalculator {
public:

	void calculateMcc(DataHolder&, ResultsHolder&);
	CorrelationCalculator();
	virtual ~CorrelationCalculator();
};

#endif /* CORRELATIONCALCULATOR_H_ */
