#include <cuda.h>
#include <cuda_runtime.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include "DataHolder.hpp"
#include "ProbClassifierManager.hpp"

using namespace std;

#include <vector>
#include <algorithm>

int main(int argc, char ** argv) {
	// Todo: Fill in this function

	string runSpecPath;
	if(argc==2)
	{
		runSpecPath=argv[1];
	}
	else
	{
		//runSpecPath = "/home/jdt42/probclassifier/RunSpecificationInputFile_cuda";
		runSpecPath = "/home/jontyzack/probclassifier/RunSpecificationInputFile_cuda";
	}

	DataHolder dataHolder(runSpecPath);
	dataHolder.readRunSpecification();
	dataHolder.importData();

	ProbClassifierManager probClassifierManager(dataHolder);
	probClassifierManager.runClassifier();

	return 0;

}

