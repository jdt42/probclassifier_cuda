/*
 * ProbClassifierManager.cpp
 *
 *  Created on: 30 Aug 2013
 *      Author: jdt42
 */

#include <stdio.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include "DataHolder.hpp"
#include "DataStructure.hpp"
#include "FeatureComboGenerator.hpp"
#include "ResultsHolder.hpp"
#include "CorrelationCalculator.hpp"
#include <boost/lexical_cast.hpp>
#include "ProbClassifierManager.hpp"
#include "SerialKernels.hpp"
#include "printCudaErrorCode.hpp"

using namespace std;



void ProbClassifierManager::generateResultsHolder(string name, const int CLASS_PROB_ARRAY_BEST_BYTES) {
	if (dataHolder.verbose != 0) {
		cout << "Creating " << name << " results holder" << endl;
	}
	ResultsHolder resultsHolder;
	resultsHolder.method = name;
	resultsHolder.mccBest = -1.0;
	resultsHolder.qBest = 0;
	resultsHolder.jBest = 0;
	float *class_prob_best;
	class_prob_best = (float*) malloc(CLASS_PROB_ARRAY_BEST_BYTES);
	resultsHolder.classProbBest = class_prob_best;
	resultsHolderMap.insert(std::map<string, ResultsHolder>::value_type(name, resultsHolder));
}

int* ProbClassifierManager::copyIntArrayToGPU(int *h_intArray, const int BYTES) {
	int *d_intArray;
	cudaError_t cudaCode = cudaMalloc((void**) &d_intArray, BYTES);
	if (cudaCode != cudaSuccess) {
		printCudaErrorCode("Error allocating device memory: ", cudaCode);
	}
	cudaError_t cudaCodeCopy = cudaMemcpy(d_intArray, h_intArray, BYTES,
			cudaMemcpyHostToDevice);
	if (cudaCodeCopy != cudaSuccess) {
		printCudaErrorCode("Error copying to device: ", cudaCodeCopy);
	}
	return d_intArray;
}

double* ProbClassifierManager::copyDoubleArrayToGPU(double *h_doubleArray, const int BYTES) {
	double *d_doubleArray;
	cudaError_t cudaCode = cudaMalloc((void**) &d_doubleArray, BYTES);
	if (cudaCode != cudaSuccess) {
		printCudaErrorCode("Error allocating device memory: ", cudaCode);
	}
	cudaError_t cudaCodeCopy = cudaMemcpy(d_doubleArray, h_doubleArray, BYTES,
			cudaMemcpyHostToDevice);
	if (cudaCodeCopy != cudaSuccess) {
		printCudaErrorCode("Error copying to device: ", cudaCodeCopy);
	}
	return d_doubleArray;
}

float* ProbClassifierManager::copyFloatArrayToGPU(float *h_floatArray, const int BYTES) {
	float *d_floatArray;
	cudaError_t cudaCode = cudaMalloc((void**) &d_floatArray, BYTES);
	if (cudaCode != cudaSuccess) {
		printCudaErrorCode("Error allocating device memory: ", cudaCode);
	}
	cudaError_t cudaCodeCopy = cudaMemcpy(d_floatArray, h_floatArray, BYTES,
			cudaMemcpyHostToDevice);
	if (cudaCodeCopy != cudaSuccess) {
		printCudaErrorCode("Error copying to device: ", cudaCodeCopy);
	}
	return d_floatArray;
}


double* ProbClassifierManager::copyDoubleArrayToHost(double *h_doubleArray, double *d_doubleArray,
		const int BYTES) {
	cudaError_t cudaCodeCopy = cudaMemcpy(h_doubleArray, d_doubleArray, BYTES,
			cudaMemcpyDeviceToHost);
	if (cudaCodeCopy != cudaSuccess) {
		printCudaErrorCode("Error copying to host: ", cudaCodeCopy);
	}
	return h_doubleArray;
}

float* ProbClassifierManager::copyFloatArrayToHost(float *h_floatArray, float *d_floatArray,
		const int BYTES) {
	cudaError_t cudaCodeCopy = cudaMemcpy(h_floatArray, d_floatArray, BYTES,
			cudaMemcpyDeviceToHost);
	if (cudaCodeCopy != cudaSuccess) {
		printCudaErrorCode("Error copying to host: ", cudaCodeCopy);
	}
	return h_floatArray;
}

int* ProbClassifierManager::copyIntArrayToHost(int *h_intArray, int *d_intArray, const int BYTES) {

	cudaError_t cudaCodeCopy = cudaMemcpy(h_intArray, d_intArray, BYTES,
			cudaMemcpyDeviceToHost);
	if (cudaCodeCopy != cudaSuccess) {
		printCudaErrorCode("Error copying to host: ", cudaCodeCopy);
	}
	return h_intArray;
}

int* ProbClassifierManager::copyIntToGPU(int h_int) {
	int *d_int;
	cudaError_t cudaCode = cudaMalloc((void**) &d_int, sizeof(int));
	if (cudaCode != cudaSuccess) {
		printCudaErrorCode("Error allocating device memory: ", cudaCode);
	}
	cudaError_t cudaCodeCopy = cudaMemcpy(d_int, &h_int, sizeof(int),
			cudaMemcpyHostToDevice);
	if (cudaCodeCopy != cudaSuccess) {
		printCudaErrorCode("Error copying to device: ", cudaCodeCopy);
	}
	return d_int;
}

double* ProbClassifierManager::copyDoubleToGPU(double h_double) {
	double *d_double;
	cudaError_t cudaCode = cudaMalloc((void**) &d_double, sizeof(double));
	if (cudaCode != cudaSuccess) {
		printCudaErrorCode("Error allocating device memory: ", cudaCode);
	}
	cudaError_t cudaCodeCopy = cudaMemcpy(d_double, &h_double, sizeof(double),
			cudaMemcpyHostToDevice);
	if (cudaCodeCopy != cudaSuccess) {
		printCudaErrorCode("Error copying to device: ", cudaCodeCopy);
	}
	return d_double;
}

int* allocateMemoryForIntArrayAndInitialiseToZero(int ARRAY_SIZE,	int ARRAY_BYTES) {
	int *h_intArray;
	h_intArray = (int*) malloc(ARRAY_BYTES);
	for (int i = 0; i < ARRAY_SIZE; i++) {
		h_intArray[i] = 0;
	}
	return h_intArray;
}

int* allocateMemoryForIntArrayAndInitialiseToOne(int ARRAY_SIZE,	int ARRAY_BYTES) {
	int *h_intArray;
	h_intArray = (int*) malloc(ARRAY_BYTES);
	for (int i = 0; i < ARRAY_SIZE; i++) {
		h_intArray[i] = 1;
	}
	return h_intArray;
}

double* allocateMemoryForDoubleArrayAndInitialiseToZero(int ARRAY_SIZE, int ARRAY_BYTES) {
	double *h_doubleArray;
	h_doubleArray = (double*) malloc(ARRAY_BYTES);
	for (int i = 0; i < ARRAY_SIZE; i++) {
		h_doubleArray[i] = 0.0;
	}
	return h_doubleArray;
}

float* allocateMemoryForFloatArrayAndInitialiseToZero(int ARRAY_SIZE,int ARRAY_BYTES) {
	float *h_floatArray;
	h_floatArray = (float*) malloc(ARRAY_BYTES);
	for (int i = 0; i < ARRAY_SIZE; i++) {
		h_floatArray[i] = 0.0;
	}
	return h_floatArray;
}

extern "C" void checkCudaErrorCodeWrapper();

extern "C" void calculateComparesByClassWrapper(DataHolder& dataHolder, dim3 blocks, dim3 threads);

extern "C" void calculateCommonFeaturesNbWrapper(DataHolder& dataHolder,ResultsHolder& resultsHolder, dim3 blocks, dim3 threads, dim3 block2, dim3 threads2);

extern "C" void calculateScoresNbWrapper(DataHolder& dataHolder, ResultsHolder& resultsHolder, dim3 blocks, dim3 threads);

extern "C" void calculateScoresWrapper(DataHolder& dataHolder, ResultsHolder& resultsHolder, dim3 blocks, dim3 threads,dim3 blocksRascal);

extern "C" void scaleScoresForClassSizeWrapper(DataHolder& dataHolder, ResultsHolder& resultsHolder, dim3 blocks, dim3 threads);

extern "C" void calculateClassVotesPredictionCountWrapper(DataHolder& dataHolder, ResultsHolder& resultsHolder, dim3 blocks,dim3 threads);

extern "C" void calculateClassVotesCummulativeScoresWrapper(DataHolder& dataHolder, ResultsHolder& resultsHolder, dim3 blocks, dim3 threads);

extern "C" void makeClassVotesCummulativeWrapper(DataHolder& dataHolder, ResultsHolder& resultsHolder, dim3 blocks, dim3 threads);

extern "C" void calculateClassProbabilitiesWrapper(DataHolder& dataHolder, ResultsHolder& resultsHolder, dim3 blocks, dim3 threads);

extern "C" void calculateConfusionMatrixWrapper(DataHolder& dataHolder, ResultsHolder& resultsHolder, dim3 blocks, dim3 threads);

void ProbClassifierManager::generateResultsHolders()
{
	if (dataHolder.runRascal) {
		generateResultsHolder("rascal", dataHolder.dataTest.CLASS_PROB_ARRAY_BEST_BYTES);
	}

	if (dataHolder.runPrw) {
		generateResultsHolder("prw", dataHolder.dataTest.CLASS_PROB_ARRAY_BEST_BYTES);
	}

	if (dataHolder.runNb) {
		generateResultsHolder("nb", dataHolder.dataTest.CLASS_PROB_ARRAY_BEST_BYTES);
	}
}

void ProbClassifierManager::allocateHostArrays() {
	dataHolder.dataTest.h_numOfComparesByClass = allocateMemoryForIntArrayAndInitialiseToZero(
		dataHolder.dataTest.NUM_COMPARES_ARRAY_SIZE, dataHolder.dataTest.NUM_COMPARES_ARRAY_BYTES);

	dataHolder.dataTrain.h_feature_count_array = allocateMemoryForIntArrayAndInitialiseToZero(
			dataHolder.dataTrain.FEATURE_COUNT_ARRAY_BY_CLASS_SIZE, dataHolder.dataTrain.FEATURE_COUNT_ARRAY_BY_CLASS_BYTES);
}


void ProbClassifierManager::copyQindependentHostDataToGPU_opencl() {
	qIndependentMemoryRequired = 0;
	cl_int ret;

	//dataHolder.d_randoms = copyIntArrayToGPU(dataHolder.h_randoms, dataHolder.RANDOMS_BYTES);
	dataHolder.d_randoms_ocl = clCreateBuffer(dataHolder.clctx, CL_MEM_READ_WRITE, dataHolder.RANDOMS_BYTES, NULL, NULL);
	ret = clEnqueueWriteBuffer(dataHolder.clcmdq, dataHolder.d_randoms_ocl, CL_TRUE, 0, dataHolder.RANDOMS_BYTES, dataHolder.h_randoms, 0, NULL, NULL);

	qIndependentMemoryRequired+= dataHolder.RANDOMS_BYTES;

	if(dataHolder.runPrw){
		//dataHolder.d_prw_lookupTable = copyDoubleArrayToGPU(dataHolder.h_prw_lookupTable, dataHolder.PRW_LOOKUPTABLE_BYTES);
		dataHolder.d_prw_lookupTable_ocl = clCreateBuffer(dataHolder.clctx, CL_MEM_READ_WRITE, dataHolder.PRW_LOOKUPTABLE_BYTES, NULL, NULL);
		ret = clEnqueueWriteBuffer(dataHolder.clcmdq, dataHolder.d_prw_lookupTable_ocl, CL_TRUE, 0, dataHolder.PRW_LOOKUPTABLE_BYTES, dataHolder.h_prw_lookupTable, 0, NULL, NULL);
		qIndependentMemoryRequired+=dataHolder.PRW_LOOKUPTABLE_BYTES;
	}
	//dataHolder.dataTrain.d_in = copyIntArrayToGPU(dataHolder.dataTrain.h_in, dataHolder.dataTrain.IN_ARRAY_BYTES);
	dataHolder.dataTrain.d_in_ocl = clCreateBuffer(dataHolder.clctx, CL_MEM_READ_WRITE, dataHolder.dataTrain.IN_ARRAY_BYTES, NULL, NULL);
	ret = clEnqueueWriteBuffer(dataHolder.clcmdq, dataHolder.dataTrain.d_in_ocl, CL_TRUE, 0, dataHolder.dataTrain.IN_ARRAY_BYTES, dataHolder.dataTrain.h_in, 0, NULL, NULL);

	//dataHolder.dataTrain.d_classMember = copyIntArrayToGPU(dataHolder.dataTrain.h_classMember, dataHolder.dataTrain.CLASS_ARRAY_BYTES);
	dataHolder.dataTrain.d_classMember_ocl = clCreateBuffer(dataHolder.clctx, CL_MEM_READ_WRITE, dataHolder.dataTrain.CLASS_ARRAY_BYTES, NULL, NULL);
	ret = clEnqueueWriteBuffer(dataHolder.clcmdq, dataHolder.dataTrain.d_classMember_ocl, CL_TRUE, 0, dataHolder.dataTrain.CLASS_ARRAY_BYTES, dataHolder.dataTrain.h_classMember, 0, NULL, NULL);

	//dataHolder.dataTrain.d_kfoldMember = copyIntArrayToGPU(dataHolder.dataTrain.h_kfoldMember, dataHolder.dataTrain.CLASS_ARRAY_BYTES);
	dataHolder.dataTrain.d_kfoldMember_ocl = clCreateBuffer(dataHolder.clctx, CL_MEM_READ_WRITE, dataHolder.dataTrain.CLASS_ARRAY_BYTES, NULL, NULL);
	ret = clEnqueueWriteBuffer(dataHolder.clcmdq, dataHolder.dataTrain.d_kfoldMember_ocl, CL_TRUE, 0, dataHolder.dataTrain.CLASS_ARRAY_BYTES, dataHolder.dataTrain.h_kfoldMember, 0, NULL, NULL);

	qIndependentMemoryRequired = qIndependentMemoryRequired + dataHolder.dataTrain.IN_ARRAY_BYTES + dataHolder.dataTrain.CLASS_ARRAY_BYTES*2;

	if(dataHolder.dataFormat==0 && dataHolder.runPrw) {
		//dataHolder.dataTrain.d_inNormalised = copyDoubleArrayToGPU(dataHolder.dataTrain.h_inNormalised, dataHolder.dataTrain.IN_ARRAY_NORMALISED_BYTES);
		dataHolder.dataTrain.d_inNormalised_ocl = clCreateBuffer(dataHolder.clctx, CL_MEM_READ_WRITE, dataHolder.dataTrain.IN_ARRAY_NORMALISED_BYTES, NULL, NULL);
		ret = clEnqueueWriteBuffer(dataHolder.clcmdq, dataHolder.dataTrain.d_inNormalised_ocl, CL_TRUE, 0, dataHolder.dataTrain.IN_ARRAY_NORMALISED_BYTES, dataHolder.dataTrain.h_inNormalised, 0, NULL, NULL);

		qIndependentMemoryRequired+=dataHolder.dataTrain.IN_ARRAY_NORMALISED_BYTES;
	}

	if(dataHolder.dataFormat==1) {
		//dataHolder.dataTrain.d_inSparseVectorBreakPoints = copyIntArrayToGPU(dataHolder.dataTrain.h_inSparseVectorBreakPoints, dataHolder.dataTrain.SPARSE_ARRAY_BYTES);
		dataHolder.dataTrain.d_inSparseVectorBreakPoints_ocl = clCreateBuffer(dataHolder.clctx, CL_MEM_READ_WRITE, dataHolder.dataTrain.SPARSE_ARRAY_BYTES, NULL, NULL);
		ret = clEnqueueWriteBuffer(dataHolder.clcmdq, dataHolder.dataTrain.d_inSparseVectorBreakPoints_ocl, CL_TRUE, 0, dataHolder.dataTrain.SPARSE_ARRAY_BYTES, dataHolder.dataTrain.h_inSparseVectorBreakPoints, 0, NULL, NULL);

		qIndependentMemoryRequired+=dataHolder.dataTrain.SPARSE_ARRAY_BYTES;
	}

	if(dataHolder.samplingMethod==0 || dataHolder.samplingMethod==1){
		//dataHolder.dataTest.d_numOfComparesByClass = copyIntArrayToGPU(dataHolder.dataTest.h_numOfComparesByClass, dataHolder.dataTrain.NUM_COMPARES_ARRAY_BYTES);
		dataHolder.dataTest.d_numOfComparesByClass_ocl = clCreateBuffer(dataHolder.clctx, CL_MEM_READ_WRITE, dataHolder.dataTrain.NUM_COMPARES_ARRAY_BYTES, NULL, NULL);
		ret = clEnqueueWriteBuffer(dataHolder.clcmdq, dataHolder.dataTest.d_numOfComparesByClass_ocl, CL_TRUE, 0, dataHolder.dataTrain.NUM_COMPARES_ARRAY_BYTES, dataHolder.dataTest.h_numOfComparesByClass, 0, NULL, NULL);

		dataHolder.copyTrainDevicePointersToTestDevicePointers(dataHolder.dataTrain, dataHolder.dataTest);
		qIndependentMemoryRequired = qIndependentMemoryRequired + dataHolder.dataTrain.NUM_COMPARES_ARRAY_BYTES;
	}
	else if(dataHolder.samplingMethod==2){

		//dataHolder.dataTest.d_in = copyIntArrayToGPU(dataHolder.dataTest.h_in, dataHolder.dataTest.IN_ARRAY_BYTES);
		dataHolder.dataTest.d_in_ocl = clCreateBuffer(dataHolder.clctx, CL_MEM_READ_WRITE, dataHolder.dataTest.IN_ARRAY_BYTES, NULL, NULL);
		ret = clEnqueueWriteBuffer(dataHolder.clcmdq, dataHolder.dataTest.d_in_ocl, CL_TRUE, 0, dataHolder.dataTest.IN_ARRAY_BYTES, dataHolder.dataTest.h_in, 0, NULL, NULL);

		//dataHolder.dataTest.d_classMember = copyIntArrayToGPU(dataHolder.dataTest.h_classMember, dataHolder.dataTest.CLASS_ARRAY_BYTES);
		dataHolder.dataTest.d_classMember_ocl = clCreateBuffer(dataHolder.clctx, CL_MEM_READ_WRITE, dataHolder.dataTest.CLASS_ARRAY_BYTES, NULL, NULL);
		ret = clEnqueueWriteBuffer(dataHolder.clcmdq, dataHolder.dataTest.d_classMember_ocl, CL_TRUE, 0, dataHolder.dataTest.CLASS_ARRAY_BYTES, dataHolder.dataTest.h_classMember, 0, NULL, NULL);

		//dataHolder.dataTest.d_kfoldMember = copyIntArrayToGPU(dataHolder.dataTest.h_kfoldMember, dataHolder.dataTest.CLASS_ARRAY_BYTES);
		dataHolder.dataTest.d_kfoldMember_ocl = clCreateBuffer(dataHolder.clctx, CL_MEM_READ_WRITE, dataHolder.dataTest.CLASS_ARRAY_BYTES, NULL, NULL);
		ret = clEnqueueWriteBuffer(dataHolder.clcmdq, dataHolder.dataTest.d_kfoldMember_ocl, CL_TRUE, 0, dataHolder.dataTest.CLASS_ARRAY_BYTES, dataHolder.dataTest.h_kfoldMember, 0, NULL, NULL);

		//dataHolder.dataTest.d_numOfComparesByClass = copyIntArrayToGPU(dataHolder.dataTest.h_numOfComparesByClass, dataHolder.dataTest.NUM_COMPARES_ARRAY_BYTES);
		dataHolder.dataTest.d_numOfComparesByClass_ocl = clCreateBuffer(dataHolder.clctx, CL_MEM_READ_WRITE, dataHolder.dataTest.NUM_COMPARES_ARRAY_BYTES, NULL, NULL);
		ret = clEnqueueWriteBuffer(dataHolder.clcmdq, dataHolder.dataTest.d_numOfComparesByClass_ocl, CL_TRUE, 0, dataHolder.dataTest.NUM_COMPARES_ARRAY_BYTES, dataHolder.dataTest.h_numOfComparesByClass, 0, NULL, NULL);


		qIndependentMemoryRequired = qIndependentMemoryRequired + dataHolder.dataTest.IN_ARRAY_BYTES + dataHolder.dataTest.CLASS_ARRAY_BYTES*2 + dataHolder.dataTest.NUM_COMPARES_ARRAY_BYTES;

		if(dataHolder.dataFormat==0 && dataHolder.runPrw) {
			//dataHolder.dataTest.d_inNormalised = copyDoubleArrayToGPU(dataHolder.dataTest.h_inNormalised, dataHolder.dataTest.IN_ARRAY_NORMALISED_BYTES);
			dataHolder.dataTest.d_inNormalised_ocl = clCreateBuffer(dataHolder.clctx, CL_MEM_READ_WRITE, dataHolder.dataTest.IN_ARRAY_NORMALISED_BYTES, NULL, NULL);
			ret = clEnqueueWriteBuffer(dataHolder.clcmdq, dataHolder.dataTest.d_inNormalised_ocl, CL_TRUE, 0, dataHolder.dataTest.IN_ARRAY_NORMALISED_BYTES, dataHolder.dataTest.h_inNormalised, 0, NULL, NULL);

			qIndependentMemoryRequired+=dataHolder.dataTest.IN_ARRAY_NORMALISED_BYTES;
		}
		if(dataHolder.dataFormat==1) {
			//dataHolder.dataTest.d_inSparseVectorBreakPoints = copyIntArrayToGPU(dataHolder.dataTest.h_inSparseVectorBreakPoints, dataHolder.dataTest.SPARSE_ARRAY_BYTES);
			dataHolder.dataTest.d_inSparseVectorBreakPoints_ocl = clCreateBuffer(dataHolder.clctx, CL_MEM_READ_WRITE, dataHolder.dataTest.SPARSE_ARRAY_BYTES, NULL, NULL);
			ret = clEnqueueWriteBuffer(dataHolder.clcmdq, dataHolder.dataTest.d_inSparseVectorBreakPoints_ocl, CL_TRUE, 0, dataHolder.dataTest.SPARSE_ARRAY_BYTES, dataHolder.dataTest.h_inSparseVectorBreakPoints, 0, NULL, NULL);

			qIndependentMemoryRequired+=dataHolder.dataTest.SPARSE_ARRAY_BYTES;
		}
	}
}





void ProbClassifierManager::copyQindependentHostDataToGPU() {
	qIndependentMemoryRequired = 0;

	dataHolder.d_randoms = copyIntArrayToGPU(dataHolder.h_randoms, dataHolder.RANDOMS_BYTES);

	qIndependentMemoryRequired+= dataHolder.RANDOMS_BYTES;

	if(dataHolder.runPrw){
		dataHolder.d_prw_lookupTable = copyDoubleArrayToGPU(dataHolder.h_prw_lookupTable, dataHolder.PRW_LOOKUPTABLE_BYTES);
		qIndependentMemoryRequired+=dataHolder.PRW_LOOKUPTABLE_BYTES;
	}
	dataHolder.dataTrain.d_in = copyIntArrayToGPU(dataHolder.dataTrain.h_in, dataHolder.dataTrain.IN_ARRAY_BYTES);
	dataHolder.dataTrain.d_classMember = copyIntArrayToGPU(dataHolder.dataTrain.h_classMember, dataHolder.dataTrain.CLASS_ARRAY_BYTES);
	dataHolder.dataTrain.d_kfoldMember = copyIntArrayToGPU(dataHolder.dataTrain.h_kfoldMember, dataHolder.dataTrain.CLASS_ARRAY_BYTES);

	qIndependentMemoryRequired = qIndependentMemoryRequired + dataHolder.dataTrain.IN_ARRAY_BYTES + dataHolder.dataTrain.CLASS_ARRAY_BYTES*2;


	if(dataHolder.dataFormat==0 && dataHolder.runPrw) {
		dataHolder.dataTrain.d_inNormalised = copyDoubleArrayToGPU(dataHolder.dataTrain.h_inNormalised, dataHolder.dataTrain.IN_ARRAY_NORMALISED_BYTES);
		qIndependentMemoryRequired+=dataHolder.dataTrain.IN_ARRAY_NORMALISED_BYTES;
	}

	if(dataHolder.dataFormat==1) {
		dataHolder.dataTrain.d_inSparseVectorBreakPoints = copyIntArrayToGPU(dataHolder.dataTrain.h_inSparseVectorBreakPoints, dataHolder.dataTrain.SPARSE_ARRAY_BYTES);
		qIndependentMemoryRequired+=dataHolder.dataTrain.SPARSE_ARRAY_BYTES;
	}

	if(dataHolder.samplingMethod==0 || dataHolder.samplingMethod==1){
		dataHolder.dataTest.d_numOfComparesByClass = copyIntArrayToGPU(dataHolder.dataTest.h_numOfComparesByClass, dataHolder.dataTrain.NUM_COMPARES_ARRAY_BYTES);
		dataHolder.copyTrainDevicePointersToTestDevicePointers(dataHolder.dataTrain, dataHolder.dataTest);
		qIndependentMemoryRequired = qIndependentMemoryRequired + dataHolder.dataTrain.NUM_COMPARES_ARRAY_BYTES;
	}
	else if(dataHolder.samplingMethod==2){

		dataHolder.dataTest.d_in = copyIntArrayToGPU(dataHolder.dataTest.h_in, dataHolder.dataTest.IN_ARRAY_BYTES);
		dataHolder.dataTest.d_classMember = copyIntArrayToGPU(dataHolder.dataTest.h_classMember, dataHolder.dataTest.CLASS_ARRAY_BYTES);
		dataHolder.dataTest.d_kfoldMember = copyIntArrayToGPU(dataHolder.dataTest.h_kfoldMember, dataHolder.dataTest.CLASS_ARRAY_BYTES);
		dataHolder.dataTest.d_numOfComparesByClass = copyIntArrayToGPU(dataHolder.dataTest.h_numOfComparesByClass, dataHolder.dataTest.NUM_COMPARES_ARRAY_BYTES);

		qIndependentMemoryRequired = qIndependentMemoryRequired + dataHolder.dataTest.IN_ARRAY_BYTES + dataHolder.dataTest.CLASS_ARRAY_BYTES*2 + dataHolder.dataTest.NUM_COMPARES_ARRAY_BYTES;

		if(dataHolder.dataFormat==0 && dataHolder.runPrw) {
			dataHolder.dataTest.d_inNormalised = copyDoubleArrayToGPU(dataHolder.dataTest.h_inNormalised, dataHolder.dataTest.IN_ARRAY_NORMALISED_BYTES);
			qIndependentMemoryRequired+=dataHolder.dataTest.IN_ARRAY_NORMALISED_BYTES;
		}
		if(dataHolder.dataFormat==1) {
			dataHolder.dataTest.d_inSparseVectorBreakPoints = copyIntArrayToGPU(dataHolder.dataTest.h_inSparseVectorBreakPoints, dataHolder.dataTest.SPARSE_ARRAY_BYTES);
			qIndependentMemoryRequired+=dataHolder.dataTest.SPARSE_ARRAY_BYTES;
		}
	}
}

void ProbClassifierManager::updateValuesForJ() {
	dataHolder.classifier.h_J_min = dataHolder.j_min;
	dataHolder.classifier.h_J_step = dataHolder.j_step;
	dataHolder.classifier.h_J = dataHolder.j_max;

	//code to set h_J to maximum possible value if user enters a value too high for multiple runs
	if(dataHolder.comboMethod==0){
		if ((dataHolder.dataTrain.L < 20)
				|| (dataHolder.dataTrain.L < 30 && abs(dataHolder.dataTrain.L - dataHolder.classifier.h_q) <= 3)
				|| (dataHolder.dataTrain.L < 50 && abs(dataHolder.dataTrain.L - dataHolder.classifier.h_q) <= 2)
				|| (abs(dataHolder.dataTrain.L - dataHolder.classifier.h_q) <= 1)) {
			FeatureComboGenerator featureComboGenerator(dataHolder);

			int maxComboNum = featureComboGenerator.nCr(dataHolder.dataTrain.L, dataHolder.classifier.h_q);
			if (maxComboNum < dataHolder.classifier.h_J) {
				dataHolder.classifier.h_J = maxComboNum;
				if (dataHolder.verbose != 0) {
					cout << "h_J reset to maximum combo num of " << maxComboNum << endl;
				}
			}
		}
	}

	if (dataHolder.classifier.h_J < dataHolder.classifier.h_J_min) {
		dataHolder.classifier.h_J_min = dataHolder.classifier.h_J;
		if (dataHolder.verbose != 0) {
			cout << "h_J_min reset to maximum combo num of " << dataHolder.classifier.h_J << endl;
		}
	}

	//check the value of h_step and reset if greater than (h_J - h_J_min)
	if (dataHolder.classifier.h_J_step > (dataHolder.classifier.h_J - dataHolder.classifier.h_J_min)) {
		dataHolder.classifier.h_J_step = dataHolder.classifier.h_J - dataHolder.classifier.h_J_min;
	}

	//calculate number of steps
	if (dataHolder.classifier.h_J_step != 0) {
		dataHolder.classifier.h_J_numOfSteps = (dataHolder.classifier.h_J - dataHolder.classifier.h_J_min) / dataHolder.classifier.h_J_step + 1;
	} else {
		dataHolder.classifier.h_J_numOfSteps = 1;
	}

	h_threadsToLaunch=min(dataHolder.classifier.h_J, dataHolder.maxThreads);

	if (dataHolder.verbose != 0) {
		cout << "J max set to " << dataHolder.classifier.h_J << endl;
		cout << "J min set to " << dataHolder.classifier.h_J_min << endl;
		cout << "Number of J steps in output =  " << dataHolder.classifier.h_J_numOfSteps << endl;
		cout << "Step size =  " << dataHolder.classifier.h_J_step << endl;
		cout << "Number of steps = " << dataHolder.classifier.h_J_numOfSteps << endl;
	}
}

void ProbClassifierManager::updateBatchIndependentArraySizeValues() {

	CONFUSION_MATRIX_ARRAY_SIZE = dataHolder.dataTrain.classes * dataHolder.dataTrain.classes * dataHolder.classifier.h_J_numOfSteps;
	CONFUSION_MATRIX_ARRAY_BYTES = CONFUSION_MATRIX_ARRAY_SIZE * sizeof(int);
	MCC_RESULTS_ARRAY_SIZE = dataHolder.classifier.h_J_numOfSteps;
	MCC_RESULTS_ARRAY_BYTES = MCC_RESULTS_ARRAY_SIZE * sizeof(double);
	CLASS_PROB_MASTER_ARRAY_SIZE = dataHolder.dataTest.N * dataHolder.dataTrain.classes * dataHolder.classifier.h_J_numOfSteps;
	CLASS_PROB_MASTER_ARRAY_BYTES = CLASS_PROB_MASTER_ARRAY_SIZE * sizeof(float);
}

void ProbClassifierManager::updateBatchDependentArraySizeValues(int batchSize) {
	SCORES_ARRAY_SIZE = batchSize * dataHolder.dataTrain.classes * dataHolder.classifier.h_J;
	SCORES_ARRAY_BYTES = SCORES_ARRAY_SIZE * sizeof(double);
	SCORES_FLOAT_ARRAY_BYTES = SCORES_ARRAY_SIZE * sizeof(float);
	CLASS_VOTES_ARRAY_SIZE = batchSize * dataHolder.dataTrain.classes * dataHolder.classifier.h_J_numOfSteps;
	CLASS_VOTES_ARRAY_BYTES = CLASS_VOTES_ARRAY_SIZE * sizeof(double);
	CLASS_VOTES_FLOAT_ARRAY_BYTES = CLASS_VOTES_ARRAY_SIZE * sizeof(float);
	CLASS_PROB_ARRAY_SIZE = batchSize * dataHolder.dataTrain.classes * dataHolder.classifier.h_J_numOfSteps;
	CLASS_PROB_ARRAY_BYTES = CLASS_PROB_ARRAY_SIZE * sizeof(float);
	if(dataHolder.runNb)
	{
		COMMON_FEATURES_NB_ARRAY_SIZE = batchSize*dataHolder.dataTrain.L*dataHolder.dataTrain.classes;
		COMMON_FEATURES_NB_ARRAY_BYTES = COMMON_FEATURES_NB_ARRAY_SIZE * sizeof(int);
	}
}

void ProbClassifierManager::calculateNumOfBatches() {
	SCORES_ARRAY_SIZE = dataHolder.dataTest.N * dataHolder.dataTrain.classes * dataHolder.classifier.h_J;
	SCORES_ARRAY_BYTES = SCORES_ARRAY_SIZE * sizeof(double);
	SCORES_FLOAT_ARRAY_BYTES = SCORES_ARRAY_SIZE * sizeof(float);
	CLASS_VOTES_ARRAY_SIZE = dataHolder.dataTest.N * dataHolder.dataTrain.classes * dataHolder.classifier.h_J_numOfSteps;
	CLASS_VOTES_ARRAY_BYTES = CLASS_VOTES_ARRAY_SIZE * sizeof(double);
	CLASS_PROB_ARRAY_SIZE = dataHolder.dataTest.N * dataHolder.dataTrain.classes * dataHolder.classifier.h_J_numOfSteps;
	CLASS_PROB_ARRAY_BYTES = CLASS_PROB_ARRAY_SIZE * sizeof(double);
	COMMON_FEATURES_NB_ARRAY_SIZE = dataHolder.dataTest.N * dataHolder.dataTrain.L * dataHolder.dataTrain.classes;
	COMMON_FEATURES_NB_ARRAY_BYTES = COMMON_FEATURES_NB_ARRAY_SIZE * sizeof(int);

	qDependentMemoryRequired = dataHolder.COMBO_ARRAY_BYTES + SCORES_ARRAY_BYTES +
		CLASS_VOTES_ARRAY_BYTES + CLASS_PROB_ARRAY_BYTES + CONFUSION_MATRIX_ARRAY_BYTES +
		COMMON_FEATURES_NB_ARRAY_BYTES;

	if(dataHolder.verbose!=0)
	{
		cout << "q dependent memory requirements: " << qDependentMemoryRequired << endl;
	}

	long gpuMemoryAvailable = 0.90*dataHolder.gpuGlobalMemory - qIndependentMemoryRequired;

	if(qDependentMemoryRequired > gpuMemoryAvailable)
	{
		dataHolder.classifier.h_numOfBatches = gpuMemoryAvailable / qDependentMemoryRequired + 1;
	}
}

void ProbClassifierManager::generateBatchData() {
	BATCH_ARRAY_BYTES = dataHolder.classifier.h_numOfBatches * sizeof(int);
	dataHolder.classifier.h_batchSizes = (int*) malloc(BATCH_ARRAY_BYTES);
	dataHolder.classifier.h_batchStartPositions = (int*) malloc(BATCH_ARRAY_BYTES);

	dataHolder.classifier.h_batchSize = dataHolder.dataTest.N/dataHolder.classifier.h_numOfBatches;

	dataHolder.classifier.h_batchStartPositions[0]=0;

	for(int i=0; i<(dataHolder.classifier.h_numOfBatches-1); i++)	{
		dataHolder.classifier.h_batchSizes[i] = dataHolder.classifier.h_batchSize;
		dataHolder.classifier.h_batchStartPositions[i+1] = dataHolder.classifier.h_batchStartPositions[i] + dataHolder.classifier.h_batchSizes[i];
	}
	dataHolder.classifier.h_batchSizes[(dataHolder.classifier.h_numOfBatches-1)] = dataHolder.dataTest.N - (dataHolder.classifier.h_numOfBatches-1)*dataHolder.classifier.h_batchSize;

	//for(int i=0; i<dataHolder.classifier.h_numOfBatches; i++)	{
	//	cout<<dataHolder.classifier.h_batchSizes[i] << "," << endl;
	//	cout<<dataHolder.classifier.h_batchStartPositions[i] << "," << endl;
	//}
}

void ProbClassifierManager::copyBatchIndependentDataToGPU_ocl(ResultsHolder& resultsHolder) {

	cl_int ret;
	//resultsHolder.d_confusion_matrix = copyIntArrayToGPU(resultsHolder.h_confusion_matrix,CONFUSION_MATRIX_ARRAY_BYTES);
	resultsHolder.d_confusion_matrix_ocl = clCreateBuffer(dataHolder.clctx, CL_MEM_READ_WRITE, CONFUSION_MATRIX_ARRAY_BYTES, NULL, NULL);
	ret = clEnqueueWriteBuffer(dataHolder.clcmdq, resultsHolder.d_confusion_matrix_ocl, CL_TRUE, 0, CONFUSION_MATRIX_ARRAY_BYTES, resultsHolder.h_confusion_matrix, 0, NULL, NULL);
}

void ProbClassifierManager::copyBatchIndependentDataToGPU(ResultsHolder& resultsHolder) {

	resultsHolder.d_confusion_matrix = copyIntArrayToGPU(resultsHolder.h_confusion_matrix,CONFUSION_MATRIX_ARRAY_BYTES);

}




void ProbClassifierManager::copyBatchDependentDataToGPU_ocl(int h_Q, int h_batchNum, ResultsHolder& resultsHolder) {
	cl_int ret;
	if(dataHolder.h_method_ref==2 && !alreadyRanCalculateCommonNbFeatures) {
		//dataHolder.dataTest.d_common_features_nb = copyIntArrayToGPU(dataHolder.dataTest.h_common_features_nb, COMMON_FEATURES_NB_ARRAY_BYTES);
		dataHolder.dataTest.d_common_features_nb_ocl = clCreateBuffer(dataHolder.clctx, CL_MEM_READ_WRITE, COMMON_FEATURES_NB_ARRAY_BYTES, NULL, NULL);
		ret = clEnqueueWriteBuffer(dataHolder.clcmdq, dataHolder.dataTest.d_common_features_nb_ocl, CL_TRUE, 0, COMMON_FEATURES_NB_ARRAY_BYTES, dataHolder.dataTest.h_common_features_nb, 0, NULL, NULL);
	}

	if(dataHolder.h_method_ref==0 || dataHolder.h_method_ref==2) {
		//resultsHolder.d_scores_float = copyFloatArrayToGPU(resultsHolder.h_scores_float,SCORES_FLOAT_ARRAY_BYTES);
		resultsHolder.d_scores_float_ocl = clCreateBuffer(dataHolder.clctx, CL_MEM_READ_WRITE, SCORES_FLOAT_ARRAY_BYTES, NULL, NULL);
		ret = clEnqueueWriteBuffer(dataHolder.clcmdq, resultsHolder.d_scores_float_ocl, CL_TRUE, 0, SCORES_FLOAT_ARRAY_BYTES, resultsHolder.h_scores_float, 0, NULL, NULL);

		//resultsHolder.d_class_votes_float = copyFloatArrayToGPU(resultsHolder.h_class_votes_float,CLASS_VOTES_FLOAT_ARRAY_BYTES);
		resultsHolder.d_class_votes_float_ocl = clCreateBuffer(dataHolder.clctx, CL_MEM_READ_WRITE, CLASS_VOTES_FLOAT_ARRAY_BYTES, NULL, NULL);
		ret = clEnqueueWriteBuffer(dataHolder.clcmdq, resultsHolder.d_class_votes_float_ocl, CL_TRUE, 0, CLASS_VOTES_FLOAT_ARRAY_BYTES, resultsHolder.h_class_votes_float, 0, NULL, NULL);

	} else {
		//resultsHolder.d_scores = copyDoubleArrayToGPU(resultsHolder.h_scores,SCORES_ARRAY_BYTES);
		resultsHolder.d_scores_ocl = clCreateBuffer(dataHolder.clctx, CL_MEM_READ_WRITE, SCORES_ARRAY_BYTES, NULL, NULL);
		ret = clEnqueueWriteBuffer(dataHolder.clcmdq, resultsHolder.d_scores_ocl, CL_TRUE, 0, SCORES_ARRAY_BYTES, resultsHolder.h_scores, 0, NULL, NULL);

		//resultsHolder.d_class_votes = copyDoubleArrayToGPU(resultsHolder.h_class_votes,CLASS_VOTES_ARRAY_BYTES);
		resultsHolder.d_class_votes_ocl = clCreateBuffer(dataHolder.clctx, CL_MEM_READ_WRITE, CLASS_VOTES_ARRAY_BYTES, NULL, NULL);
		ret = clEnqueueWriteBuffer(dataHolder.clcmdq, resultsHolder.d_class_votes_ocl, CL_TRUE, 0, CLASS_VOTES_ARRAY_BYTES, resultsHolder.h_class_votes, 0, NULL, NULL);
	}
	//resultsHolder.d_class_prob = copyFloatArrayToGPU(resultsHolder.h_class_prob,CLASS_PROB_ARRAY_BYTES);
	resultsHolder.d_class_prob_ocl = clCreateBuffer(dataHolder.clctx, CL_MEM_READ_WRITE, CLASS_PROB_ARRAY_BYTES, NULL, NULL);
	ret = clEnqueueWriteBuffer(dataHolder.clcmdq, resultsHolder.d_class_prob_ocl, CL_TRUE, 0, CLASS_PROB_ARRAY_BYTES, resultsHolder.h_class_prob, 0, NULL, NULL);
}


void ProbClassifierManager::copyBatchDependentDataToGPU(int h_Q, int h_batchNum, ResultsHolder& resultsHolder) {
	if(dataHolder.h_method_ref==2 && !alreadyRanCalculateCommonNbFeatures) {
		dataHolder.dataTest.d_common_features_nb = copyIntArrayToGPU(dataHolder.dataTest.h_common_features_nb, COMMON_FEATURES_NB_ARRAY_BYTES);
	}

	if(dataHolder.h_method_ref==0 || dataHolder.h_method_ref==2) {
		resultsHolder.d_scores_float = copyFloatArrayToGPU(resultsHolder.h_scores_float,SCORES_FLOAT_ARRAY_BYTES);
		resultsHolder.d_class_votes_float = copyFloatArrayToGPU(resultsHolder.h_class_votes_float,CLASS_VOTES_FLOAT_ARRAY_BYTES);
	} else {
		resultsHolder.d_scores = copyDoubleArrayToGPU(resultsHolder.h_scores,SCORES_ARRAY_BYTES);
		resultsHolder.d_class_votes = copyDoubleArrayToGPU(resultsHolder.h_class_votes,CLASS_VOTES_ARRAY_BYTES);
	}
	resultsHolder.d_class_prob = copyFloatArrayToGPU(resultsHolder.h_class_prob,CLASS_PROB_ARRAY_BYTES);

}

void ProbClassifierManager::setUpBatchIndependentHostArrays(ResultsHolder& resultsHolder) {
	resultsHolder.h_mcc_results =	allocateMemoryForDoubleArrayAndInitialiseToZero(
			MCC_RESULTS_ARRAY_SIZE, MCC_RESULTS_ARRAY_BYTES);
	resultsHolder.h_confusion_matrix = allocateMemoryForIntArrayAndInitialiseToZero(
			CONFUSION_MATRIX_ARRAY_SIZE, CONFUSION_MATRIX_ARRAY_BYTES);
	resultsHolder.h_class_prob_master = allocateMemoryForFloatArrayAndInitialiseToZero(
			CLASS_PROB_MASTER_ARRAY_SIZE, CLASS_PROB_MASTER_ARRAY_BYTES);
}

void ProbClassifierManager::setUpBatchDependentHostArrays(int batchNum, ResultsHolder& resultsHolder) {
	if(dataHolder.classifier.h_numOfBatches>1 || !alreadyRanCalculateCommonNbFeatures)
	{
		dataHolder.dataTest.h_common_features_nb = allocateMemoryForIntArrayAndInitialiseToZero( COMMON_FEATURES_NB_ARRAY_SIZE,
			COMMON_FEATURES_NB_ARRAY_BYTES);
	}

	if(dataHolder.h_method_ref==0 || dataHolder.h_method_ref==2) {
		resultsHolder.h_scores_float = allocateMemoryForFloatArrayAndInitialiseToZero(
				SCORES_ARRAY_SIZE, SCORES_FLOAT_ARRAY_BYTES);
		resultsHolder.h_class_votes_float =	allocateMemoryForFloatArrayAndInitialiseToZero(
					CLASS_VOTES_ARRAY_SIZE, CLASS_VOTES_FLOAT_ARRAY_BYTES);
	} else {
		resultsHolder.h_scores = allocateMemoryForDoubleArrayAndInitialiseToZero(
					SCORES_ARRAY_SIZE, SCORES_ARRAY_BYTES);
		resultsHolder.h_class_votes =	allocateMemoryForDoubleArrayAndInitialiseToZero(
				CLASS_VOTES_ARRAY_SIZE, CLASS_VOTES_ARRAY_BYTES);
	}
	resultsHolder.h_class_prob = allocateMemoryForFloatArrayAndInitialiseToZero(
			CLASS_PROB_ARRAY_SIZE, CLASS_PROB_ARRAY_BYTES);
}

void ProbClassifierManager::generateRandomFeaturesForSubSamples() {

	FeatureComboGenerator featureComboGenerator(dataHolder);
	if(dataHolder.comboMethod==0){
		featureComboGenerator.generateCombos();
	}else if(dataHolder.comboMethod==2){
		featureComboGenerator.generateCombosFromFeatureCounts();
	}

	featureComboGenerator.printCombos();
	if (dataHolder.implementation == 1) {

		dataHolder.d_in_combo = copyIntArrayToGPU(dataHolder.h_in_combo, dataHolder.COMBO_ARRAY_BYTES);
		dataHolder.d_comboBreakPoints = copyIntArrayToGPU(dataHolder.h_comboBreakPoints, dataHolder.COMBO_BREAKPOINTS_BYTES);
	}
	else if (dataHolder.implementation == 2) {
		cl_int ret;
		dataHolder.d_in_combo_ocl = clCreateBuffer(dataHolder.clctx, CL_MEM_READ_WRITE, dataHolder.COMBO_ARRAY_BYTES, NULL, NULL);
		ret = clEnqueueWriteBuffer(dataHolder.clcmdq, dataHolder.d_in_combo_ocl, CL_TRUE, 0, dataHolder.COMBO_ARRAY_BYTES, dataHolder.h_in_combo, 0, NULL, NULL);

		dataHolder.d_comboBreakPoints_ocl = clCreateBuffer(dataHolder.clctx, CL_MEM_READ_WRITE, dataHolder.COMBO_BREAKPOINTS_BYTES, NULL, NULL);
		ret = clEnqueueWriteBuffer(dataHolder.clcmdq, dataHolder.d_comboBreakPoints_ocl, CL_TRUE, 0, dataHolder.COMBO_BREAKPOINTS_BYTES, dataHolder.h_comboBreakPoints, 0, NULL, NULL);

	}
}

void ProbClassifierManager::calculateFeatureCountByClass(){

	int L = dataHolder.dataTrain.L;
	int N = dataHolder.dataTrain.N;

	for(int itemId=0; itemId<N; itemId++)
	{
		//cout << "itemId: " << itemId << endl;
		int classId = dataHolder.dataTrain.h_classMember[itemId];
		//cout << "classId: " << classId << endl;
		for (int featureId=0; featureId<L; featureId++){
			//cout << "featureId: " << featureId << endl;
			if(dataHolder.dataTrain.h_in[itemId*L + featureId] > 0){
				dataHolder.dataTrain.h_feature_count_array[classId*L + featureId]++;
			}
			//cout << "feature: " << dataHolder.dataTrain.h_in[itemId*L + featureId] << endl;
			//cout << "featureCount: " << dataHolder.dataTrain.h_feature_count_array[classId*L + featureId]<< endl;
		}
	}
	dataHolder.dataTrain.print_h_feature_count_array();
	dataHolder.write_h_feature_count_array(dataHolder.dataTrain);
}


void ProbClassifierManager::runQindependentKernels() {
	int blocksToLaunch = min(dataHolder.maxBlocks,dataHolder.dataTest.N);
	dim3 blockSize1(blocksToLaunch, blocksToLaunch, 1);
	dim3 blockSize2(blocksToLaunch, 1, 1);
	dim3 defaultDim(1,1,1);

	//if(dataHolder.samplingMethod==2 && dataHolder.runNb)
	//{
	//	calculateFeatureCountByClass();
	//}

	//parallel implementation
	if (dataHolder.implementation ==2) {
		copyQindependentHostDataToGPU_opencl();
		calculateComparesByClassWrapper(dataHolder, blockSize2, defaultDim);
	}
	else if (dataHolder.implementation == 1) {

		copyQindependentHostDataToGPU();
		calculateComparesByClassWrapper(dataHolder, blockSize2,	defaultDim);
	}
	//serial implementation
	else if (dataHolder.implementation == 0)
	{
		serialKernels.setQindependentConstants(dataHolder);
		serialKernels.calculateComparesByClass(dataHolder);
	} else {
		cout << "please select an implementation, serial=0, parallel=1" << endl;
	}
}

void ProbClassifierManager::updateMasterClassProb(int batchNum, ResultsHolder& resultsHolder) {
	for (int rootId=0; rootId < dataHolder.classifier.h_batchSizes[batchNum]; rootId ++) {
		int masterRootId = rootId + dataHolder.classifier.h_batchStartPositions[batchNum];
		for (int stepNum = 0; stepNum < dataHolder.classifier.h_J_numOfSteps; stepNum++) {
			for (int classId = 0; classId < dataHolder.dataTrain.classes; classId++) {
				resultsHolder.h_class_prob_master[masterRootId * dataHolder.classifier.h_J_numOfSteps * dataHolder.dataTrain.classes
						+ stepNum * dataHolder.dataTrain.classes + classId] = resultsHolder.h_class_prob[rootId
						* dataHolder.classifier.h_J_numOfSteps * dataHolder.dataTrain.classes + stepNum * dataHolder.dataTrain.classes
						+ classId];
			}
		}
	}
}


void ProbClassifierManager::runParallelKernels(ResultsHolder& resultsHolder) {
	int threadsToLaunch2 = min(dataHolder.maxThreads, dataHolder.dataTrain.N);
	int threadsToLaunch3 = min(dataHolder.maxThreads, dataHolder.dataTrain.L);

	dim3 defaultDim(1, 1, 1);
	dim3 threadSize(h_threadsToLaunch, 1, 1);
	dim3 threadSize2(threadsToLaunch2,1,1);
	dim3 threadSize3(threadsToLaunch3,1,1);
	cout << "threads: "  << h_threadsToLaunch << endl;
	// copy data to GPU

	if(dataHolder.implementation==1) {
		copyBatchIndependentDataToGPU(resultsHolder);
	}
	else if (dataHolder.implementation==2) {
		copyBatchIndependentDataToGPU_ocl(resultsHolder);
	}

	for(int batchNum=0; batchNum < dataHolder.classifier.h_numOfBatches; batchNum++){
		dataHolder.classifier.h_currentBatchNum = batchNum;
		dataHolder.classifier.h_currentBatchStartPos = dataHolder.classifier.h_batchStartPositions[batchNum];
		dataHolder.classifier.h_currentBatchSize = dataHolder.classifier.h_batchSizes[batchNum];

		int blocksToLaunch = min(dataHolder.maxBlocks,dataHolder.classifier.h_batchSizes[batchNum]);
		int blocksToLaunch2 = min(dataHolder.maxBlocks, dataHolder.dataTrain.N);

		cout << "blocks: "  << blocksToLaunch << endl;
		dim3 blockSize1(blocksToLaunch, blocksToLaunch, 1);
		dim3 blockSize2(blocksToLaunch, 1, 1);
		dim3 blockSize3(blocksToLaunch, blocksToLaunch2,1);
		dim3 blockSize4(blocksToLaunch, dataHolder.classifier.h_J,1);

		updateBatchDependentArraySizeValues(dataHolder.classifier.h_batchSizes[batchNum]);
		setUpBatchDependentHostArrays(batchNum, resultsHolder);

		if(dataHolder.implementation==1) {
			copyBatchDependentDataToGPU(dataHolder.classifier.h_q, batchNum, resultsHolder);
		}
		else if (dataHolder.implementation==2) {
			copyBatchDependentDataToGPU_ocl(dataHolder.classifier.h_q, batchNum, resultsHolder);
		}

		if (dataHolder.verbose != 0) {
			cout << "Calculating scores ...." << endl;
		}

		if (dataHolder.h_method_ref == 2) {
			if(dataHolder.classifier.h_numOfBatches>1 || !alreadyRanCalculateCommonNbFeatures) {
				calculateCommonFeaturesNbWrapper(dataHolder, resultsHolder, blockSize3, defaultDim, blockSize2, threadsToLaunch3);
				alreadyRanCalculateCommonNbFeatures=true;
			}

			calculateScoresNbWrapper(dataHolder, resultsHolder, blockSize2, threadSize);

		} else if(dataHolder.h_method_ref==1 || dataHolder.h_method_ref==0) {
			// launch the scores kernel

			calculateScoresWrapper(dataHolder, resultsHolder, blockSize2, threadSize, blockSize3);

			scaleScoresForClassSizeWrapper(dataHolder, resultsHolder, blockSize2, threadSize);

		}

		if (dataHolder.classifierMethod == 0) {
			if (dataHolder.verbose != 0) {
				cout << "Calculating class votes on prediction count basis ...." << endl;
			}
			calculateClassVotesPredictionCountWrapper(dataHolder, resultsHolder, blockSize2,defaultDim);

		} else if (dataHolder.classifierMethod == 1) {
			if (dataHolder.verbose != 0) {
				cout << "Calculating class votes on cummulative scores basis ...." << endl;
			}
			calculateClassVotesCummulativeScoresWrapper(dataHolder, resultsHolder, blockSize2, defaultDim);
		}

		makeClassVotesCummulativeWrapper(dataHolder, resultsHolder, blockSize2, defaultDim);

		if (dataHolder.verbose != 0) {
			cout << "Calculating class probabilities ...." << endl;
		}

		calculateClassProbabilitiesWrapper(dataHolder, resultsHolder, blockSize2, defaultDim);

		if (dataHolder.testCase) {
			if(dataHolder.implementation==1) {
				if(dataHolder.h_method_ref==0 || dataHolder.h_method_ref==2) {
					resultsHolder.h_scores_float = copyFloatArrayToHost(resultsHolder.h_scores_float, resultsHolder.d_scores_float, SCORES_FLOAT_ARRAY_BYTES);
					resultsHolder.h_class_votes_float = copyFloatArrayToHost(resultsHolder.h_class_votes_float, resultsHolder.d_class_votes_float, CLASS_VOTES_FLOAT_ARRAY_BYTES);
				} else {
					resultsHolder.h_scores = copyDoubleArrayToHost(resultsHolder.h_scores, resultsHolder.d_scores, SCORES_ARRAY_BYTES);
					resultsHolder.h_class_votes = copyDoubleArrayToHost(resultsHolder.h_class_votes, resultsHolder.d_class_votes, CLASS_VOTES_ARRAY_BYTES);
				}

				dataHolder.dataTest.h_numOfComparesByClass = copyIntArrayToHost(dataHolder.dataTest.h_numOfComparesByClass, dataHolder.dataTest.d_numOfComparesByClass,
						dataHolder.dataTest.NUM_COMPARES_ARRAY_BYTES);

			}else if (dataHolder.implementation==2) {
				if(dataHolder.h_method_ref==0 || dataHolder.h_method_ref==2) {
					//resultsHolder.h_scores_float = copyFloatArrayToHost(resultsHolder.h_scores_float, resultsHolder.d_scores_float, SCORES_FLOAT_ARRAY_BYTES);
					clEnqueueReadBuffer(dataHolder.clcmdq,resultsHolder.d_scores_float_ocl,CL_TRUE,0,SCORES_FLOAT_ARRAY_BYTES, resultsHolder.h_scores_float,0,NULL,NULL);
					//resultsHolder.h_class_votes_float = copyFloatArrayToHost(resultsHolder.h_class_votes_float, resultsHolder.d_class_votes_float, CLASS_VOTES_FLOAT_ARRAY_BYTES);
					clEnqueueReadBuffer(dataHolder.clcmdq,resultsHolder.d_class_votes_float_ocl,CL_TRUE,0,CLASS_VOTES_FLOAT_ARRAY_BYTES, resultsHolder.h_class_votes_float,0,NULL,NULL);
				} else {
					//resultsHolder.h_scores = copyDoubleArrayToHost(resultsHolder.h_scores, resultsHolder.d_scores, SCORES_ARRAY_BYTES);
					clEnqueueReadBuffer(dataHolder.clcmdq,resultsHolder.d_scores_ocl,CL_TRUE,0,SCORES_ARRAY_BYTES, resultsHolder.h_scores,0,NULL,NULL);
					//resultsHolder.h_class_votes = copyDoubleArrayToHost(resultsHolder.h_class_votes, resultsHolder.d_class_votes, CLASS_VOTES_ARRAY_BYTES);
					clEnqueueReadBuffer(dataHolder.clcmdq,resultsHolder.d_class_votes_ocl,CL_TRUE,0,CLASS_VOTES_ARRAY_BYTES, resultsHolder.h_class_votes,0,NULL,NULL);
				}
				//dataHolder.dataTest.h_numOfComparesByClass = copyIntArrayToHost(dataHolder.dataTest.h_numOfComparesByClass, dataHolder.dataTest.d_numOfComparesByClass, dataHolder.dataTest.NUM_COMPARES_ARRAY_BYTES);
				clEnqueueReadBuffer(dataHolder.clcmdq,dataHolder.dataTest.d_numOfComparesByClass_ocl,CL_TRUE,0,dataHolder.dataTest.NUM_COMPARES_ARRAY_BYTES, dataHolder.dataTest.h_numOfComparesByClass,0,NULL,NULL);
			}

		}

		//dataHolder.dataTest.h_common_features_nb = copyIntArrayToHost(dataHolder.dataTest.h_common_features_nb, dataHolder.dataTest.d_common_features_nb,
		//		COMMON_FEATURES_NB_ARRAY_BYTES);

		//for(int i=0; i<32; i++)
		//{
		//	cout << i << "," << dataHolder.dataTest.h_common_features_nb[i] << endl;
		//}


		if(dataHolder.implementation==1) {
			resultsHolder.h_class_prob = copyFloatArrayToHost(resultsHolder.h_class_prob, resultsHolder.d_class_prob, CLASS_PROB_ARRAY_BYTES);

		}
		else if (dataHolder.implementation==2) {
			//resultsHolder.h_class_prob = copyFloatArrayToHost(resultsHolder.h_class_prob, resultsHolder.d_class_prob, CLASS_PROB_ARRAY_BYTES);
			clEnqueueReadBuffer(dataHolder.clcmdq,resultsHolder.d_class_prob_ocl,CL_TRUE,0,CLASS_PROB_ARRAY_BYTES, resultsHolder.h_class_prob,0,NULL,NULL);
		}
		updateMasterClassProb(batchNum, resultsHolder);

		calculateConfusionMatrixWrapper(dataHolder, resultsHolder, blockSize2, defaultDim);

		if (dataHolder.verbose != 0) {
				cout << "Calculating confusion matrix ...." << endl;
		}

		if(dataHolder.implementation==1) {
			if(dataHolder.h_method_ref==0 || dataHolder.h_method_ref==2) {
				cudaFree(resultsHolder.d_scores_float);
				cudaFree(resultsHolder.d_class_votes_float);
			} else {
				cudaFree(resultsHolder.d_scores);
				cudaFree(resultsHolder.d_class_votes);
			}
			cudaFree(resultsHolder.d_class_prob);
		}
		else if (dataHolder.implementation==2) {
			if(dataHolder.h_method_ref==0 || dataHolder.h_method_ref==2) {
				clReleaseMemObject(resultsHolder.d_scores_float_ocl);
				clReleaseMemObject(resultsHolder.d_class_votes_float_ocl);
			} else {
				clReleaseMemObject(resultsHolder.d_scores_ocl);
				clReleaseMemObject(resultsHolder.d_class_votes_ocl);
			}
			clReleaseMemObject(resultsHolder.d_class_prob_ocl);
		}
		//if only one batch is being run then these values can be retained in global memory on the card for the
		//next q value
		if(dataHolder.classifier.h_numOfBatches>1) {
			if(dataHolder.implementation==1) {
				cudaFree(dataHolder.dataTest.d_common_features_nb);
			}
			else if (dataHolder.implementation==2) {
				clReleaseMemObject(dataHolder.dataTest.d_common_features_nb_ocl);
			}
			alreadyRanCalculateCommonNbFeatures=false;
		}
	}

	if(dataHolder.implementation==1) {
		resultsHolder.h_confusion_matrix = copyIntArrayToHost(resultsHolder.h_confusion_matrix,	resultsHolder.d_confusion_matrix, CONFUSION_MATRIX_ARRAY_BYTES);
		cudaFree(resultsHolder.d_confusion_matrix);
	}
	else if (dataHolder.implementation==2) {
		clEnqueueReadBuffer(dataHolder.clcmdq,resultsHolder.d_confusion_matrix_ocl,CL_TRUE,0,CONFUSION_MATRIX_ARRAY_BYTES, resultsHolder.h_confusion_matrix,0,NULL,NULL);
		clReleaseMemObject(resultsHolder.d_confusion_matrix_ocl);
	}
}

void ProbClassifierManager::runSerialKernels(ResultsHolder& resultsHolder) {

	for(int batchNum=0; batchNum < dataHolder.classifier.h_numOfBatches; batchNum++) {
		dataHolder.classifier.h_currentBatchNum = batchNum;
		dataHolder.classifier.h_currentBatchStartPos = dataHolder.classifier.h_batchStartPositions[batchNum];
		dataHolder.classifier.h_currentBatchSize = dataHolder.classifier.h_batchSizes[batchNum];
		updateBatchDependentArraySizeValues(dataHolder.classifier.h_batchSizes[batchNum]);
		setUpBatchDependentHostArrays(batchNum, resultsHolder);

		serialKernels.setConstants(dataHolder);

		if (dataHolder.verbose != 0) {
			cout << "Calculating scores ...." << endl;
		}
		if (dataHolder.h_method_ref == 2) {
			serialKernels.calculateCommonFeaturesNb(dataHolder);
			serialKernels.calculateScoresNb(dataHolder, resultsHolder);
		}
		else if(dataHolder.h_method_ref==1 || dataHolder.h_method_ref==0)
		{
			serialKernels.calculateScores(dataHolder, resultsHolder);

			if(dataHolder.h_method_ref==0){
				serialKernels.scaleFloatScoresForClassSize(dataHolder, resultsHolder);
			}else{
				serialKernels.scaleScoresForClassSize(dataHolder, resultsHolder);
			}
		}

		if (dataHolder.classifierMethod == 0) {
			if (dataHolder.verbose != 0) {
				cout << "Calculating class votes on prediction count basis ...." << endl;
			}

			if(dataHolder.h_method_ref==0 || dataHolder.h_method_ref==2){
				serialKernels.calculateClassVotesFloatPredictionCount(dataHolder, resultsHolder);
			}else{
				serialKernels.calculateClassVotesPredictionCount(dataHolder, resultsHolder);
			}
		} else if (dataHolder.classifierMethod == 1) {
			if (dataHolder.verbose != 0) {
				cout << "Calculating class votes on cummulative scores basis ...." << endl;
			}

			if(dataHolder.h_method_ref==0 || dataHolder.h_method_ref==2){
				serialKernels.calculateClassVotesFloatCummulativeScores(dataHolder, resultsHolder);
			}
			else{
				serialKernels.calculateClassVotesCummulativeScores(dataHolder, resultsHolder);
			}
		}

		if(dataHolder.h_method_ref==0 || dataHolder.h_method_ref==2){
			serialKernels.makeClassVotesFloatCummulative(dataHolder, resultsHolder);
			serialKernels.calculateClassProbabilitiesFloat(dataHolder, resultsHolder);
		}else{
			serialKernels.makeClassVotesCummulative(dataHolder, resultsHolder);
			serialKernels.calculateClassProbabilities(dataHolder, resultsHolder);
		}
		if (dataHolder.verbose != 0) {
			cout << "Calculating class probabilities ...." << endl;
		}

		updateMasterClassProb(batchNum, resultsHolder);

		if (dataHolder.verbose != 0) {
			cout << "Calculating confusion matrix ...." << endl;
		}
		serialKernels.calculateConfusionMatrix(dataHolder, resultsHolder);
	}

}

void ProbClassifierManager::calculateCorrelation(ResultsHolder& resultsHolder){
	CorrelationCalculator correlationCalculator;
	correlationCalculator.calculateMcc(dataHolder, resultsHolder);
}

void ProbClassifierManager::printArrays(ResultsHolder& resultsHolder, DataHolder& dataHolder){
	//dataHolder.dataTest.print_h_in();
	//dataHolder.dataTest.print_h_inNormalised();
	//dataHolder.dataTest.print_h_numOfComparesByClass();
	//dataHolder.print_h_in_combo();
	//resultsHolder.print_h_scores(dataHolder);
	//resultsHolder.print_h_class_votes(dataHolder);
	//resultsHolder.print_h_class_prob(dataHolder);
	resultsHolder.print_confusion_matrix(dataHolder);
}

int ProbClassifierManager::calculateMethodRef(string currentMethodName)
{
	int h_method_ref;

	if (currentMethodName == "rascal") {
		h_method_ref = 0;
	} else if (currentMethodName == "prw") {
		h_method_ref = 1;
	} else if (currentMethodName == "nb") {
		h_method_ref = 2;
	} else if (currentMethodName == "dascal") {
		h_method_ref = 3;
	}
	return h_method_ref;
}

void ProbClassifierManager::freeGpuMemoryForSubSampleIndependentData() {
	cudaFree(dataHolder.dataTest.d_classMember);
	cudaFree(dataHolder.dataTest.d_kfoldMember);
	cudaFree(dataHolder.dataTest.d_in);
	cudaFree(dataHolder.dataTest.d_inSparseVectorBreakPoints);
	cudaFree(dataHolder.dataTrain.d_classMember);
	cudaFree(dataHolder.dataTrain.d_kfoldMember);
	cudaFree(dataHolder.dataTrain.d_in);
	cudaFree(dataHolder.dataTrain.d_inSparseVectorBreakPoints);
	if(dataHolder.classifier.h_numOfBatches==1)
	{
		cudaFree(dataHolder.dataTest.d_common_features_nb);
	}
	cudaFree(dataHolder.dataTest.d_numOfComparesByClass);
	cudaFree(dataHolder.d_randoms);
}

void ProbClassifierManager::freeGpuMemoryForSubSampleIndependentData_ocl() {

	clReleaseMemObject(dataHolder.dataTest.d_classMember_ocl);
	clReleaseMemObject(dataHolder.dataTest.d_kfoldMember_ocl);
	clReleaseMemObject(dataHolder.dataTest.d_in_ocl);
	if(dataHolder.dataFormat==1){
		clReleaseMemObject(dataHolder.dataTest.d_inSparseVectorBreakPoints_ocl);
	}
	if(dataHolder.samplingMethod==2)
	{
		clReleaseMemObject(dataHolder.dataTrain.d_classMember_ocl);
		clReleaseMemObject(dataHolder.dataTrain.d_kfoldMember_ocl);
		clReleaseMemObject(dataHolder.dataTrain.d_in_ocl);
		if(dataHolder.dataFormat==1){
			clReleaseMemObject(dataHolder.dataTrain.d_inSparseVectorBreakPoints_ocl);
		}
	}
	if(dataHolder.classifier.h_numOfBatches==1 && dataHolder.runNb)
	{
		clReleaseMemObject(dataHolder.dataTest.d_common_features_nb_ocl);
	}
	clReleaseMemObject(dataHolder.dataTest.d_numOfComparesByClass_ocl);
	clReleaseMemObject(dataHolder.d_randoms_ocl);

}

void ProbClassifierManager::printBestClassificationResults() {
	for (it_type iterator = resultsHolderMap.begin();
				iterator != resultsHolderMap.end(); iterator++) {
		string currentMethod = iterator->first;
		//cout << "currentMethod: " << currentMethod << endl;
		ResultsHolder currentResultsHolder = iterator->second;

		if (!dataHolder.testCase) {
			currentResultsHolder.printMccBest(dataHolder);
		//printMccBest(bestMccResultsPath, currentResultsHolder.classProbBest,
		//		&dataHolder.dataTrain.h_classIdVector, &dataHolder.dataTrain.h_itemIdVector,
		//		&dataHolder.dataTrain.h_classIdKeySetVector, dataHolder.dataTrain.N, dataHolder.dataTrain.classes,
		//		currentResultsHolder.qBest, currentResultsHolder.jBest);
		}
	}
}

void ProbClassifierManager::runClassifiersForQvalue(){
	//loop for each classifier type
	for (it_type iterator=resultsHolderMap.begin(); iterator!=resultsHolderMap.end(); iterator++) {
		dataHolder.currentMethodName = iterator->first;
		dataHolder.h_method_ref = calculateMethodRef(dataHolder.currentMethodName);

		if (dataHolder.verbose != 0) {
			cout << "Running classifier " << dataHolder.currentMethodName << ", q=" << dataHolder.classifier.h_q << endl;
		}

		ResultsHolder& currentResultsHolder = iterator->second;
		setUpBatchIndependentHostArrays(currentResultsHolder);

		if(dataHolder.implementation == 1 || dataHolder.implementation ==2) {
			runParallelKernels(currentResultsHolder);
		}
		else if(dataHolder.implementation == 0) {
			runSerialKernels(currentResultsHolder);
		}
		if (dataHolder.verbose != 0) {
			printArrays(currentResultsHolder, dataHolder);
		}

		calculateCorrelation(currentResultsHolder);
		currentResultsHolder.updateResultsHolder(dataHolder);

	}
	if (dataHolder.implementation == 1) {
		cudaFree(dataHolder.d_in_combo);
		//checkCudaErrorCodeWrapper();
	}
	else if (dataHolder.implementation == 2) {
		clReleaseMemObject(dataHolder.d_in_combo_ocl);
	}

}

void ProbClassifierManager::runClassifier() {

	generateResultsHolders();
	allocateHostArrays();
	cudaEvent_t start, stop;
	if(dataHolder.implementation==1) {

		cudaEventCreate(&start);
		cudaEventCreate(&stop);
		cudaEventRecord(start, 0);
	}
	runQindependentKernels();

	//copyIntArrayToHost(dataHolder.dataTest.h_numOfComparesByClass, dataHolder.dataTest.d_numOfComparesByClass,
	//								dataHolder.dataTest.NUM_COMPARES_ARRAY_BYTES);
	//dataHolder.dataTest.print_h_numOfComparesByClass();

	if(dataHolder.verbose!=0) {
		cout << "q independent memory: " << qIndependentMemoryRequired << endl;
	}

	// Todo: q loop is here
	for (int h_q = dataHolder.q_min; h_q <= dataHolder.q_max; h_q += dataHolder.q_step)	{
		dataHolder.classifier.h_q=h_q;

		if (dataHolder.verbose != 0) {
			cout << "Running classifiers for Q = " << dataHolder.classifier.h_q << endl;
		}

		updateValuesForJ();
		updateBatchIndependentArraySizeValues();
		calculateNumOfBatches();

		generateBatchData();

		generateRandomFeaturesForSubSamples();

		runClassifiersForQvalue();
	}

	if (dataHolder.implementation == 1) {
		freeGpuMemoryForSubSampleIndependentData();
	}
	else if (dataHolder.implementation == 2) {
		freeGpuMemoryForSubSampleIndependentData_ocl();
	}

	float elapsedTime=0;

	if(dataHolder.implementation==1) {
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&elapsedTime, start, stop);
	}

	if (dataHolder.verbose != 0) {
		cout << "\nElapsed time: " << elapsedTime << endl;
	}

	if(!dataHolder.testCase) {
		printBestClassificationResults();
	}

}


ProbClassifierManager::ProbClassifierManager(DataHolder& data) : dataHolder(data){
	// TODO Auto-generated constructor stub
	qDependentMemoryRequired = 0.0;
	qIndependentMemoryRequired = 0.0;
	//default is one batch

	alreadyRanCalculateCommonNbFeatures=false;

}

ProbClassifierManager::~ProbClassifierManager() {
	// TODO Auto-generated destructor stub
}

