/*
 * CorrelationCalculator.cpp
 *
 *  Created on: 1 Jul 2013
 *      Author: jdt42
 */

#include "CorrelationCalculator.hpp"
#include <stdio.h>
#include <math.h>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/io.hpp>
#include "ResultsHolder.hpp"
#include "DataHolder.hpp"

using namespace boost::numeric::ublas;

void CorrelationCalculator::calculateMcc(DataHolder& dataHolder, ResultsHolder& resultsHolder) {

	int h_classes = dataHolder.dataTrain.classes;
	int h_numOfSteps = dataHolder.classifier.h_J_numOfSteps;

	for(int step=0; step < h_numOfSteps; step++)
	{
		double mcc=0.0;
		if(h_classes==2)
		{
			double TP = (double) resultsHolder.h_confusion_matrix[step * h_classes * h_classes + 3] * 1.0;
			double FP = (double) resultsHolder.h_confusion_matrix[step * h_classes * h_classes + 1] * 1.0;
			double TN = (double) resultsHolder.h_confusion_matrix[step * h_classes * h_classes + 0] * 1.0;
			double FN = (double) resultsHolder.h_confusion_matrix[step * h_classes * h_classes + 2] * 1.0;

			double denomBeforePower = (TP+FP)*(TP+FN)*(TN+FP)*(TN+FN);
			if(denomBeforePower!=0.0)
			{
				mcc = ((TP*TN) - (FP*FN)) / pow(denomBeforePower,0.5);
			}
		}
		else
		{
			matrix<double> confusionMatrix (h_classes, h_classes);
			matrix<double> confusionMatrixTranspose (h_classes, h_classes);
			double trace;
			double total;

			for(int i=0; i<h_classes; i++)
			{
				for(int j=0; j<h_classes; j++)
				{
					confusionMatrix(i,j) = (double) resultsHolder.h_confusion_matrix[step * h_classes * h_classes + i*h_classes + j];
					total = total + confusionMatrix(i,j);
					if(i==j)
					{
						trace = trace + confusionMatrix(i,j);
					}
				}
			}

			confusionMatrixTranspose = trans(confusionMatrix);

			double numSum = 0.0;
			double denom1 = 0.0;
			double denom2 = 0.0;
			double denomSum1 = 0.0;
			double denomSum2 = 0.0;
			double denominator = 0.0;
			double numerator = 0.0;

			for(int i=0; i<h_classes; i++)
			{
				for(int j=0; j<h_classes; j++)
				{
					matrix_row<matrix<double> > aRow(confusionMatrix, i);
					matrix_column<matrix<double> > aColumn(confusionMatrix, j);

					for(int k=0; k<h_classes; k++)
					{
						numSum = numSum + aRow[k] * aColumn[k];
					}
				}
			}


			for(int i=0; i<h_classes; i++)
			{
				for(int j=0; j<h_classes; j++)
				{
					matrix_row<matrix<double> > aRow(confusionMatrix, i);
					matrix_column<matrix<double> > aColumn(confusionMatrixTranspose, j);

					for(int k=0; k<h_classes; k++)
					{
						denomSum1 = denomSum1 + aRow[k] * aColumn[k];
					}
				}
			}

			for(int i=0; i<h_classes; i++)
			{
				for(int j=0; j<h_classes; j++)
				{
					matrix_row<matrix<double> > aRow(confusionMatrixTranspose, i);
					matrix_column<matrix<double> > aColumn(confusionMatrix, j);

					for(int k=0; k<h_classes; k++)
					{
						denomSum2 = denomSum2 + aRow[k] * aColumn[k];
					}
				}
			}
			numerator = total * trace - numSum;
			denom1 = pow((total*total - denomSum1),0.5);
			denom2 = pow((total*total - denomSum2),0.5);
			denominator = denom1 * denom2;
			if(denominator!=0.0)
			{
				mcc = numerator/denominator;
			}
		}
		resultsHolder.h_mcc_results[step] = mcc;
	}
}


CorrelationCalculator::CorrelationCalculator() {
	// TODO Auto-generated constructor stub

}

CorrelationCalculator::~CorrelationCalculator() {
	// TODO Auto-generated destructor stub
}

