/*
 * ProbClassifierManager.h
 *
 *  Created on: 30 Aug 2013
 *      Author: jdt42
 */

#ifndef PROBCLASSIFIERMANAGER_H_
#define PROBCLASSIFIERMANAGER_H_

#include <string>
#include "DataHolder.hpp"
#include "ResultsHolder.hpp"
#include "SerialKernels.hpp"

using namespace std;

class ProbClassifierManager {
public:

	/**
	 * DataHolder object
	 */
	DataHolder& dataHolder;
	/**
	 * Map of classifier type as a key to the ResultsHolder object
	 */
	map<string, ResultsHolder> resultsHolderMap;



	/**
	 * Host variable of number of threads to launch
	 */
	int h_threadsToLaunch;

	/**
	 * Contains the block size for running the kernels, blocksToLaunch x 1 x 1
	 */


	/**
	 * Runs the classifers
	 */
	void runClassifier();

	/**
	 * Generates the results holders using generateResultsHolder
	 */
	void generateResultsHolders();

	/**
	 * Populates host arrays
	 */
	void allocateHostArrays();

	/**
	 * Set up run specific host arrays
	 */
	void setUpBatchIndependentHostArrays(ResultsHolder&);

	/**
	 * Copies Host data that is independent of the subsample to the GPU via cuda
	 */
	void copyQindependentHostDataToGPU();

	/**
	 * Copies Host data that is independent of the subsample to the GPU via opencl
	 */
	void copyQindependentHostDataToGPU_opencl();

	/**
	 * Checks that the user defined minimum and maximum values for j are relevant for the
	 * combination of q and L.  The number of threads is aligned to j, so the method calculates
	 * the number of threads to launch as the minimum of j and the gpu specific max number of threads
	 */
	void updateValuesForJ();

	/**
	 * Copes run specific data to GPU
	 */
	void copyBatchIndependentDataToGPU(ResultsHolder&);

	/**
	 * Copes run specific data to GPU with opencl
	 */
	void copyBatchIndependentDataToGPU_ocl(ResultsHolder&);

	/**
	 * Generates the random selection of features for each subsample.  The number of random features required
	 * is q by j
	 */
	void generateRandomFeaturesForSubSamples();

	/**
	 * Calculates the array size values for use in generating arrays
	 */
	void updateBatchDependentArraySizeValues(int);

	/**
	 * Generates a results holder for a particular classifier
	 */
	void generateResultsHolder(string, const int);

	/**
	 * Runs the kernels that are independent of the subsample q.  These are the kernels to calculate the number
	 * of compares for each data item to each data class and for naive bayesian the kernel to calculate the number
	 * of times each feature for each data item appears in each class.
	 */
	void runQindependentKernels();

	/**
	 *  Run the kernels on the GPU
	 */
	void runParallelKernels(ResultsHolder&);

	/**
	 * Run the kernels on the CPU
	 */
	void runSerialKernels(ResultsHolder&);

	/**
	 * Calculates the matthews correlation coefficient for each of the scenarios sampled in the j runs
	 */
	void calculateCorrelation(ResultsHolder&);

	/**
	 * prints various arrays to stdout
	 */
	void printArrays(ResultsHolder&, DataHolder&);

	/**
	 * Helper method to return method ref
	 */
	int calculateMethodRef(string);

	/**
	 * Frees GPU memory for subsample independent data cuda
	 */
	void freeGpuMemoryForSubSampleIndependentData();

	/**
	 * Frees GPU memory for subsample independent data opencl
	 */
	void freeGpuMemoryForSubSampleIndependentData_ocl();

	/**
	 * For each classifier being run prints a data file listing the classification results for each data item.
	 */
	void printBestClassificationResults();

	/**
	 * Allocates memory, copies an integer to the GPU and returns a pointer
	 */
	int* copyIntToGPU(int);

	/**
	 * Copies an integer array to the host
	 */
	int* copyIntArrayToHost(int *, int *, const int);

	/**
	 * Allocates memory, copies a double to the GPU and returns a pointer
	 */
	double* copyDoubleToGPU(double);
	/**
	 * Copies a double array to host
	 */
	double* copyDoubleArrayToHost(double *, double *, const int);

	/**
	 * Copies a float array to host
	 */
	float* copyFloatArrayToHost(float *h_doubleArray, float *d_doubleArray,
			const int BYTES);

	/**
	 * Allocates memory, copies a float array to the GPU and returns a pointer
	 */
	float* copyFloatArrayToGPU(float *, const int);
	/**
	 * Allocates memory, copies a double array to the GPU and returns a pointer
	 */
	double* copyDoubleArrayToGPU(double *, const int);

	/**
	 * Allocates memory, copies an integer array to the GPU and returns a pointer
	 */
	int* copyIntArrayToGPU(int *, const int);

	/**
	 * Calculates the number of batches required
	 */
	void calculateNumOfBatches();

	/**
	 *  Sets up arrays to list the batch sizes and vector starting positions.
	 */
	void generateBatchData();

	/**
	 * Calculates the array sizes for arrays that are batch independent
	 */
	void updateBatchIndependentArraySizeValues();

	/**
	 * Copies batch independent data to the gpu with cuda
	 */
	void copyBatchDependentDataToGPU(int, int, ResultsHolder&);

	/**
	 * Copies batch independent data to the gpu with opencl
	 */
	void copyBatchDependentDataToGPU_ocl(int, int, ResultsHolder&);


	/**
	 * Sets up the batch dependent host arrays
	 */
	void setUpBatchDependentHostArrays(int, ResultsHolder&);
	/**
	 * Updates the master probabbility array to collect results for all data points in all batches
	 */
	void updateMasterClassProb(int, ResultsHolder&);
	/**
	 * Runs the classifiers selected for a given q value after setting up input data
	 */
	void runClassifiersForQvalue();

	/**
	 * For a fixed training set calculates the number of 1's in each class by feature
	 */
	void calculateFeatureCountByClass();

	/**
	 * References the total global memory needed to run the clasifiers.  This is used to work out the
	 * number of batches required by reference to the total memory available
	 */
	long qIndependentMemoryRequired;
	/**
	 * References the global memory required to store the q and batch independent data
	 */
	long qDependentMemoryRequired;

	/**
	 * References serial kernels
	 */
	SerialKernels serialKernels;



	/**
		 * References the length of the confusion matrix.  A matrix is generated for each of the j
		 * steps requested by the user
		 */
		int CONFUSION_MATRIX_ARRAY_SIZE;
		/**
		 * References the size of the confusion matrix.  A matrix is generated for eacg of the j
		 * steps requested by the user
		 */
		int CONFUSION_MATRIX_ARRAY_BYTES;

		/**
		 * References the length of the array containing the MCC results for each of the j steps
		 * at the q value currently selected.
		 */
		int MCC_RESULTS_ARRAY_SIZE;
		/**
		 * References the size of the array containing the MCC results for each of the j steps
		 * at the q value currently selected.
		 */
		int MCC_RESULTS_ARRAY_BYTES;
		/**
		 * References the length of the array collecting all the probability results for all data
		 * points for all batches.
		 *
		 */
		int CLASS_PROB_MASTER_ARRAY_SIZE;
		/**
		 * References the size of the array collecting all the probability results for all data
		 * points for all batches.
		 */
		int CLASS_PROB_MASTER_ARRAY_BYTES;
		/**
		 * References the length of the array to hold the scores for each feature vector for
		 * each class for each random selection of features.
		 */
		int SCORES_ARRAY_SIZE;
		/**
		 * References the size of the array to hold the scores for each feature vector for
		 * each class for each random selection of features.
		 */
		int SCORES_ARRAY_BYTES;
		/**
		 * References the size of the floating point array to hold the scores for each feature vector for
		 * each class for each random selection of features.
		 */
		int SCORES_FLOAT_ARRAY_BYTES;
		/**
		 * References the length of the of the array storing the votes for each class for each
		 * data item.  The votes are either on prediction count or cummulative scores.  The votes
		 * for each class are accumulated by the number of j steps so that each scenario requested
		 * by the user generates a result
		 */
		int CLASS_VOTES_ARRAY_SIZE;
		/**
		 * References the size of the of the array storing the votes for each class for each
		 * data item.  The votes are either on prediction count or cummulative scores.  The votes
		 * for each class are accumulated by the number of j steps so that each scenario requested
		 * by the user generates a result
		 */
		int CLASS_VOTES_ARRAY_BYTES;
		/**
		 * References the size of the of the array storing the votes for each class for each
		 * data item.  The votes are either on prediction count or cummulative scores.  The votes
		 * for each class are accumulated by the number of j steps so that each scenario requested
		 * by the user generates a result
		 */
		int CLASS_VOTES_FLOAT_ARRAY_BYTES;

		/**
		 * References the length of the array showing the probability of each data item belonging
		 * to each class.  This is calculated as the votes for each class divided by the total
		 * votes.  The results are accumulated for each of the j steps requested by the user.
		 */
		int CLASS_PROB_ARRAY_SIZE;
		/**
		 * References the size of the array showing the probability of each data item belonging
		 * to each class.  This is calculated as the votes for each class divided by the total
		 * votes.  The results are accumulated for each of the j steps requested by the user.
		 */
		int CLASS_PROB_ARRAY_BYTES;


		/**
		 * References the size of the arrays containing information about the batch sizes
		 * and batch start positions
		 */
		int BATCH_ARRAY_BYTES;

		/**
		 * Length of array showing number of times each feature in each feature vector appears with
		 * that value in each class
		 */
		int COMMON_FEATURES_NB_ARRAY_SIZE;
		/**
		 * Size of array showing number of times each feature in each feature vector appears with
		 * that value in each class
		 */
		int COMMON_FEATURES_NB_ARRAY_BYTES;






		/**
		 * If only one batch is being run then this boolean allows cod eto be run only once giving a time saving for the
		 * naive bayesian.
		 */
		bool alreadyRanCalculateCommonNbFeatures;

	/**
	 * A type def for use in loop structures
	 */
	typedef std::map<std::string, ResultsHolder>::iterator it_type;

	ProbClassifierManager(DataHolder&);
	virtual ~ProbClassifierManager();
};

#endif /* PROBCLASSIFIERMANAGER_H_ */
