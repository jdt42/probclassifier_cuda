#include "printCudaErrorCode.hpp"

using namespace std;

void printCudaErrorCode(string msg, cudaError_t error) {
	cout << msg << cudaGetErrorString(error) << endl;

}

void print(string msg) {
	cout << "***** " << msg << endl;

}
