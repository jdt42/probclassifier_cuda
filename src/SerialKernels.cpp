/*
 * SerialKernels.cpp
 *
 *  Created on: 2 Sep 2013
 *      Author: jdt42
 */

#include "SerialKernels.hpp"
#include "DataHolder.hpp"
#include <math.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

void SerialKernels::setConstants(DataHolder& dataHolder)
{
	batchSize = dataHolder.classifier.h_currentBatchSize;
	batchStartPosition = dataHolder.classifier.h_currentBatchStartPos;
	Q = dataHolder.classifier.h_q;
	J = dataHolder.classifier.h_J;
	J_min = dataHolder.classifier.h_J_min;
	J_step = dataHolder.classifier.h_J_step;
	J_numOfSteps = dataHolder.classifier.h_J_numOfSteps;
}

void SerialKernels::setQindependentConstants(DataHolder& dataHolder)
{
	prw_param = dataHolder.prwParam;
	N_tr = dataHolder.dataTrain.N;
	N_ts = dataHolder.dataTest.N;
	L = dataHolder.dataTrain.L;
	classes = dataHolder.dataTrain.classes;
}

void SerialKernels::calculateComparesByClass(DataHolder& dataHolder)
{
	for(int rootId=0; rootId<N_ts; rootId++){
		int rootKfoldRef = dataHolder.dataTest.h_kfoldMember[rootId];

		for(int targetId=0; targetId<N_tr; targetId++){
			int targetClassId = dataHolder.dataTrain.h_classMember[targetId];
			int targetKfoldRef = dataHolder.dataTrain.h_kfoldMember[targetId];
			//only proceed if root and target are from different parts of the kfold split
			if(rootKfoldRef!=targetKfoldRef){
				//count the number of target feature vectors each root feature vector is compared against for each class so we can scale the score
				dataHolder.dataTest.h_numOfComparesByClass[rootId*classes + targetClassId]++;
			}
		}
	}
}

void SerialKernels::calculateCommonFeaturesNb(DataHolder& dataHolder)
{
	for(int rootId=0; rootId<batchSize; rootId++) {
		int rootKfoldRef = dataHolder.dataTest.h_kfoldMember[rootId];
		//cout << "root: " << rootId << "," << rootKfoldRef << endl;
		for(int targetId=0; targetId<N_tr; targetId++) {
			int classId = dataHolder.dataTrain.h_classMember[targetId];
			int targetKfoldRef = dataHolder.dataTrain.h_kfoldMember[targetId];

			//only proceed if root and target are from different parts of the kfold split
			if(rootKfoldRef!=targetKfoldRef) {
				//cout << "target: " << targetId << "," << targetKfoldRef << endl;
				for(int featureNum=0; featureNum<L; featureNum++) 	{
					int featureRoot = dataHolder.dataTest.h_in[rootId*L + featureNum];
					int featureTarget = dataHolder.dataTrain.h_in[targetId*L + featureNum];

					if(featureRoot == featureTarget) {
						//cout << "featureNum: " << featureNum << ", " << featureRoot << ", " << featureTarget << endl;
						//cout << "before: " << dataHolder.dataTest.h_common_features_nb[rootId*L*classes + classId*L + featureNum];
						dataHolder.dataTest.h_common_features_nb[rootId*L*classes + classId*L + featureNum]++;
						//cout << "after: " << dataHolder.dataTest.h_common_features_nb[rootId*L*classes + classId*L + featureNum];
					}
				}
			}
		}
	}
	/**
	for(int i=0; i<batchSize; i++){
		cout << "rootId: " << i << endl;
		for(int k=0; k<classes; k++){
			cout << "classId: " << k << endl;
			for(int j=0; j<L; j++){
				cout << dataHolder.dataTest.h_common_features_nb[i*L*classes + k*L + j];
			}
			cout << endl;
		}
		cout << endl;
	}
	**/
}

void SerialKernels::calculateScoresNb(DataHolder& dataHolder, ResultsHolder& resultsHolder)
{
	for(int rootId = 0; rootId<batchSize; rootId++){
		for(int comboId =0; comboId<J; comboId++)	{
			for(int classId = 0; classId<classes; classId++){
				float score = 1.0;
				float numOfCompares = (float) dataHolder.dataTest.h_numOfComparesByClass[rootId*classes + classId];

				for(int i=0; i<Q; i++) {
					int featureNum = dataHolder.h_in_combo[comboId*Q + i];
					score = score * ((dataHolder.dataTest.h_common_features_nb[rootId*L*classes + classId*L + featureNum]+1.0)/(numOfCompares+2.0));
				}

				resultsHolder.h_scores_float[rootId*classes*J + classes*comboId + classId] = resultsHolder.h_scores_float[rootId*classes*J + classes*comboId + classId] + score;
			}
		}
	}
}


/*
 * The scores in d_scores are totals and need to be divided by the count stored in d_numOfComparesByClass
 */
void SerialKernels::scaleScoresForClassSize(DataHolder& dataHolder, ResultsHolder& resultsHolder) {

	for (int rootId = 0; rootId<batchSize; rootId ++) {
		int masterRootId = rootId + batchStartPosition;
		for (int comboId = 0; comboId<J; comboId ++) {
			for (int classId = 0; classId < classes; classId++) {
				double score = resultsHolder.h_scores[rootId*classes* J + classes*comboId + classId];
				double numOfComparesForClass = dataHolder.dataTest.h_numOfComparesByClass[masterRootId*classes + classId];
				double weightedScore = score / numOfComparesForClass;
				resultsHolder.h_scores[rootId*classes*J + classes*comboId + classId] = weightedScore;
			}
		}
	}
}

/*
 * The scores in d_scores are totals and need to be divided by the count stored in d_numOfComparesByClass
 */
void SerialKernels::scaleFloatScoresForClassSize(DataHolder& dataHolder, ResultsHolder& resultsHolder) {

	for (int rootId = 0; rootId<batchSize; rootId ++) {
		int masterRootId = rootId + batchStartPosition;
		for (int comboId = 0; comboId<J; comboId ++) {
			for (int classId = 0; classId<classes; classId++) {
				float score = resultsHolder.h_scores_float[rootId*classes*J + classes*comboId + classId];
				float numOfComparesForClass = dataHolder.dataTest.h_numOfComparesByClass[masterRootId*classes + classId];
				float weightedScore = score / numOfComparesForClass;
				resultsHolder.h_scores_float[rootId*classes*J + classes*comboId + classId] = weightedScore;
			}
		}
	}
}

/*
 * Calculates the votes for each class by comparing the scores for each subset of feature vectors
 */
void SerialKernels::calculateClassVotesPredictionCount(DataHolder& dataHolder, ResultsHolder& resultsHolder)
{
	for(int rootId = 0; rootId<batchSize; rootId++) {
		int stepNum = 0;
		for(int comboId = 0; comboId<J; comboId++) {
			double maxScore = 0.0;
			int classPred = 0;
			double numberOfMaxScores = 0.0;

			for(int classId = 0; classId<classes; classId++) {
				double currentScore = resultsHolder.h_scores[rootId*J*classes + comboId*classes + classId];

				//if else statement handles case where more than one class has the same number of votes
				if(currentScore > maxScore) {
					classPred = classId;
					numberOfMaxScores = 1.0;
					maxScore = currentScore;
				}
				//workaround to use pregenerated pseudo randoms numbers
				else if(currentScore==maxScore) 	{
					numberOfMaxScores = numberOfMaxScores + 1.0;

					int randomPos = (rootId + comboId + classId) % 10000;
					double r = dataHolder.h_randoms[randomPos] / 1000.0;

					if(r < (1.0 / numberOfMaxScores)) {
						classPred = classId;
					}
				}
			}

			if(comboId > (J_min-1))	{
				stepNum = ((comboId - J_min) / J_step) + 1;
			}

			double oldClassVote = resultsHolder.h_class_votes[rootId*J_numOfSteps*classes + stepNum*classes + classPred];
			double newClassVote = oldClassVote + 1.0;
			resultsHolder.h_class_votes[rootId*J_numOfSteps*classes + stepNum*classes + classPred] = newClassVote;
		}
	}
}


/*
 * Calculates the votes for each class by comparing the scores for each subset of feature vectors
 */
void SerialKernels::calculateClassVotesFloatPredictionCount(DataHolder& dataHolder, ResultsHolder& resultsHolder)
{
	for(int rootId=0; rootId<batchSize; rootId++)
	{
		int stepNum=0;
		for(int comboId=0; comboId<J; comboId++) {
			float maxScore=0.0;
			int classPred=0;
			float numberOfMaxScores=0.0;

			for(int classId = 0; classId<classes; classId++) {
				float currentScore = resultsHolder.h_scores_float[rootId*J*classes + comboId*classes + classId];

				//if else statement handles case where more than one class has the same number of votes
				if(currentScore > maxScore) {
					classPred=classId;
					numberOfMaxScores=1.0;
					maxScore = currentScore;
				}
				//workaround to use pregenerated pseudo randoms numbers
				else if(currentScore==maxScore) 	{
					numberOfMaxScores = numberOfMaxScores + 1.0;

					int randomPos = (rootId + comboId + classId) % 10000;
					float r = dataHolder.h_randoms[randomPos] / 1000.0;

					if(r < (1.0 / numberOfMaxScores)) {
						classPred = classId;
					}
				}
			}

			if(comboId > (J_min-1)) {
				stepNum = ((comboId - J_min) / J_step) + 1;
			}

			float oldClassVote = resultsHolder.h_class_votes_float[rootId*J_numOfSteps*classes + stepNum*classes + classPred];
			float newClassVote = oldClassVote + 1.0;
			resultsHolder.h_class_votes_float[rootId*J_numOfSteps*classes + stepNum*classes + classPred] = newClassVote;
		}
	}
}



void SerialKernels::calculateClassVotesCummulativeScores(DataHolder& dataHolder, ResultsHolder& resultsHolder)
{
	for(int rootId=0; rootId<batchSize; rootId++){
		int stepNum=0;
		for(int comboId = 0; comboId<J; comboId++) {
			if(comboId > (J_min-1)) {
				stepNum = ((comboId - J_min) / J_step) + 1;
			}

			for(int classId=0; classId<classes; classId++) {
				double currentScore = resultsHolder.h_scores[rootId*J*classes + comboId*classes + classId];
				double oldClassVote = resultsHolder.h_class_votes[rootId*J_numOfSteps*classes + stepNum*classes + classId];
				double newClassVote = oldClassVote + currentScore;
				resultsHolder.h_class_votes[rootId*J_numOfSteps*classes + stepNum*classes + classId] = newClassVote;
			}
		}
	}
}


void SerialKernels::calculateClassVotesFloatCummulativeScores(DataHolder& dataHolder, ResultsHolder& resultsHolder)
{
	for(int rootId=0; rootId<batchSize; rootId++) {
		int stepNum=0;

		for(int comboId=0; comboId<J; comboId++) {
			if(comboId > (J_min-1))	{
				stepNum = ((comboId - J_min) / J_step) + 1;
			}

			for(int classId=0; classId<classes; classId++) {
				float currentScore = resultsHolder.h_scores_float[rootId*J*classes + comboId*classes + classId];
				float oldClassVote = resultsHolder.h_class_votes_float[rootId*J_numOfSteps*classes + stepNum*classes + classId];
				float newClassVote = oldClassVote + currentScore;
				resultsHolder.h_class_votes_float[rootId*J_numOfSteps*classes + stepNum*classes + classId] = newClassVote;
			}
		}
	}
}


/*
 * makes the class votes cummulative for J
 */
void SerialKernels::makeClassVotesCummulative(DataHolder& dataHolder, ResultsHolder& resultsHolder)
{
	for(int rootId=0; rootId<batchSize; rootId++)	{
		for(int stepNum=1; stepNum<J_numOfSteps; stepNum++){
			for(int classId = 0; classId < classes; classId++){
				double currentClassVote = resultsHolder.h_class_votes[rootId*J_numOfSteps*classes + stepNum*classes + classId];
				double previousClassVote = resultsHolder.h_class_votes[rootId*J_numOfSteps*classes + (stepNum-1)*classes + classId];
				double cummulativeClassVote = currentClassVote + previousClassVote;
				resultsHolder.h_class_votes[rootId*J_numOfSteps*classes + stepNum*classes + classId] = cummulativeClassVote;
			}
		}
	}
}

void SerialKernels::makeClassVotesFloatCummulative(DataHolder& dataHolder, ResultsHolder& resultsHolder)
{
	for(int rootId=0; rootId<batchSize; rootId++){
		for(int stepNum=1; stepNum<J_numOfSteps; stepNum++){
			for(int classId=0; classId<classes; classId++){
				float currentClassVote = resultsHolder.h_class_votes_float[rootId*J_numOfSteps*classes + stepNum*classes + classId];
				float previousClassVote = resultsHolder.h_class_votes_float[rootId*J_numOfSteps*classes + (stepNum-1)*classes + classId];
				float cummulativeClassVote = currentClassVote + previousClassVote;
				resultsHolder.h_class_votes_float[rootId*J_numOfSteps*classes + stepNum*classes + classId] = cummulativeClassVote;
			}
		}
	}
}

/*
 * Calculate the class probabilities from the class votes
 */
void SerialKernels::calculateClassProbabilities(DataHolder& dataHolder, ResultsHolder& resultsHolder)
{
	for(int rootId = 0; rootId<N_ts; rootId++){
		//calculate voteSum
		for(int stepNum=0; stepNum<J_numOfSteps; stepNum++){
			double voteSum = 0.0;
			for(int classId = 0; classId<classes; classId++){
				voteSum = voteSum + resultsHolder.h_class_votes[rootId*J_numOfSteps*classes + stepNum*classes + classId];
			}

			for(int classId = 0; classId<classes; classId++){
				resultsHolder.h_class_prob[rootId*J_numOfSteps*classes + stepNum*classes + classId] =
						resultsHolder.h_class_votes[rootId*J_numOfSteps*classes + stepNum*classes + classId] / voteSum;
			}
		}
	}
}

/*
 * Calculate the class probabilities from the class votes
 */
void SerialKernels::calculateClassProbabilitiesFloat(DataHolder& dataHolder, ResultsHolder& resultsHolder)
{
	for(int rootId = 0; rootId<N_ts; rootId++)	{
		//calculate voteSum
		for(int stepNum=0; stepNum<J_numOfSteps; stepNum++)	{
			float voteSum = 0.0;
			for(int classId = 0; classId<classes; classId++){
				voteSum = voteSum + resultsHolder.h_class_votes_float[rootId*J_numOfSteps*classes + stepNum*classes + classId];
			}

			for(int classId = 0; classId<classes; classId++)
			{
				resultsHolder.h_class_prob[rootId*J_numOfSteps*classes + stepNum*classes + classId] =
						resultsHolder.h_class_votes_float[rootId*J_numOfSteps*classes + stepNum*classes + classId] / voteSum;
			}
		}
	}
}



/*
 * Calculate the confusion matrix
 */
void SerialKernels::calculateConfusionMatrix(DataHolder& dataHolder, ResultsHolder& resultsHolder)
{
	for(int rootId=0; rootId<N_ts; rootId++)	{
		int trueClass = dataHolder.dataTest.h_classMember[rootId];

		for(int stepNum=0; stepNum<J_numOfSteps; stepNum++)	{
			float maxProb = 0.0;
			int classPred = 0;
			float numberOfMaxProb = 0.0;

			for(int classId=0; classId<classes; classId++){
				float currentProb = resultsHolder.h_class_prob[rootId*J_numOfSteps*classes + stepNum*classes + classId];

				//if else statement handles case where more than one class has the same number of votes
				if(currentProb > maxProb){
					classPred = classId;
					numberOfMaxProb = 1.0;
					maxProb = currentProb;
				}
				//workaround to use pregenerated pseudo randoms numbers
				else if(currentProb==maxProb){
					numberOfMaxProb = numberOfMaxProb + 1.0;

					int randomPos = (rootId + stepNum + classId) % 10000;
					float r = dataHolder.h_randoms[randomPos]  / 1000.0;

					if(r < (1.0 / numberOfMaxProb)){
						classPred = classId;
					}
				}
			}
			resultsHolder.h_confusion_matrix[stepNum*classes*classes + trueClass*classes + classPred]++;
		}
	}
}

/*
* Calculates the scores using prw for each data point for each combination
*/
void SerialKernels::calculateScoresPrw(DataHolder& dataHolder, ResultsHolder& resultsHolder) {

	for (int rootId=0; rootId<batchSize; rootId ++) {
		int masterRootId = rootId + batchStartPosition;
	for (int comboId = 0; comboId < J; comboId ++) {
			for (int targetId=0; targetId<N_tr; targetId++) {
				int classId = dataHolder.dataTrain.h_classMember[targetId];

				//only proceed if root and target are from different parts of the kfold split
				if (dataHolder.dataTest.h_kfoldMember[masterRootId] != dataHolder.dataTrain.h_kfoldMember[targetId]) {
					double score = 0.0;
					double diffSum = 0.0;

					for(int i=dataHolder.h_comboBreakPoints[comboId]; i<dataHolder.h_comboBreakPoints[comboId+1]; i++) {
						int featureNum = dataHolder.h_in_combo[i];
						double featureDiff = abs(dataHolder.dataTest.h_inNormalised[masterRootId*L + featureNum] - dataHolder.dataTrain.h_inNormalised[targetId*L + featureNum]);
						diffSum = diffSum + (featureDiff*featureDiff);
					}

					//if diffSum is an integer try to use the precomputed value

					double intPart;
					if(modf(diffSum,&intPart)==0 && diffSum<256){
						score = prw_lookupTable[(int) intPart];
					}else{
						score = exp(-1.0 * (diffSum / (2.0 * prw_param * prw_param)));
					}

					resultsHolder.h_scores[rootId * classes * J + classes * comboId + classId] =
								resultsHolder.h_scores[rootId * classes * J + classes * comboId + classId] +
								score;
				}
			}
		}
	}
}



/**
* Calculates the scores using prw for each data point for each combination with a sparse representation
*/
void SerialKernels::calculateScoresPrwSparse(DataHolder& dataHolder, ResultsHolder& resultsHolder) {
	for (int rootId = 0; rootId < batchSize; rootId ++) {
		int masterRootId = rootId + batchStartPosition;

		for (int comboId = 0; comboId < J; comboId ++) {
			for (int targetId=0; targetId<N_tr; targetId++) {
				//int classId = d_classMember[targetId];

				//only proceed if root and target are from different parts of the kfold split
				if (dataHolder.dataTest.h_kfoldMember[masterRootId] != dataHolder.dataTrain.h_kfoldMember[targetId]) {
					double score = 0.0;
					int diffSum = 0;

					int * rootPointer = &dataHolder.dataTest.h_in[dataHolder.dataTest.h_inSparseVectorBreakPoints[rootId + batchStartPosition]];
					int * targetPointer = &dataHolder.dataTrain.h_in[dataHolder.dataTrain.h_inSparseVectorBreakPoints[targetId]];

					//calculate the distance between the root and target feature vector subset
					for(int i=dataHolder.h_comboBreakPoints[comboId]; i<dataHolder.h_comboBreakPoints[comboId+1]; i++) {
						int featureNum = dataHolder.h_in_combo[i];

						int featureRoot=0;
						int featureTarget=0;

						while(*rootPointer<=featureNum && rootPointer<&dataHolder.dataTest.h_in[dataHolder.dataTest.h_inSparseVectorBreakPoints[masterRootId+1]]) {
							if (*rootPointer==featureNum) {
								featureRoot=1;
							}
							rootPointer++;
						}

						while(*targetPointer <= featureNum && targetPointer < &dataHolder.dataTrain.h_in[dataHolder.dataTrain.h_inSparseVectorBreakPoints[targetId+1]]) {
							if (*targetPointer==featureNum) {
								featureTarget=1;
							}
							targetPointer++;
						}

						//int featureDiff = abs(featureRoot - featureTarget);
						diffSum = diffSum + abs(featureRoot - featureTarget);
					}

					double intPart;
					if(modf(diffSum,&intPart)==0 && diffSum<256){
						score = prw_lookupTable[(int) intPart];
					}else{
						score = exp(-1.0 * (diffSum / (2.0 * prw_param * prw_param)));
					}
					resultsHolder.h_scores[rootId * classes * J + classes * comboId + dataHolder.dataTrain.h_classMember[targetId]] =
					resultsHolder.h_scores[rootId * classes * J + classes * comboId + dataHolder.dataTrain.h_classMember[targetId]] + score;

				}
			}
		}
	}
}







/**
* Calculates the scores using rascal for each data point for each combination
*/
void SerialKernels::calculateScoresRascal(DataHolder& dataHolder, ResultsHolder& resultsHolder) {

	for (int rootId=0; rootId<batchSize; rootId ++) {
		int masterRootId=rootId + batchStartPosition;
		//cout << "JT: masterRootId " << masterRootId << endl;
		for (int comboId=0; comboId<J; comboId ++) {
			//cout << "JT: comboId " << comboId << endl;
			for (int targetId=0; targetId<N_tr; targetId++) {

				int classId = dataHolder.dataTrain.h_classMember[targetId];
				//cout << "JT: targetId " << targetId << "," << classId << endl;
				//only proceed if root and target are from different parts of the kfold split
				if (dataHolder.dataTest.h_kfoldMember[masterRootId] != dataHolder.dataTrain.h_kfoldMember[targetId]) {
					int diffSum = 0;

					for(int i=dataHolder.h_comboBreakPoints[comboId]; i<dataHolder.h_comboBreakPoints[comboId+1]; i++) {
						int featureNum = dataHolder.h_in_combo[i];
						//cout << "JT: feature: " << featureNum << endl;
						int featureDiff = abs(dataHolder.dataTest.h_in[masterRootId*L + featureNum] - dataHolder.dataTrain.h_in[targetId*L + featureNum]);
						diffSum = diffSum + featureDiff;
						if (diffSum > 0) {
							break;
						}
					}
					//cout << "JT: diffSum: " << diffSum << endl;
					if (diffSum == 0) {
						//cout << "hi: " << J << endl;
						//cout << "JT: score: " <<resultsHolder.h_scores_float[0] << endl;
						resultsHolder.h_scores_float[rootId*classes*J + classes*comboId + classId] =
								resultsHolder.h_scores_float[rootId*classes*J + classes*comboId + classId] + 1.0;
					}
				}
			}
		}
	}
	/**
	for(int rootId=0; rootId<batchSize; rootId++){
		cout << "root: " << rootId << endl;
		for (int comboId=0; comboId<J; comboId++) {
			cout << "comboId: " << comboId;
			for(int classId=0; classId<classes; classId++){
				cout << ", ";
				cout << resultsHolder.h_scores_float[rootId*classes*J + classes*comboId + classId];
			}
			cout << endl;
		}
	}
**/
}


/**
* Calculates the scores using rascal for each data point for each combination
*/
void SerialKernels::calculateScoresRascalSparse(DataHolder& dataHolder, ResultsHolder& resultsHolder) {

	for (int rootId = 0; rootId < batchSize; rootId ++) {
		int masterRootId = rootId + batchStartPosition;
		for (int comboId = 0; comboId < J; comboId ++) {
			for (int targetId=0; targetId<N_tr; targetId++) {
				int classId = dataHolder.dataTrain.d_classMember[targetId];

				//only proceed if root and target are from different parts of the kfold split
				if (dataHolder.dataTrain.d_kfoldMember[masterRootId] != dataHolder.dataTrain.d_kfoldMember[targetId]) {
					int diffSum = 0;

					int * rootPointer = &dataHolder.dataTest.h_in[dataHolder.dataTest.h_inSparseVectorBreakPoints[masterRootId]];
					int * targetPointer = &dataHolder.dataTrain.h_in[dataHolder.dataTrain.h_inSparseVectorBreakPoints[targetId]];

					//calculate the distance between the root and target feature vector subset
					for(int i=dataHolder.h_comboBreakPoints[comboId]; i<dataHolder.h_comboBreakPoints[comboId+1]; i++) {
						int featureNum = dataHolder.h_in_combo[i];
						int featureRoot=0;
						int featureTarget=0;

						while(*rootPointer <= featureNum && rootPointer < &dataHolder.dataTrain.h_in[dataHolder.dataTrain.h_inSparseVectorBreakPoints[masterRootId+1]]) {
							if (*rootPointer==featureNum) {
								featureRoot=1;
							}
							rootPointer++;
						}

						while(*targetPointer <= featureNum && targetPointer < &dataHolder.dataTrain.h_in[dataHolder.dataTrain.h_inSparseVectorBreakPoints[targetId+1]]) {
							if (*targetPointer==featureNum) {
								featureTarget=1;
							}
							targetPointer++;
						}

						diffSum = diffSum + abs(featureRoot - featureTarget);

						//break out of loop for rascal once the subsamples differ
						if (diffSum > 0) {
							break;
						}
					}

					//if subsamples are identical add one
					if (diffSum == 0) {
						resultsHolder.h_scores_float[rootId * classes * J + classes * comboId + classId] =
								resultsHolder.h_scores_float[rootId * classes * J + classes * comboId + classId] + 1.0;
					}
				}
			}
		}
	}
}


void SerialKernels::calculateScores(DataHolder& dataHolder, ResultsHolder& resultsHolder) {

	if(dataHolder.h_method_ref==1)
	{
		for(int i=0; i<256; i++)
		{
			prw_lookupTable[i] = dataHolder.h_prw_lookupTable[i];
		}

		if(dataHolder.dataFormat==0)
		{
			calculateScoresPrw(dataHolder, resultsHolder);
		}
		else if(dataHolder.dataFormat==1)
		{
			calculateScoresPrwSparse(dataHolder, resultsHolder);
		}
	}
	else if(dataHolder.h_method_ref==0)
	{
		if(dataHolder.dataFormat==0)
		{
			calculateScoresRascal(dataHolder, resultsHolder);
		}
		else if(dataHolder.dataFormat==1)
		{
			calculateScoresRascalSparse(dataHolder, resultsHolder);
		}
	}

}






SerialKernels::SerialKernels() {
	// TODO Auto-generated constructor stub

}

SerialKernels::~SerialKernels() {
	// TODO Auto-generated destructor stub
}

