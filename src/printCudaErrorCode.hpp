/*
 * printCudaErrorCode.cuh
 *
 *  Created on: 23 Sep 2013
 *      Author: mw529
 */
#ifndef PRINTCUDAERRORCODE_CUH_
#define PRINTCUDAERRORCODE_CUH_

#include <string>
#include <cuda.h>
#include <cuda_runtime.h>
#include <string>
#include <iostream>

using namespace std;

/**
 * prints the cuda error code
 */
void printCudaErrorCode(string msg, cudaError_t error);

void print(string msg);

#endif /* PRINTCUDAERRORCODE_CUH_ */
