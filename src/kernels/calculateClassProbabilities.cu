#include "../printCudaErrorCode.hpp"
#include "../DataStructure.hpp"
#include "../DataHolder.hpp"
#include "../ResultsHolder.hpp"

__constant__ int classes;
__constant__ int batchSize;
__constant__ int batchStartPosition;
__constant__ int J_numOfSteps;
__constant__ int method_ref;

__device__ float *d_class_prob;
__device__ float *d_class_votes_float;
__device__ double *d_class_votes;


void setupConstantDataCalculateClassProbabilities(int *h_classes, int *h_batchSize,
		int *h_batchStartPosition, int *h_J_numOfSteps, int *h_method_ref)
{
	cudaMemcpyToSymbol(classes,h_classes,sizeof(int));
	cudaMemcpyToSymbol(batchSize,h_batchSize,sizeof(int));
	cudaMemcpyToSymbol(batchStartPosition,h_batchStartPosition,sizeof(int));
	cudaMemcpyToSymbol(J_numOfSteps,h_J_numOfSteps,sizeof(int));
	cudaMemcpyToSymbol(method_ref,h_method_ref,sizeof(int));
}


__global__ void setUpDevicePointersCalculateClassProbabilities(float *class_prob, float *class_votes_float, double *class_votes)
{
	d_class_prob = (float*) class_prob;
	if(method_ref==0 || method_ref==2){
		d_class_votes_float = (float*) class_votes_float;
	}else{
		d_class_votes = (double*) class_votes;
	}
}

	/**
	 * Calculates the probability of each data point belonging to each class as the votes for each class
	 * divided by the total votes in opencl.
	 */

#define STRINGIFY(A) #A
const char* kernel_source_4 = STRINGIFY(
\n#pragma OPENCL EXTENSION cl_khr_fp64 : enable\n
__kernel void calculateClassProbabilities_ocl(__global float* d_class_prob, __global double* d_class_votes,
	const int batchSize, const int J_numOfSteps, const int classes) {

	for (int rootId = get_group_id(0); rootId < batchSize; rootId += get_num_groups(0)) {
		//calculate voteSum
		for (int stepNum = 0; stepNum < J_numOfSteps; stepNum++) {
			double voteSum = 0.0;
			for (int classId = 0; classId < classes; classId++) {
				voteSum = voteSum
						+ d_class_votes[rootId * J_numOfSteps * classes
								+ stepNum * classes + classId];
			}

			for (int classId = 0; classId < classes; classId++) {
				d_class_prob[rootId * J_numOfSteps * classes
						+ stepNum * classes + classId] = d_class_votes[rootId
						* J_numOfSteps * classes + stepNum * classes
						+ classId] / voteSum;
			}
		}
	}
}
);

	/**
	 * Calculates the probability of each data point belonging to each class as the votes for each class
	 * divided by the total votes in cuda
	 */
__global__ void calculateClassProbabilities() {

	for (int rootId = blockIdx.x; rootId < batchSize; rootId += gridDim.x) {
		//calculate voteSum
		for (int stepNum = 0; stepNum < J_numOfSteps; stepNum++) {
			double voteSum = 0.0;
			for (int classId = 0; classId < classes; classId++) {
				voteSum = voteSum
						+ d_class_votes[rootId * J_numOfSteps * classes
								+ stepNum * classes + classId];
			}

			for (int classId = 0; classId < classes; classId++) {
				d_class_prob[rootId * J_numOfSteps * classes
						+ stepNum * classes + classId] = d_class_votes[rootId
						* J_numOfSteps * classes + stepNum * classes
						+ classId] / voteSum;
			}
		}
	}
}


/**
 * Calculates the probability of each data point belonging to each class as the votes for each class
 * divided by the total votes in opencl.
 */
 #define STRINGIFY(A) #A
const char* kernel_source_5 = STRINGIFY(
__kernel void calculateClassProbabilitiesFloat_ocl(__global float* d_class_prob, __global float* d_class_votes_float,
	const int batchSize, const int J_numOfSteps, const int classes) {

	for (int rootId = get_group_id(0); rootId < batchSize; rootId += get_num_groups(0)) {
		//calculate voteSum
		for (int stepNum = 0; stepNum < J_numOfSteps; stepNum++) {
			float voteSum = 0.0;
			for (int classId = 0; classId < classes; classId++) {
				voteSum = voteSum
					+ d_class_votes_float[rootId * J_numOfSteps * classes
							+ stepNum * classes + classId];
			}

			for (int classId = 0; classId < classes; classId++) {
				d_class_prob[rootId * J_numOfSteps * classes
					+ stepNum * classes + classId] = d_class_votes_float[rootId
					* J_numOfSteps * classes + stepNum * classes
					+ classId] / voteSum;
			}
		}
	}
}
);



/**
 * Calculates the probability of each data point belonging to each class as the votes for each class
 * divided by the total votes.
 */
__global__ void calculateClassProbabilitiesFloat() {

	for (int rootId = blockIdx.x; rootId < batchSize; rootId += gridDim.x) {
		//calculate voteSum
		for (int stepNum = 0; stepNum < J_numOfSteps; stepNum++) {
			float voteSum = 0.0;
			for (int classId = 0; classId < classes; classId++) {
				voteSum = voteSum
					+ d_class_votes_float[rootId * J_numOfSteps * classes
							+ stepNum * classes + classId];
			}

			for (int classId = 0; classId < classes; classId++) {
				d_class_prob[rootId * J_numOfSteps * classes
					+ stepNum * classes + classId] = d_class_votes_float[rootId
					* J_numOfSteps * classes + stepNum * classes
					+ classId] / voteSum;
			}
		}
	}
}


extern "C" void calculateClassProbabilitiesWrapper(DataHolder dataHolder, ResultsHolder resultsHolder, dim3 blocks, dim3 threads) {

	if(dataHolder.implementation==1){
		setupConstantDataCalculateClassProbabilities((int*) &dataHolder.dataTrain.classes, (int*) &dataHolder.classifier.h_currentBatchSize,
			(int*) &dataHolder.classifier.h_currentBatchStartPos, (int*) &dataHolder.classifier.h_J_numOfSteps, (int*) &dataHolder.h_method_ref);

		setUpDevicePointersCalculateClassProbabilities<<<1,1>>>(resultsHolder.d_class_prob,resultsHolder.d_class_votes_float, resultsHolder.d_class_votes);

		if(dataHolder.h_method_ref==0 || dataHolder.h_method_ref==2)
		{
			calculateClassProbabilitiesFloat<<<blocks, threads>>>();
		}
		else
		{
			calculateClassProbabilities<<<blocks, threads>>>();
		}
		cudaError_t cudaCodeCalculateClassProb = cudaGetLastError();
		if(cudaCodeCalculateClassProb!=cudaSuccess)
		{	printCudaErrorCode("error in calculate class prob kernel: ", cudaCodeCalculateClassProb);}
	}
	else if (dataHolder.implementation==2) {
	  if(dataHolder.h_method_ref==0 || dataHolder.h_method_ref==2)
	  {
		cl_int ret;
		cl_program clpgm;
		clpgm = clCreateProgramWithSource(dataHolder.clctx,1, &kernel_source_5, NULL, &ret);
		char clcompileflags[4096]="";
		ret = clBuildProgram(clpgm, 0, NULL, clcompileflags, NULL, NULL);
		cl_kernel clkern = clCreateKernel(clpgm, "calculateClassProbabilitiesFloat_ocl", &ret);
		cl_event event;
		size_t global_item_size = blocks.x * threads.x;
		size_t local_item_size = threads.x;

		ret = clSetKernelArg(clkern,0,sizeof(cl_mem),(void*)&resultsHolder.d_class_prob_ocl);
		ret = clSetKernelArg(clkern,1,sizeof(cl_mem),(void*)&resultsHolder.d_class_votes_float_ocl);
		ret = clSetKernelArg(clkern,2,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchSize);
		ret = clSetKernelArg(clkern,3,sizeof(int),(void*)&dataHolder.classifier.h_J_numOfSteps);
		ret = clSetKernelArg(clkern,4,sizeof(int),(void*)&dataHolder.dataTrain.classes);

		ret = clEnqueueNDRangeKernel(dataHolder.clcmdq,clkern,1,NULL,&global_item_size,&local_item_size,0,NULL,&event);
		ret = clWaitForEvents(1,&event);
		ret=clReleaseEvent(event);
	  }
	  else
	  {
	    cl_int ret;
		cl_program clpgm;
		clpgm = clCreateProgramWithSource(dataHolder.clctx,1, &kernel_source_4, NULL, &ret);
		char clcompileflags[4096]="";
		ret = clBuildProgram(clpgm, 0, NULL, clcompileflags, NULL, NULL);
		cl_kernel clkern = clCreateKernel(clpgm, "calculateClassProbabilities_ocl", &ret);
		cl_event event;
		size_t global_item_size = blocks.x * threads.x;
		size_t local_item_size = threads.x;

		ret = clSetKernelArg(clkern,0,sizeof(cl_mem),(void*)&resultsHolder.d_class_prob_ocl);
		ret = clSetKernelArg(clkern,1,sizeof(cl_mem),(void*)&resultsHolder.d_class_votes_ocl);
		ret = clSetKernelArg(clkern,2,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchSize);
		ret = clSetKernelArg(clkern,3,sizeof(int),(void*)&dataHolder.classifier.h_J_numOfSteps);
		ret = clSetKernelArg(clkern,4,sizeof(int),(void*)&dataHolder.dataTrain.classes);

		ret = clEnqueueNDRangeKernel(dataHolder.clcmdq,clkern,1,NULL,&global_item_size,&local_item_size,0,NULL,&event);
		ret = clWaitForEvents(1,&event);
		ret=clReleaseEvent(event);

	  }
	  
	}
}
