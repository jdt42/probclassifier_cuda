#include "../printCudaErrorCode.hpp"
#include "../DataStructure.hpp"
#include "../DataHolder.hpp"
#include "../ResultsHolder.hpp"

__constant__ int classes;
__constant__ int batchSize;
__constant__ int batchStartPosition;
__constant__ int J;
__constant__ int J_numOfSteps;

__device__ float *d_class_votes_float;
__device__ double *d_class_votes;

void setupConstantDataMakeClassVotesCummulative(int *h_classes, int *h_batchSize,
		int *h_batchStartPosition,int *h_J,int *h_J_numOfSteps)
{
	cudaMemcpyToSymbol(classes,h_classes,sizeof(int));
	cudaMemcpyToSymbol(batchSize,h_batchSize,sizeof(int));
	cudaMemcpyToSymbol(batchStartPosition,h_batchStartPosition,sizeof(int));

	cudaMemcpyToSymbol(J,h_J,sizeof(int));
	cudaMemcpyToSymbol(J_numOfSteps,h_J_numOfSteps,sizeof(int));
}

__global__ void setupDevicePointersMakeClassVotesCummulative(float *class_votes_float, double *class_votes)
{
	d_class_votes_float = (float*) class_votes_float;
	d_class_votes = (double*) class_votes;
}

/*
 * makes the class votes cummulative for J in opencl
 */
#define STRINGIFY(A) #A
 const char* kernel_source_6 = STRINGIFY(
\n#pragma OPENCL EXTENSION cl_khr_fp64 : enable\n
__kernel void makeClassVotesCummulative_ocl(__global double* d_class_votes,
    const int batchSize, const int J_numOfSteps, const int classes) {

	for (int rootId = get_group_id(0); rootId < batchSize; rootId += get_num_groups(0)) {
		for (int stepNum = 1; stepNum < J_numOfSteps; stepNum++) {
			for (int classId = 0; classId < classes; classId++) {
				double currentClassVote = d_class_votes[rootId * J_numOfSteps
						* classes + stepNum * classes + classId];
				double previousClassVote = d_class_votes[rootId
						* J_numOfSteps * classes
						+ (stepNum - 1) * classes + classId];
				double cummulativeClassVote = currentClassVote
						+ previousClassVote;
				d_class_votes[rootId * J_numOfSteps * classes
						+ stepNum * classes + classId] =
						cummulativeClassVote;
			}
		}
	}
}
);



/*
 * makes the class votes cummulative for J in cuda
 */
__global__ void makeClassVotesCummulative() {

	for (int rootId = blockIdx.x; rootId < batchSize; rootId += rootId += gridDim.x) {
		for (int stepNum = 1; stepNum < J_numOfSteps; stepNum++) {
			for (int classId = 0; classId < classes; classId++) {
				double currentClassVote = d_class_votes[rootId * J_numOfSteps
						* classes + stepNum * classes + classId];
				double previousClassVote = d_class_votes[rootId
						* J_numOfSteps * classes
						+ (stepNum - 1) * classes + classId];
				double cummulativeClassVote = currentClassVote
						+ previousClassVote;
				d_class_votes[rootId * J_numOfSteps * classes
						+ stepNum * classes + classId] =
						cummulativeClassVote;
			}
		}
	}
}


/*
 * makes the class votes cummulative for J in opencl
 */
const char* kernel_source_7 = STRINGIFY(
__kernel void makeClassVotesFloatCummulative_ocl(__global float* d_class_votes_float,
    const int batchSize, const int J_numOfSteps, const int classes) {

	for (int rootId = get_group_id(0); rootId < batchSize; rootId += get_num_groups(0)) {
		for (int stepNum = 1; stepNum < J_numOfSteps; stepNum++) {
			for (int classId = 0; classId < classes; classId++) {
				float currentClassVote = d_class_votes_float[rootId * J_numOfSteps	* classes
				        + stepNum * classes + classId];
				float previousClassVote = d_class_votes_float[rootId* J_numOfSteps * classes
						+ (stepNum - 1) * classes + classId];
				float cummulativeClassVote = currentClassVote + previousClassVote;
				d_class_votes_float[rootId * J_numOfSteps * classes
						+ stepNum * classes + classId] = cummulativeClassVote;
			}
		}
	}
}
);


/*
 * makes the class votes cummulative for J in cuda
 */
__global__ void makeClassVotesFloatCummulative() {

	for (int rootId = blockIdx.x; rootId < batchSize; rootId += gridDim.x) {
		for (int stepNum = 1; stepNum < J_numOfSteps; stepNum++) {
			for (int classId = 0; classId < classes; classId++) {
				float currentClassVote = d_class_votes_float[rootId * J_numOfSteps	* classes
				        + stepNum * classes + classId];
				float previousClassVote = d_class_votes_float[rootId* J_numOfSteps * classes
						+ (stepNum - 1) * classes + classId];
				float cummulativeClassVote = currentClassVote + previousClassVote;
				d_class_votes_float[rootId * J_numOfSteps * classes
						+ stepNum * classes + classId] = cummulativeClassVote;
			}
		}
	}

}



extern "C" void makeClassVotesCummulativeWrapper(DataHolder dataHolder, ResultsHolder resultsHolder, dim3 blocks, dim3 threads) {
	if(dataHolder.implementation==1){
		setupConstantDataMakeClassVotesCummulative((int*) &dataHolder.dataTrain.classes, (int*) &dataHolder.classifier.h_currentBatchSize,
			(int*) &dataHolder.classifier.h_currentBatchStartPos,(int*) &dataHolder.classifier.h_J,(int*) &dataHolder.classifier.h_J_numOfSteps);

		setupDevicePointersMakeClassVotesCummulative<<<1,1>>>(resultsHolder.d_class_votes_float, resultsHolder.d_class_votes);

		if(dataHolder.h_method_ref==0 || dataHolder.h_method_ref==2){
			makeClassVotesFloatCummulative<<<blocks, threads>>>();
		}else{
			makeClassVotesCummulative<<<blocks, threads>>>();
		}
	}
	else if (dataHolder.implementation==2) {
	  if(dataHolder.h_method_ref==0 || dataHolder.h_method_ref==2){
		cl_int ret;
		cl_program clpgm;
		clpgm = clCreateProgramWithSource(dataHolder.clctx,1, &kernel_source_7, NULL, &ret);
		char clcompileflags[4096]="";
		ret = clBuildProgram(clpgm, 0, NULL, clcompileflags, NULL, NULL);
		cl_kernel clkern = clCreateKernel(clpgm, "makeClassVotesFloatCummulative_ocl", &ret);
		cl_event event;
		size_t global_item_size = blocks.x * threads.x;
		size_t local_item_size = threads.x;
			
		ret = clSetKernelArg(clkern,0,sizeof(cl_mem),(void*)&resultsHolder.d_class_votes_float_ocl);
		ret = clSetKernelArg(clkern,1,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchSize);
		ret = clSetKernelArg(clkern,2,sizeof(int),(void*)&dataHolder.classifier.h_J_numOfSteps);
		ret = clSetKernelArg(clkern,3,sizeof(int),(void*)&dataHolder.dataTrain.classes);
		
		ret = clEnqueueNDRangeKernel(dataHolder.clcmdq,clkern,1,NULL,&global_item_size,&local_item_size,0,NULL,&event);
		ret = clWaitForEvents(1,&event);
		ret=clReleaseEvent(event);

	  }else{
		cl_int ret;
		cl_program clpgm;
		clpgm = clCreateProgramWithSource(dataHolder.clctx,1, &kernel_source_6, NULL, &ret);
		char clcompileflags[4096]="";
		ret = clBuildProgram(clpgm, 0, NULL, clcompileflags, NULL, NULL);

		cl_kernel clkern = clCreateKernel(clpgm, "makeClassVotesCummulative_ocl", &ret);
		cl_event event;
		size_t global_item_size = blocks.x*threads.x;
		size_t local_item_size = threads.x;

		ret = clSetKernelArg(clkern,0,sizeof(cl_mem),(void*)&resultsHolder.d_class_votes_ocl);
		ret = clSetKernelArg(clkern,1,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchSize);
		ret = clSetKernelArg(clkern,2,sizeof(int),(void*)&dataHolder.classifier.h_J_numOfSteps);
		ret = clSetKernelArg(clkern,3,sizeof(int),(void*)&dataHolder.dataTrain.classes);		

		ret = clEnqueueNDRangeKernel(dataHolder.clcmdq,clkern,1,NULL,&global_item_size,&local_item_size,0,NULL,&event);
		ret = clWaitForEvents(1,&event);
		ret=clReleaseEvent(event);

	  }
	}
}
