#include "../printCudaErrorCode.hpp"
#include "../DataStructure.hpp"
#include "../DataHolder.hpp"
#include "../ResultsHolder.hpp"

__constant__ int N_tr;
__constant__ int N_ts;
__constant__ int L;
__constant__ int classes;
__constant__ int batchSize;
__constant__ int batchStartPosition;

__device__ int* d_numOfComparesByClass;
__device__ int* d_common_features_nb;
__device__ int* d_in_tr;
__device__ int* d_inSparseVectorBreakPoints_tr;
__device__ int* d_classMember_tr;
__device__ int* d_kfoldMember_tr;
__device__ int* d_in_ts;
__device__ int* d_inSparseVectorBreakPoints_ts;
__device__ int* d_classMember_ts;
__device__ int* d_kfoldMember_ts;


void setupConstantDataCalculateCommonFeaturesNb(int *h_N_tr, int *h_N_ts,int *h_L,int *h_classes, int *h_batchSize, int *h_batchStartPosition)
{
	cudaMemcpyToSymbol(N_tr,h_N_tr,sizeof(int));
	cudaMemcpyToSymbol(N_ts,h_N_ts,sizeof(int));
	cudaMemcpyToSymbol(L,h_L,sizeof(int));
	cudaMemcpyToSymbol(classes,h_classes,sizeof(int));
	cudaMemcpyToSymbol(batchSize,h_batchSize,sizeof(int));
	cudaMemcpyToSymbol(batchStartPosition,h_batchStartPosition,sizeof(int));
}

__global__ void setUpDevicePointersCalculateCommonFeaturesNb(int *numOfComparesByClass, int *common_features_nb,
		int *in_tr, int *inSparseVectorBreakPoints_tr, int *classMember_tr, int *kfoldMember_tr,
		int *in_ts, int *inSparseVectorBreakPoints_ts, int *classMember_ts, int *kfoldMember_ts)
{
	d_numOfComparesByClass = (int*) numOfComparesByClass;
	d_common_features_nb = (int*) common_features_nb;
	d_in_tr = (int*) in_tr;
	d_inSparseVectorBreakPoints_tr = (int*) inSparseVectorBreakPoints_tr;
	d_classMember_tr = (int*) classMember_tr;
	d_kfoldMember_tr = (int*) kfoldMember_tr;
	d_in_ts = (int*) in_ts;
	d_inSparseVectorBreakPoints_ts = (int*) inSparseVectorBreakPoints_ts;
	d_classMember_ts = (int*) classMember_ts;
	d_kfoldMember_ts = (int*) kfoldMember_ts;
}


#define STRINGIFY(A) #A
	const char* kernel_source_3 = STRINGIFY(
	__kernel void calculateCommonFeaturesNb_ocl(__global int* d_common_features_nb, __global int* d_kfoldMember_tr,
			__global int* d_kfoldMember_ts,__global int* d_classMember_tr, __global int* d_in_tr, __global int* d_in_ts,
			const int batchSize, const int batchStartPosition, const int N_tr, const int L, const int classes)
	{
		for (int rootId = get_group_id(0); rootId<batchSize; rootId += get_num_groups(0)) {
			int masterRootId = rootId + batchStartPosition;
			int rootKfoldRef = d_kfoldMember_ts[masterRootId];

			for(int featureNum = get_local_id(0); featureNum<L; featureNum += get_local_size(0))
			{
				for (int targetId = 0; targetId<N_tr; targetId ++) {
					int classId = d_classMember_tr[targetId];
					int targetKfoldRef = d_kfoldMember_tr[targetId];

					//only proceed if root and target are from different parts of the kfold split
					if (rootKfoldRef != targetKfoldRef) {
						int featureRoot = d_in_ts[masterRootId*L + featureNum];
						int featureTarget = d_in_tr[targetId*L + featureNum];

						if (featureRoot == featureTarget) {
							d_common_features_nb[rootId*L*classes + classId*L + featureNum] =
							d_common_features_nb[rootId*L*classes + classId*L + featureNum]+1;
						}
					}
				}
			}
		}
	}
);



__global__ void calculateCommonFeaturesNb() {
	for (int rootId = blockIdx.x; rootId<batchSize; rootId += gridDim.x) {
		int masterRootId = rootId + batchStartPosition;
		int rootKfoldRef = d_kfoldMember_ts[masterRootId];

		for(int featureNum = threadIdx.x; featureNum<L; featureNum += blockDim.x)
		{
			for (int targetId = 0; targetId<N_tr; targetId ++) {
				int classId = d_classMember_tr[targetId];
				int targetKfoldRef = d_kfoldMember_tr[targetId];

				//only proceed if root and target are from different parts of the kfold split
				if (rootKfoldRef != targetKfoldRef) {
					int featureRoot = d_in_ts[masterRootId*L + featureNum];
					int featureTarget = d_in_tr[targetId*L + featureNum];

					if (featureRoot == featureTarget) {
						//atomicAdd(&d_common_features_nb[rootId*L*classes + classId*L + featureNum], 1);
						d_common_features_nb[rootId*L*classes + classId*L + featureNum] =
								d_common_features_nb[rootId*L*classes + classId*L + featureNum]+1;
					}
				}
			}
		}
	}
}

#define STRINGIFY(A) #A
	const char* kernel_source_22 = STRINGIFY(
__kernel void calculateCommonFeaturesNbSparse_ocl(__global int* d_common_features_nb, __global int* d_kfoldMember_tr,
			__global int* d_kfoldMember_ts,__global int* d_classMember_tr, __global int* d_in_tr, __global int* d_in_ts,
			, __global int* inSparseVectorBreakPoints_tr, __global int* inSparseVectorBreakPoints_ts,
			const int batchSize, const int batchStartPosition, const int N_tr, const int L, const int classes)
{
	for (int rootId = get_group_id(0); rootId < batchSize; rootId += get_num_groups(0)) {
		int masterRootId = rootId + batchStartPosition;
		int rootKfoldRef = d_kfoldMember_ts[masterRootId];

		for (int targetId = get_group_id(1); targetId < N_tr; targetId += get_num_groups(1)) {
			int classId = d_classMember_tr[targetId];
			int targetKfoldRef = d_kfoldMember_tr[targetId];

			//only proceed if root and target are from different parts of the kfold split
			if (rootKfoldRef != targetKfoldRef) {
				int rootStartPos = d_inSparseVectorBreakPoints_ts[masterRootId];
				int rootEndPos = d_inSparseVectorBreakPoints_ts[masterRootId+1];
				int targetStartPos = d_inSparseVectorBreakPoints_tr[targetId];
				int targetEndPos = d_inSparseVectorBreakPoints_tr[targetId+1];

				int * rootPointer = &d_in_ts[rootStartPos];
				int * targetPointer = &d_in_tr[targetStartPos];
				int * rootPointerEnd = &d_in_ts[rootEndPos];
				int * targetPointerEnd = &d_in_tr[targetEndPos];

				for (int featureNum = 0; featureNum < L; featureNum++) {
					int featureRoot=0;
					int featureTarget=0;

					while(*rootPointer <= featureNum && rootPointer < rootPointerEnd) {
						if (*rootPointer==featureNum) {
							featureRoot=1;
						}
						rootPointer++;
					}

					while(*targetPointer <= featureNum && targetPointer < targetPointerEnd) {
						if (*targetPointer==featureNum) {
							featureTarget=1;
						}
						targetPointer++;
					}

					if (featureRoot == featureTarget) {
						atomic_add(&d_common_features_nb[rootId * L * classes
							+ classId * L + featureNum], 1);
					}
				}
			}
		}
	}
}
);




__global__ void calculateCommonFeaturesNbSparse() {
	for (int rootId = blockIdx.x; rootId < batchSize; rootId += gridDim.x) {
		int masterRootId = rootId + batchStartPosition;
		int rootKfoldRef = d_kfoldMember_ts[masterRootId];

		for (int targetId = blockIdx.y; targetId < N_tr; targetId += gridDim.y) {
			int classId = d_classMember_tr[targetId];
			int targetKfoldRef = d_kfoldMember_tr[targetId];

			//only proceed if root and target are from different parts of the kfold split
			if (rootKfoldRef != targetKfoldRef) {
				int rootStartPos = d_inSparseVectorBreakPoints_ts[masterRootId];
				int rootEndPos = d_inSparseVectorBreakPoints_ts[masterRootId+1];
				int targetStartPos = d_inSparseVectorBreakPoints_tr[targetId];
				int targetEndPos = d_inSparseVectorBreakPoints_tr[targetId+1];

				int * rootPointer = &d_in_ts[rootStartPos];
				int * targetPointer = &d_in_tr[targetStartPos];
				int * rootPointerEnd = &d_in_ts[rootEndPos];
				int * targetPointerEnd = &d_in_tr[targetEndPos];

				for (int featureNum = 0; featureNum < L; featureNum++) {
					int featureRoot=0;
					int featureTarget=0;

					while(*rootPointer <= featureNum && rootPointer < rootPointerEnd) {
						if (*rootPointer==featureNum) {
							featureRoot=1;
						}
						rootPointer++;
					}

					while(*targetPointer <= featureNum && targetPointer < targetPointerEnd) {
						if (*targetPointer==featureNum) {
							featureTarget=1;
						}
						targetPointer++;
					}

					if (featureRoot == featureTarget) {
						atomicAdd(&d_common_features_nb[rootId * L * classes
							+ classId * L + featureNum], 1);
					}
				}
			}
		}
	}
}


extern "C" void calculateCommonFeaturesNbWrapper(DataHolder dataHolder, ResultsHolder resultsHolder,
		dim3 blocks, dim3 threads, dim3 blocks2, dim3 threads2) {
	if(dataHolder.implementation==1)
	{
		setupConstantDataCalculateCommonFeaturesNb((int*) &dataHolder.dataTrain.N,(int*) &dataHolder.dataTest.N,(int*) &dataHolder.dataTrain.L,(int*) &dataHolder.dataTrain.classes, (int*) &dataHolder.classifier.h_currentBatchSize,
			(int*) &dataHolder.classifier.h_currentBatchStartPos);
		setUpDevicePointersCalculateCommonFeaturesNb<<<1,1>>>(dataHolder.dataTest.d_numOfComparesByClass, dataHolder.dataTest.d_common_features_nb,
			dataHolder.dataTrain.d_in, dataHolder.dataTrain.d_inSparseVectorBreakPoints, dataHolder.dataTrain.d_classMember, dataHolder.dataTrain.d_kfoldMember,
			dataHolder.dataTest.d_in, dataHolder.dataTest.d_inSparseVectorBreakPoints, dataHolder.dataTest.d_classMember, dataHolder.dataTest.d_kfoldMember);

		if(dataHolder.dataFormat==0)
		{
			calculateCommonFeaturesNb<<<blocks2, threads2>>>();
		}
		else if(dataHolder.dataFormat==1)
		{
			calculateCommonFeaturesNbSparse<<<blocks, threads>>>();
		}

		cudaError_t cudaCodeCalculateCommonFeatureNb = cudaGetLastError();
		if(cudaCodeCalculateCommonFeatureNb!=cudaSuccess)
		{	printCudaErrorCode("error in calculate common features nb kernel: ", cudaCodeCalculateCommonFeatureNb);}
	}
	else if(dataHolder.implementation==2)
	{
	  if(dataHolder.dataFormat==0)
	  {
		cl_int ret;
		cl_program clpgm;
		clpgm = clCreateProgramWithSource(dataHolder.clctx,1, &kernel_source_3, NULL, &ret);
		char clcompileflags[4096]="";
		ret = clBuildProgram(clpgm, 0, NULL, clcompileflags, NULL, NULL);
		cl_kernel clkern = clCreateKernel(clpgm, "calculateCommonFeaturesNb_ocl", &ret);
		cl_event event;
		size_t global_item_size = blocks.x * blocks.y * threads.x;
		size_t local_item_size = threads.x;

		ret = clSetKernelArg(clkern,0,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_common_features_nb_ocl);
		ret = clSetKernelArg(clkern,1,sizeof(cl_mem),(void*)&dataHolder.dataTrain.d_kfoldMember_ocl);
		ret = clSetKernelArg(clkern,2,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_kfoldMember_ocl);
		ret = clSetKernelArg(clkern,3,sizeof(cl_mem),(void*)&dataHolder.dataTrain.d_classMember_ocl);
		ret = clSetKernelArg(clkern,4,sizeof(cl_mem),(void*)&dataHolder.dataTrain.d_in_ocl);
		ret = clSetKernelArg(clkern,5,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_in_ocl);
		ret = clSetKernelArg(clkern,6,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchSize);
		ret = clSetKernelArg(clkern,7,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchStartPos);
		ret = clSetKernelArg(clkern,8,sizeof(int),(void*)&dataHolder.dataTrain.N);
		ret = clSetKernelArg(clkern,9,sizeof(int),(void*)&dataHolder.dataTrain.L);
		ret = clSetKernelArg(clkern,10,sizeof(int),(void*)&dataHolder.dataTrain.classes);

		ret = clEnqueueNDRangeKernel(dataHolder.clcmdq,clkern,1,NULL,&global_item_size,&local_item_size,0,NULL,&event);
		ret = clWaitForEvents(1,&event);
		ret=clReleaseEvent(event);

      }
	  else if (dataHolder.dataFormat==1)
	  {
		cl_int ret;
		cl_program clpgm;
		clpgm = clCreateProgramWithSource(dataHolder.clctx,1, &kernel_source_22, NULL, &ret);
		char clcompileflags[4096]="";
		ret = clBuildProgram(clpgm, 0, NULL, clcompileflags, NULL, NULL);
		cl_kernel clkern = clCreateKernel(clpgm, "calculateCommonFeaturesNbSparse_ocl", &ret);
		cl_event event;
		size_t global_item_size = blocks2.x * threads2.x;
		size_t local_item_size = threads2.x;

		ret = clSetKernelArg(clkern,0,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_common_features_nb_ocl);
		ret = clSetKernelArg(clkern,1,sizeof(cl_mem),(void*)&dataHolder.dataTrain.d_kfoldMember_ocl);
		ret = clSetKernelArg(clkern,2,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_kfoldMember_ocl);
		ret = clSetKernelArg(clkern,3,sizeof(cl_mem),(void*)&dataHolder.dataTrain.d_classMember_ocl);
		ret = clSetKernelArg(clkern,4,sizeof(cl_mem),(void*)&dataHolder.dataTrain.d_in_ocl);
		ret = clSetKernelArg(clkern,5,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_in_ocl);
		ret = clSetKernelArg(clkern,6,sizeof(cl_mem),(void*)&dataHolder.dataTrain.d_inSparseVectorBreakPoints_ocl);
		ret = clSetKernelArg(clkern,7,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_inSparseVectorBreakPoints_ocl);
		ret = clSetKernelArg(clkern,8,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchSize);
		ret = clSetKernelArg(clkern,9,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchStartPos);
		ret = clSetKernelArg(clkern,10,sizeof(int),(void*)&dataHolder.dataTrain.N);
		ret = clSetKernelArg(clkern,11,sizeof(int),(void*)&dataHolder.dataTrain.L);
		ret = clSetKernelArg(clkern,12,sizeof(int),(void*)&dataHolder.dataTrain.classes);

		ret = clEnqueueNDRangeKernel(dataHolder.clcmdq,clkern,1,NULL,&global_item_size,&local_item_size,0,NULL,&event);
		ret = clWaitForEvents(1,&event);
		ret=clReleaseEvent(event);

	  }
	}
}
