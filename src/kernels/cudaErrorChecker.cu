#include "../printCudaErrorCode.hpp"

extern "C" void checkCudaErrorCodeWrapper() {
	cout << "*** checkingCudaErrorCode ***" << endl;
	cudaError_t cudaErrorCode = cudaGetLastError();
	if(cudaErrorCode!=cudaSuccess)
	{	printCudaErrorCode("error: ", cudaErrorCode);}
	else
	{
		cout << "no cuda error code detected"<< endl;
	}
}
