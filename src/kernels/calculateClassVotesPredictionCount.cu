#include "../printCudaErrorCode.hpp"
#include "../DataStructure.hpp"
#include "../DataHolder.hpp"
#include "../ResultsHolder.hpp"

__constant__ int classes;
__constant__ int batchSize;
__constant__ int batchStartPosition;

__constant__ int J;
__constant__ int J_step;
__constant__ int J_min;
__constant__ int J_numOfSteps;

__device__ float* d_scores_float;
__device__ double* d_scores;
__device__ float* d_class_votes_float;
__device__ double* d_class_votes;
__device__ int* d_randoms;

void setupConstantDataCalculateClassVotesPredictionCount(int *h_classes, int *h_batchSize,
		int *h_batchStartPosition,int *h_J, int *h_J_step, int *h_J_min, int *h_J_numOfSteps)
{
	cudaMemcpyToSymbol(classes,h_classes,sizeof(int));
	cudaMemcpyToSymbol(batchSize,h_batchSize,sizeof(int));
	cudaMemcpyToSymbol(batchStartPosition,h_batchStartPosition,sizeof(int));

	cudaMemcpyToSymbol(J,h_J,sizeof(int));
	cudaMemcpyToSymbol(J_step,h_J_step,sizeof(int));
	cudaMemcpyToSymbol(J_min,h_J_min,sizeof(int));
	cudaMemcpyToSymbol(J_numOfSteps,h_J_numOfSteps,sizeof(int));
}

__global__ void setUpDevicePointersCalculateClassVotesPredictionCount(int *randoms,
		float *class_votes_float, float *scores_float,
		double *class_votes, double *scores)
{
	d_scores_float = (float*) scores_float;
	d_scores = (double*) scores;
	d_class_votes_float = (float*) class_votes_float;
	d_class_votes = (double*) class_votes;
	d_randoms = (int*) randoms;
}

	/**
	 * Calculates the votes for each class on a prediction count basis.  I.e. each random subsample
	 * assessed over all other data points contributes one vote.  The voting system is like first past
	 * the post. In opencl.
	 */
#define STRINGIFY(A) #A
	 const char* kernel_source_12 = STRINGIFY(
     \n#pragma OPENCL EXTENSION cl_khr_fp64 : enable\n
__kernel void calculateClassVotesPredictionCount_ocl(__global double* d_class_votes, __global double* d_scores, __global float* d_randoms,
		const int batchSize, const int J, const int J_min, const int J_step, const int J_numOfSteps, const int classes) {

	for (int rootId = get_group_id(0); rootId < batchSize; rootId += get_num_groups(0)) {
		int d_stepNum = 0;

		for (int comboId = 0; comboId < J; comboId++) {
			double maxScore = 0.0;
			int classPred = 0;
			double numberOfMaxScores = 0.0;

			for (int classId = 0; classId < classes; classId++) {
				double currentScore = d_scores[rootId * J * classes
						+ comboId * classes + classId];

				//if else statement handles case where more than one class has the same number of votes
				if (currentScore > maxScore) {
					classPred = classId;
					numberOfMaxScores = 1.0;
					maxScore = currentScore;
				}
				//workaround to use pregenerated pseudo randoms numbers
				else if (currentScore == maxScore) {
					numberOfMaxScores = numberOfMaxScores + 1.0;

					int randomPos = (rootId + comboId + classId) % 10000;
					double r = d_randoms[randomPos] / 1000.0;

					if (r < (1.0 / numberOfMaxScores)) {
						classPred = classId;
					}
				}
			}

			if (comboId > (J_min - 1)) {
				d_stepNum = ((comboId - J_min) / J_step) + 1;
			}

			double oldClassVote = d_class_votes[rootId * J_numOfSteps
					* classes + d_stepNum * classes + classPred];
			double newClassVote = oldClassVote + 1.0;
			d_class_votes[rootId * J_numOfSteps * classes
					+ d_stepNum * classes + classPred] = newClassVote;
		}
	}
}
);

	/**
	 * Calculates the votes for each class on a prediction count basis.  I.e. each random subsample
	 * assessed over all other data points contributes one vote.  The voting system is like first past
	 * the post. In cuda.
	 */
__global__ void calculateClassVotesPredictionCount() {

	for (int rootId = blockIdx.x; rootId < batchSize; rootId += gridDim.x) {
		int d_stepNum = 0;

		for (int comboId = 0; comboId < J; comboId++) {
			double maxScore = 0.0;
			int classPred = 0;
			double numberOfMaxScores = 0.0;

			for (int classId = 0; classId < classes; classId++) {
				double currentScore = d_scores[rootId * J * classes
						+ comboId * classes + classId];

				//if else statement handles case where more than one class has the same number of votes
				if (currentScore > maxScore) {
					classPred = classId;
					numberOfMaxScores = 1.0;
					maxScore = currentScore;
				}
				//workaround to use pregenerated pseudo randoms numbers
				else if (currentScore == maxScore) {
					numberOfMaxScores = numberOfMaxScores + 1.0;

					int randomPos = (rootId + comboId + classId) % 10000;
					double r = d_randoms[randomPos] / 1000.0;

					if (r < (1.0 / numberOfMaxScores)) {
						classPred = classId;
					}
				}
			}

			if (comboId > (J_min - 1)) {
				d_stepNum = ((comboId - J_min) / J_step) + 1;
			}

			double oldClassVote = d_class_votes[rootId * J_numOfSteps
					* classes + d_stepNum * classes + classPred];
			double newClassVote = oldClassVote + 1.0;
			d_class_votes[rootId * J_numOfSteps * classes
					+ d_stepNum * classes + classPred] = newClassVote;
		}
	}
}


/**
 * Calculates the votes for each class on a prediction count basis.  I.e. each random subsample
 * assessed over all other data points contributes one vote.  The voting system is like first past
 * the post. In opencl.
 */
 const char* kernel_source_13 = STRINGIFY(
		 __kernel void calculateClassVotesFloatPredictionCount_ocl(__global float* d_class_votes_float, __global float* d_scores_float, __global float* d_randoms,
		 		const int batchSize, const int J, const int J_min, const int J_step, const int J_numOfSteps, const int classes) {

for (int rootId = get_group_id(0); rootId < batchSize; rootId += get_num_groups(0)) {
	int d_stepNum = 0;

	for (int comboId = 0; comboId < J; comboId++) {
		float maxScore = 0.0;
		int classPred = 0;
		float numberOfMaxScores = 0.0;

		for (int classId = 0; classId < classes; classId++) {
			float currentScore = d_scores_float[rootId * J * classes
					+ comboId * classes + classId];

			//if else statement handles case where more than one class has the same number of votes
			if (currentScore > maxScore) {
				classPred = classId;
				numberOfMaxScores = 1.0;
				maxScore = currentScore;
			}
			//workaround to use pregenerated pseudo randoms numbers
			else if (currentScore == maxScore) {
				numberOfMaxScores = numberOfMaxScores + 1.0;

				int randomPos = (rootId + comboId + classId) % 10000;
				float r = d_randoms[randomPos] / 1000.0;

				if (r < (1.0 / numberOfMaxScores)) {
					classPred = classId;
				}
			}
		}

		if (comboId > (J_min - 1)) {
			d_stepNum = ((comboId - J_min) / J_step) + 1;
		}

		float oldClassVote = d_class_votes_float[rootId * J_numOfSteps
				* classes + d_stepNum * classes + classPred];
		float newClassVote = oldClassVote + 1.0;
		d_class_votes_float[rootId * J_numOfSteps * classes
				+ d_stepNum * classes + classPred] = newClassVote;
	}
}
}
);


/**
 * Calculates the votes for each class on a prediction count basis.  I.e. each random subsample
 * assessed over all other data points contributes one vote.  The voting system is like first past
 * the post. In cuda.
 */
__global__ void calculateClassVotesFloatPredictionCount() {

for (int rootId = blockIdx.x; rootId < batchSize; rootId += gridDim.x) {
	int d_stepNum = 0;

	for (int comboId = 0; comboId < J; comboId++) {
		float maxScore = 0.0;
		int classPred = 0;
		float numberOfMaxScores = 0.0;

		for (int classId = 0; classId < classes; classId++) {
			float currentScore = d_scores_float[rootId * J * classes
					+ comboId * classes + classId];

			//if else statement handles case where more than one class has the same number of votes
			if (currentScore > maxScore) {
				classPred = classId;
				numberOfMaxScores = 1.0;
				maxScore = currentScore;
			}
			//workaround to use pregenerated pseudo randoms numbers
			else if (currentScore == maxScore) {
				numberOfMaxScores = numberOfMaxScores + 1.0;

				int randomPos = (rootId + comboId + classId) % 10000;
				float r = d_randoms[randomPos] / 1000.0;

				if (r < (1.0 / numberOfMaxScores)) {
					classPred = classId;
				}
			}
		}

		if (comboId > (J_min - 1)) {
			d_stepNum = ((comboId - J_min) / J_step) + 1;
		}

		float oldClassVote = d_class_votes_float[rootId * J_numOfSteps
				* classes + d_stepNum * classes + classPred];
		float newClassVote = oldClassVote + 1.0;
		d_class_votes_float[rootId * J_numOfSteps * classes
				+ d_stepNum * classes + classPred] = newClassVote;
	}
}
}




extern "C" void calculateClassVotesPredictionCountWrapper(DataHolder dataHolder, ResultsHolder resultsHolder, 	dim3 blocks, dim3 threads) {
	if(dataHolder.implementation==1){
		setupConstantDataCalculateClassVotesPredictionCount((int*) &dataHolder.dataTrain.classes, (int*) &dataHolder.classifier.h_currentBatchSize,
			(int*) &dataHolder.classifier.h_currentBatchStartPos,(int*) &dataHolder.classifier.h_J, (int*) &dataHolder.classifier.h_J_step, (int*) &dataHolder.classifier.h_J_min, (int*) &dataHolder.classifier.h_J_numOfSteps);

		setUpDevicePointersCalculateClassVotesPredictionCount<<<1,1>>>(dataHolder.d_randoms, resultsHolder.d_class_votes_float, resultsHolder.d_scores_float, resultsHolder.d_class_votes, resultsHolder.d_scores);

		if(dataHolder.h_method_ref==0 || dataHolder.h_method_ref==2)
		{
			calculateClassVotesFloatPredictionCount<<<blocks, threads>>>();
		}
		else
		{
			calculateClassVotesPredictionCount<<<blocks, threads>>>();
		}
		cudaError_t cudaCodeCalculateClassVotesPredictionCount = cudaGetLastError();
		if(cudaCodeCalculateClassVotesPredictionCount!=cudaSuccess)
		{	printCudaErrorCode("error in calculate class votes prediction count kernel: ", cudaCodeCalculateClassVotesPredictionCount);}
	}
	else if (dataHolder.implementation==2) {
	  if(dataHolder.h_method_ref==0 || dataHolder.h_method_ref==2)
	  {
		cl_int ret;
		cl_program clpgm;
		clpgm = clCreateProgramWithSource(dataHolder.clctx,1, &kernel_source_13, NULL, &ret);

		char clcompileflags[4096]="";
		ret = clBuildProgram(clpgm, 0, NULL, clcompileflags, NULL, NULL);
		cl_kernel clkern = clCreateKernel(clpgm, "calculateClassVotesFloatPredictionCount_ocl", &ret);
		cl_event event;
		size_t global_item_size = blocks.x*threads.x;
		size_t local_item_size = threads.x;
		cout << "error: " << ret << endl;
		ret = clSetKernelArg(clkern,0,sizeof(cl_mem),(void*)&resultsHolder.d_class_votes_float_ocl);
		ret = clSetKernelArg(clkern,1,sizeof(cl_mem),(void*)&resultsHolder.d_scores_float_ocl);
		ret = clSetKernelArg(clkern,2,sizeof(cl_mem),(void*)&dataHolder.d_randoms_ocl);
		ret = clSetKernelArg(clkern,3,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchSize);
		ret = clSetKernelArg(clkern,4,sizeof(int),(void*)&dataHolder.classifier.h_J);
		ret = clSetKernelArg(clkern,5,sizeof(int),(void*)&dataHolder.classifier.h_J_min);
		ret = clSetKernelArg(clkern,6,sizeof(int),(void*)&dataHolder.classifier.h_J_step);
		ret = clSetKernelArg(clkern,7,sizeof(int),(void*)&dataHolder.classifier.h_J_numOfSteps);
		ret = clSetKernelArg(clkern,8,sizeof(int),(void*)&dataHolder.dataTrain.classes);

		ret = clEnqueueNDRangeKernel(dataHolder.clcmdq,clkern,1,NULL,&global_item_size,&local_item_size,0,NULL,&event);
		ret = clWaitForEvents(1,&event);
		ret=clReleaseEvent(event);

	  }else{
		  cl_int ret;
		cl_program clpgm;
		clpgm = clCreateProgramWithSource(dataHolder.clctx,1, &kernel_source_12, NULL, &ret);

		char clcompileflags[4096]="";
		ret = clBuildProgram(clpgm, 0, NULL, clcompileflags, NULL, NULL);
		cl_kernel clkern = clCreateKernel(clpgm, "calculateClassVotesPredictionCount_ocl", &ret);
		cl_event event;
		size_t global_item_size = blocks.x * threads.x;
		size_t local_item_size = threads.x;

		ret = clSetKernelArg(clkern,0,sizeof(cl_mem),(void*)&resultsHolder.d_class_votes_ocl);
		ret = clSetKernelArg(clkern,1,sizeof(cl_mem),(void*)&resultsHolder.d_scores_ocl);
		ret = clSetKernelArg(clkern,2,sizeof(cl_mem),(void*)&dataHolder.d_randoms_ocl);
		ret = clSetKernelArg(clkern,3,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchSize);
		ret = clSetKernelArg(clkern,4,sizeof(int),(void*)&dataHolder.classifier.h_J);
		ret = clSetKernelArg(clkern,5,sizeof(int),(void*)&dataHolder.classifier.h_J_min);
		ret = clSetKernelArg(clkern,6,sizeof(int),(void*)&dataHolder.classifier.h_J_step);
		ret = clSetKernelArg(clkern,7,sizeof(int),(void*)&dataHolder.classifier.h_J_numOfSteps);
		ret = clSetKernelArg(clkern,8,sizeof(int),(void*)&dataHolder.dataTrain.classes);

		ret = clEnqueueNDRangeKernel(dataHolder.clcmdq,clkern,1,NULL,&global_item_size,&local_item_size,0,NULL,&event);
		cout << "error code " << ret << endl;
		ret = clWaitForEvents(1,&event);
		ret=clReleaseEvent(event);
	  }
	  
	}
}
