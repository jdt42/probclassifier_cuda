#include "../printCudaErrorCode.hpp"
#include "../DataStructure.hpp"
#include "../DataHolder.hpp"
#include "../ResultsHolder.hpp"

__constant__ int L;
__constant__ int classes;
__constant__ int batchSize;
__constant__ int batchStartPosition;
__constant__ int Q;
__constant__ int J;

__device__ float *d_scores_float;
__device__ int *d_numOfComparesByClass;
__device__ int *d_common_features_nb;
__device__ int *d_in_combo;

void setupConstantDataCalculateScoresNb(int *h_L,int *h_classes, int *h_batchSize, int *h_batchStartPosition
		, int *h_Q, int *h_J)
{
	cudaMemcpyToSymbol(L,h_L,sizeof(int));
	cudaMemcpyToSymbol(classes,h_classes,sizeof(int));
	cudaMemcpyToSymbol(batchSize,h_batchSize,sizeof(int));
	cudaMemcpyToSymbol(batchStartPosition,h_batchStartPosition,sizeof(int));
	cudaMemcpyToSymbol(Q,h_Q,sizeof(int));
	cudaMemcpyToSymbol(J,h_J,sizeof(int));
}

__global__ void setupDevicePointersCalculateScoresNb(float *scores_float, int *numOfComparesByClass,
		int *common_features_nb, int *in_combo){
	d_scores_float = (float*) scores_float;
	d_numOfComparesByClass = (int*) numOfComparesByClass;
	d_common_features_nb = (int*) common_features_nb;
	d_in_combo = (int*) in_combo;
}

	/**
	 * Calculates the scores using nb for each data point for each combination in opencl
	 */
#define STRINGIFY(A) #A
const char* kernel_source_21 = STRINGIFY(
__kernel void calculateScoresNb_ocl(__global float* d_scores_float, __global int* d_common_features_nb, __global int* d_numOfComparesByClass, __global int* d_in_combo,
	const int batchSize, const int batchStartPosition, const int J, const int classes, const int L, const int Q)
{
	for (int rootId=get_group_id(0); rootId<batchSize; rootId+=get_num_groups(0)) {
		int masterRootId = rootId + batchStartPosition;

		for (int comboId=get_local_id(0); comboId<J; comboId += get_local_size(0)) {
			for (int classId=0; classId<classes; classId++) {
				float score = 1.0;
				float numOfCompares = (float) d_numOfComparesByClass[masterRootId*classes + classId];

				for (int i=0; i<Q; i++) {
					int featureNum = d_in_combo[comboId*Q + i];
					score = score * ((d_common_features_nb[rootId*L*classes + classId*L + featureNum] + 1.0)
									/ (numOfCompares + 2.0));
				}
				//atomicAdd(&d_scores_float[rootId * *d_classes * J + *d_classes * comboId + classId], score);
				d_scores_float[rootId*classes*J + classes*comboId + classId] =
						d_scores_float[rootId*classes*J + classes*comboId + classId] + score;
			}
		}
	}
}
);





	/**
	 * Calculates the scores using nb for each data point for each combination in cuda
	 */
__global__ void calculateScoresNb() {
	for (int rootId=blockIdx.x; rootId<batchSize; rootId+=gridDim.x) {
		int masterRootId = rootId + batchStartPosition;

		for (int comboId=threadIdx.x; comboId<J; comboId += blockDim.x) {
			for (int classId=0; classId<classes; classId++) {
				float score = 1.0;
				float numOfCompares = (float) d_numOfComparesByClass[masterRootId*classes + classId];

				for (int i=0; i<Q; i++) {
					int featureNum = d_in_combo[comboId*Q + i];
					score = score * ((d_common_features_nb[rootId*L*classes + classId*L + featureNum] + 1.0)
									/ (numOfCompares + 2.0));
				}
				//atomicAdd(&d_scores_float[rootId * *d_classes * J + *d_classes * comboId + classId], score);
				d_scores_float[rootId*classes*J + classes*comboId + classId] =
						d_scores_float[rootId*classes*J + classes*comboId + classId] + score;
			}
		}
	}
}

extern "C" void calculateScoresNbWrapper(DataHolder dataHolder, ResultsHolder resultsHolder,
		dim3 blocks, dim3 threads) {
	if(dataHolder.implementation==1){
		setupConstantDataCalculateScoresNb((int*) &dataHolder.dataTrain.L,(int*) &dataHolder.dataTrain.classes, (int*) &dataHolder.classifier.h_currentBatchSize, (int*) &dataHolder.classifier.h_currentBatchStartPos,
			(int*) &dataHolder.classifier.h_q,(int*) &dataHolder.classifier.h_J);

		setupDevicePointersCalculateScoresNb<<<1,1>>>(resultsHolder.d_scores_float, dataHolder.dataTest.d_numOfComparesByClass, dataHolder.dataTest.d_common_features_nb, dataHolder.d_in_combo);


		calculateScoresNb<<<blocks, threads>>>();
		cudaError_t cudaCodeCalculateScoresNb = cudaGetLastError();
		if(cudaCodeCalculateScoresNb!=cudaSuccess)
		{	printCudaErrorCode("error in calculate scores nb kernel: ", cudaCodeCalculateScoresNb);}
	}
	else if (dataHolder.implementation==2) {
		cl_int ret;
		cl_program clpgm;
		clpgm = clCreateProgramWithSource(dataHolder.clctx,1, &kernel_source_21, NULL, &ret);

		char clcompileflags[4096]="";
		ret = clBuildProgram(clpgm, 0, NULL, clcompileflags, NULL, NULL);

		cl_kernel clkern = clCreateKernel(clpgm, "calculateScoresNb_ocl", &ret);
		cl_event event;
		size_t global_item_size = blocks.x * threads.x;
		size_t local_item_size = threads.x;


		ret = clSetKernelArg(clkern,0,sizeof(cl_mem),(void*)&resultsHolder.d_scores_float_ocl);
		ret = clSetKernelArg(clkern,1,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_common_features_nb_ocl);
		ret = clSetKernelArg(clkern,2,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_numOfComparesByClass_ocl);
		ret = clSetKernelArg(clkern,3,sizeof(cl_mem),(void*)&dataHolder.d_in_combo_ocl);
		ret = clSetKernelArg(clkern,4,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchSize);
		ret = clSetKernelArg(clkern,5,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchStartPos);
		ret = clSetKernelArg(clkern,6,sizeof(int),(void*)&dataHolder.classifier.h_J);
		ret = clSetKernelArg(clkern,7,sizeof(int),(void*)&dataHolder.dataTrain.classes);
		ret = clSetKernelArg(clkern,8,sizeof(int),(void*)&dataHolder.dataTrain.L);
		ret = clSetKernelArg(clkern,9,sizeof(int),(void*)&dataHolder.classifier.h_q);
		
		ret = clEnqueueNDRangeKernel(dataHolder.clcmdq,clkern,1,NULL,&global_item_size,&local_item_size,0,NULL,&event);
		ret = clWaitForEvents(1,&event);
		ret=clReleaseEvent(event);
	}
}

