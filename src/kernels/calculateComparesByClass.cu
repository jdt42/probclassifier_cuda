#include "../printCudaErrorCode.hpp"
#include "../DataStructure.hpp"
#include "../DataHolder.hpp"

	/**
	 * Calculates the number of times each feature vector is compared to data points in each class
	 */


__constant__ int N_tr;
__constant__ int N_ts;
__constant__ int classes;

__device__ int* d_numOfComparesByClass;
__device__ int* d_in_tr;
__device__ int* d_classMember_tr;
__device__ int* d_kfoldMember_tr;
__device__ int* d_in_ts;
__device__ int* d_classMember_ts;
__device__ int* d_kfoldMember_ts;


void setupConstantData(int *h_N_tr, int *h_N_ts,int *h_classes)
{
	cudaMemcpyToSymbol(N_tr,h_N_tr,sizeof(int));
	cudaMemcpyToSymbol(N_ts,h_N_ts,sizeof(int));
	cudaMemcpyToSymbol(classes,h_classes,sizeof(int));
}

__global__ void setUpDevicePointers(int *numOfComparesByClass,
		int *in_tr, int *classMember_tr, int *kfoldMember_tr,
		int *in_ts, int *classMember_ts, int *kfoldMember_ts)
{
	d_numOfComparesByClass = (int*) numOfComparesByClass;
	d_in_tr = (int*) in_tr;
	d_classMember_tr = (int*) classMember_tr;
	d_kfoldMember_tr = (int*) kfoldMember_tr;
	d_in_ts = (int*) in_ts;
	d_classMember_ts = (int*) classMember_ts;
	d_kfoldMember_ts = (int*) kfoldMember_ts;
}

#define STRINGIFY(A) #A
	const char* kernel_source = STRINGIFY(
	__kernel void calculateComparesByClass_ocl(__global int* d_numOfComparesByClass_ocl, __global int* d_classMember_tr_ocl,
			__global int* d_kfoldMember_tr_ocl,__global int* d_kfoldMember_ts_ocl, const int N_tr_ocl, const int N_ts_ocl, const int classes_ocl)
	{
		//int rootId = get_global_id(0);

		for (int rootId = get_group_id(0); rootId<N_ts_ocl; rootId += get_num_groups(0)) {
		//for (int rootId = get_global_id(0); rootId<N_ts_ocl; rootId += get_global_size(0)) {
			for (int targetId=0; targetId<N_tr_ocl; targetId++) {
				//only proceed if root and target are from different parts of the kfold split
				if (d_kfoldMember_ts_ocl[rootId] != d_kfoldMember_tr_ocl[targetId]) {
					//count the number of target feature vectors each root feature vector is compared against for each class so we can scale the score
					atomic_add(&d_numOfComparesByClass_ocl[rootId * classes_ocl + d_classMember_tr_ocl[targetId]], 1);
				}
			}
		}
	}
	);




__global__ void calculateComparesByClass() {

	for (int rootId = blockIdx.x; rootId<N_ts; rootId += gridDim.x) {
		for (int targetId=0; targetId<N_tr; targetId++) {
			//only proceed if root and target are from different parts of the kfold split
			if (d_kfoldMember_ts[rootId] != d_kfoldMember_tr[targetId]) {
				//count the number of target feature vectors each root feature vector is compared against for each class so we can scale the score
				atomicAdd(&d_numOfComparesByClass[rootId * classes + d_classMember_tr[targetId]], 1);
			}
		}
	}
}

extern "C" void calculateComparesByClassWrapper(DataHolder& dataHolder, dim3 blocks, dim3 threads) {

	if(dataHolder.implementation==1) {
		setupConstantData((int*) &dataHolder.dataTrain.N, (int*) &dataHolder.dataTest.N, (int*) &dataHolder.dataTrain.classes);

		setUpDevicePointers<<<1,1>>>(dataHolder.dataTest.d_numOfComparesByClass,
				dataHolder.dataTrain.d_in, dataHolder.dataTrain.d_classMember, dataHolder.dataTrain.d_kfoldMember,
				dataHolder.dataTest.d_in, dataHolder.dataTest.d_classMember, dataHolder.dataTest.d_kfoldMember);

		calculateComparesByClass<<<blocks, threads>>>();
		cudaError_t cudaCodeComparesByClass = cudaGetLastError();
		if(cudaCodeComparesByClass!=cudaSuccess)
		{	printCudaErrorCode("error in calculate compares by class kernel: ", cudaCodeComparesByClass);}
	}
	else if(dataHolder.implementation==2) {

		cl_int ret;
		cl_program clpgm;
		clpgm = clCreateProgramWithSource(dataHolder.clctx,1, &kernel_source, NULL, &ret);

		char clcompileflags[4096]="";
		ret = clBuildProgram(clpgm, 0, NULL, clcompileflags, NULL, NULL);

		cl_kernel clkern = clCreateKernel(clpgm, "calculateComparesByClass_ocl", &ret);
		cl_event event;
		size_t global_item_size = blocks.x;
		size_t local_item_size = 1;

		ret = clSetKernelArg(clkern,0,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_numOfComparesByClass_ocl);
		ret = clSetKernelArg(clkern,1,sizeof(cl_mem),(void*)&dataHolder.dataTrain.d_classMember_ocl);
		ret = clSetKernelArg(clkern,2,sizeof(cl_mem),(void*)&dataHolder.dataTrain.d_kfoldMember_ocl);
		ret = clSetKernelArg(clkern,3,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_kfoldMember_ocl);
		ret = clSetKernelArg(clkern,4,sizeof(int),(void*)&dataHolder.dataTrain.N);
		ret = clSetKernelArg(clkern,5,sizeof(int),(void*)&dataHolder.dataTest.N);
		ret = clSetKernelArg(clkern,6,sizeof(int),(void*)&dataHolder.dataTrain.classes);

		ret = clEnqueueNDRangeKernel(dataHolder.clcmdq,clkern,1,NULL,&global_item_size,&local_item_size,0,NULL,&event);
		ret = clWaitForEvents(1,&event);
		ret=clReleaseEvent(event);

	}

}

