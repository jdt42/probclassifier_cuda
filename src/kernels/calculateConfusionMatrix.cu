#include "../printCudaErrorCode.hpp"
#include "../DataStructure.hpp"
#include "../DataHolder.hpp"
#include "../ResultsHolder.hpp"

__constant__ int classes;
__constant__ int batchSize;
__constant__ int batchStartPosition;
__constant__ int J_numOfSteps;

__device__ int *d_randoms;
__device__ int *d_confusion_matrix;
__device__ float *d_class_prob;
__device__ int *d_classMember;

void setupConstantDataCalculateConfusionMatrix(int *h_classes, int *h_batchSize,
		int *h_batchStartPosition, int *h_J_numOfSteps)
{
	cudaMemcpyToSymbol(classes,h_classes,sizeof(int));
	cudaMemcpyToSymbol(batchSize,h_batchSize,sizeof(int));
	cudaMemcpyToSymbol(batchStartPosition,h_batchStartPosition,sizeof(int));
	cudaMemcpyToSymbol(J_numOfSteps,h_J_numOfSteps,sizeof(int));
}

__global__ void setUpDevicePointersCalculateConfusionMatrix(int *randoms,
		int *confusion_matrix, float *class_prob,
		int *classMember)
{
	d_randoms = (int*) randoms;
	d_confusion_matrix = (int*)confusion_matrix;
	d_class_prob = (float*) class_prob;
	d_classMember = (int*) classMember;
}

	/**
	 * Calculates the confusion matrix for each of the j steps sampled in opencl
	 */
#define STRINGIFY(A) #A
	  const char* kernel_source_14 = STRINGIFY(
__kernel void calculateConfusionMatrix_ocl(__global int* d_confusion_matrix, __global int* d_classMember, __global float* d_class_prob, __global float* d_randoms,
	const int J_numOfSteps, const int classes, const int batchSize, const int batchStartPosition) {

	for (int rootId = get_group_id(0); rootId < batchSize; rootId += get_num_groups(0)) {
		int masterRootId = rootId + batchStartPosition;
		int trueClass = d_classMember[masterRootId];

		for (int stepNum = 0; stepNum < J_numOfSteps; stepNum++) {
			float maxProb = 0.0;
			int classPred = 0;
			float numberOfMaxVotes = 0.0;

			for (int classId = 0; classId < classes; classId++) {
				float currentProb = d_class_prob[rootId * J_numOfSteps
						* classes + stepNum * classes + classId];

				//if else statement handles case where more than one class has the same probability
				if (currentProb > maxProb) {
					classPred = classId;
					numberOfMaxVotes = 1.0;
					maxProb = currentProb;
				}
				//workaround to use pregenerated pseudo randoms numbers
				else if (currentProb == maxProb) {
					numberOfMaxVotes = numberOfMaxVotes + 1.0;

					int randomPos = (rootId + stepNum + classId) % 10000;
					float r = d_randoms[randomPos] / 1000.0;

					if (r < (1.0 / numberOfMaxVotes)) {
						classPred = classId;
					}
				}
			}
			atomic_add(
					&d_confusion_matrix[stepNum * classes * classes
							+ trueClass * classes + classPred], 1);

		}

	}

}
);


	/**
	 * Calculates the confusion matrix for each of the j steps sampled.  In cuda
	 */
__global__ void calculateConfusionMatrix() {

	for (int rootId = blockIdx.x; rootId < batchSize; rootId += gridDim.x) {
		int masterRootId = rootId + batchStartPosition;
		int trueClass = d_classMember[masterRootId];

		for (int stepNum = 0; stepNum < J_numOfSteps; stepNum++) {
			float maxProb = 0.0;
			int classPred = 0;
			float numberOfMaxVotes = 0.0;

			for (int classId = 0; classId < classes; classId++) {
				float currentProb = d_class_prob[rootId * J_numOfSteps
						* classes + stepNum * classes + classId];

				//if else statement handles case where more than one class has the same probability
				if (currentProb > maxProb) {
					classPred = classId;
					numberOfMaxVotes = 1.0;
					maxProb = currentProb;
				}
				//workaround to use pregenerated pseudo randoms numbers
				else if (currentProb == maxProb) {
					numberOfMaxVotes = numberOfMaxVotes + 1.0;

					int randomPos = (rootId + stepNum + classId) % 10000;
					float r = d_randoms[randomPos] / 1000.0;

					if (r < (1.0 / numberOfMaxVotes)) {
						classPred = classId;
					}
				}
			}
			atomicAdd(
					&d_confusion_matrix[stepNum * classes * classes
							+ trueClass * classes + classPred], 1);
		}
	}
}

extern "C" void calculateConfusionMatrixWrapper(DataHolder dataHolder, ResultsHolder resultsHolder, dim3 blocks, dim3 threads) {
	if(dataHolder.implementation==1){
		setupConstantDataCalculateConfusionMatrix((int*) &dataHolder.dataTrain.classes, (int*) &dataHolder.classifier.h_currentBatchSize,
			(int*) &dataHolder.classifier.h_currentBatchStartPos, (int*) &dataHolder.classifier.h_J_numOfSteps);

		setUpDevicePointersCalculateConfusionMatrix<<<1,1>>>(dataHolder.d_randoms, resultsHolder.d_confusion_matrix, resultsHolder.d_class_prob, dataHolder.dataTest.d_classMember);

		calculateConfusionMatrix<<<blocks, threads>>>();
		cudaError_t cudaCodeCalculateConfusionMatrix = cudaGetLastError();
		if(cudaCodeCalculateConfusionMatrix!=cudaSuccess)
		{	printCudaErrorCode("error in calculate confusion matrix: ", cudaCodeCalculateConfusionMatrix);}
	}
	else if (dataHolder.implementation==2) {
		cl_int ret;
		cl_program clpgm;
		clpgm = clCreateProgramWithSource(dataHolder.clctx,1, &kernel_source_14, NULL, &ret);
		char clcompileflags[4096]="";
		ret = clBuildProgram(clpgm, 0, NULL, clcompileflags, NULL, NULL);
		cl_kernel clkern = clCreateKernel(clpgm, "calculateConfusionMatrix_ocl", &ret);
		cl_event event;
		size_t global_item_size = blocks.x*threads.x;
		size_t local_item_size = threads.x;
		ret = clSetKernelArg(clkern,0,sizeof(cl_mem),(void*)&resultsHolder.d_confusion_matrix_ocl);
		ret = clSetKernelArg(clkern,1,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_classMember_ocl);
		ret = clSetKernelArg(clkern,2,sizeof(cl_mem),(void*)&resultsHolder.d_class_prob_ocl);
		ret = clSetKernelArg(clkern,3,sizeof(cl_mem),(void*)&dataHolder.d_randoms_ocl);
		ret = clSetKernelArg(clkern,4,sizeof(int),(void*)&dataHolder.classifier.h_J_numOfSteps);
		ret = clSetKernelArg(clkern,5,sizeof(int),(void*)&dataHolder.dataTrain.classes);
		ret = clSetKernelArg(clkern,6,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchSize);
		ret = clSetKernelArg(clkern,7,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchStartPos);
		cout << "error code " << ret << endl;
		ret = clEnqueueNDRangeKernel(dataHolder.clcmdq,clkern,1,NULL,&global_item_size,&local_item_size,0,NULL,&event);
		ret = clWaitForEvents(1,&event);
		ret=clReleaseEvent(event);

	}
}

