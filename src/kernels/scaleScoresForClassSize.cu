#include "../printCudaErrorCode.hpp"
#include "../DataStructure.hpp"
#include "../DataHolder.hpp"
#include "../ResultsHolder.hpp"

__constant__ int classes;
__constant__ int batchSize;
__constant__ int batchStartPosition;
__constant__ int J;

__device__ int* d_numOfComparesByClass;
__device__ float* d_scores_float;
__device__ double* d_scores;

void setupConstantDataScaleScoresForClassSize(int *h_classes, int *h_batchSize, int *h_batchStartPosition,
		int *h_J)
{
	cudaMemcpyToSymbol(classes,h_classes,sizeof(int));
	cudaMemcpyToSymbol(batchSize,h_batchSize,sizeof(int));
	cudaMemcpyToSymbol(batchStartPosition,h_batchStartPosition,sizeof(int));
	cudaMemcpyToSymbol(J,h_J,sizeof(int));
}

__global__ void setupDevicePointersScaleScoresForClassSize(int *numOfComparesByClass, float *scores_float, double *scores)
{
	d_numOfComparesByClass = (int*) numOfComparesByClass;
	d_scores_float = (float*) scores_float;
	d_scores = (double*) scores;
}

	/**
	 * Weights the score by the number of compares for each class to make the results comparable in opencl
	 */
#define STRINGIFY(A) #A
const char* kernel_source_8 = STRINGIFY(	 
\n#pragma OPENCL EXTENSION cl_khr_fp64 : enable\n
__kernel void scaleScoresForClassSize_ocl(__global double* d_scores, __global int* d_numOfComparesByClass,
		const int batchSize, const int batchStartPosition, const int classes, const int J) {

	for (int rootId = get_group_id(0); rootId < batchSize; rootId += get_num_groups(0)) {
		int masterRootId = rootId + batchStartPosition;
		for (int comboId = get_local_id(0); comboId < J; comboId += get_local_size(0)) {
			for (int classId = 0; classId < classes; classId++) {
				double score = d_scores[rootId * classes * J
						+ classes * comboId + classId];
				double numOfComparesForClass = d_numOfComparesByClass[masterRootId
						* classes + classId];

				double weightedScore = score / numOfComparesForClass;
				d_scores[rootId * classes * J + classes * comboId
						+ classId] = weightedScore;
			}
		}
	}
}
);

	/**
	 * Weights the score by the number of compares for each class to make the results comparable in cuda
	 */
__global__ void scaleScoresForClassSize() {

	for (int rootId = blockIdx.x; rootId < batchSize; rootId += gridDim.x) {
		int masterRootId = rootId + batchStartPosition;
		for (int comboId = threadIdx.x; comboId < J; comboId += blockDim.x) {
			for (int classId = 0; classId < classes; classId++) {
				double score = d_scores[rootId * classes * J
						+ classes * comboId + classId];
				double numOfComparesForClass = d_numOfComparesByClass[masterRootId
						* classes + classId];

				double weightedScore = score / numOfComparesForClass;
				d_scores[rootId * classes * J + classes * comboId
						+ classId] = weightedScore;
			}
		}
	}
}

/**
 * Weights the score by the number of compares for each class to make the results comparable in opencl
 */
 const char* kernel_source_9 = STRINGIFY(
__kernel void scaleFloatScoresForClassSize_ocl(__global float* d_scores_float, __global int* d_numOfComparesByClass,
		const int batchSize, const int batchStartPosition, const int classes, const int J) {

	for (int rootId = get_group_id(0); rootId < batchSize; rootId += get_num_groups(0)) {
		int masterRootId = rootId + batchStartPosition;
		for (int comboId = get_local_id(0); comboId < J; comboId += get_local_size(0)) {
			for (int classId = 0; classId < classes; classId++) {
				float score = d_scores_float[rootId * classes * J
					+ classes * comboId + classId];
				float numOfComparesForClass = d_numOfComparesByClass[masterRootId
					* classes + classId];

				float weightedScore = score / numOfComparesForClass;
				d_scores_float[rootId * classes * J + classes * comboId
					+ classId] = weightedScore;
		}
	}
}
}
);

/**
 * Weights the score by the number of compares for each class to make the results comparable in cuda
 */
__global__ void scaleFloatScoresForClassSize() {

	for (int rootId = blockIdx.x; rootId < batchSize; rootId += gridDim.x) {
		int masterRootId = rootId + batchStartPosition;
		for (int comboId = threadIdx.x; comboId < J; comboId += blockDim.x) {
			for (int classId = 0; classId < classes; classId++) {
				float score = d_scores_float[rootId * classes * J
					+ classes * comboId + classId];
				float numOfComparesForClass = d_numOfComparesByClass[masterRootId
					* classes + classId];

				float weightedScore = score / numOfComparesForClass;
				d_scores_float[rootId * classes * J + classes * comboId
					+ classId] = weightedScore;
		}
	}
}
}



extern "C" void scaleScoresForClassSizeWrapper(DataHolder dataHolder, ResultsHolder resultsHolder,	dim3 blocks, dim3 threads) {
	if(dataHolder.implementation==1){
		setupConstantDataScaleScoresForClassSize((int*) &dataHolder.dataTrain.classes, (int*) &dataHolder.classifier.h_currentBatchSize, (int*) &dataHolder.classifier.h_currentBatchStartPos,
			(int*) &dataHolder.classifier.h_J);

		setupDevicePointersScaleScoresForClassSize<<<1,1>>>(dataHolder.dataTest.d_numOfComparesByClass, resultsHolder.d_scores_float, resultsHolder.d_scores);

		if(dataHolder.h_method_ref == 0){
			scaleFloatScoresForClassSize<<<blocks, threads>>>();
		}else{
			scaleScoresForClassSize<<<blocks, threads>>>();
		}

		cudaError_t cudaCodeScaleScores = cudaGetLastError();
		if(cudaCodeScaleScores!=cudaSuccess)
		{	printCudaErrorCode("error in scale scores kernel: ", cudaCodeScaleScores);}
	}
	else if (dataHolder.implementation==2) {
	  if(dataHolder.h_method_ref == 0)
	  {
		cl_int ret;
		cl_program clpgm;
		clpgm = clCreateProgramWithSource(dataHolder.clctx,1, &kernel_source_9, NULL, &ret);

		char clcompileflags[4096]="";
		ret = clBuildProgram(clpgm, 0, NULL, clcompileflags, NULL, NULL);
		cl_kernel clkern = clCreateKernel(clpgm, "scaleFloatScoresForClassSize_ocl", &ret);
		cl_event event;
		size_t global_item_size = blocks.x * threads.x;
		size_t local_item_size = threads.x;

		ret = clSetKernelArg(clkern,0,sizeof(cl_mem),(void*)&resultsHolder.d_scores_float_ocl);
		ret = clSetKernelArg(clkern,1,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_numOfComparesByClass_ocl);
		ret = clSetKernelArg(clkern,2,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchSize);
		ret = clSetKernelArg(clkern,3,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchStartPos);
		ret = clSetKernelArg(clkern,4,sizeof(int),(void*)&dataHolder.dataTrain.classes);
		ret = clSetKernelArg(clkern,5,sizeof(int),(void*)&dataHolder.classifier.h_J);
		
		ret = clEnqueueNDRangeKernel(dataHolder.clcmdq,clkern,1,NULL,&global_item_size,&local_item_size,0,NULL,&event);
		ret = clWaitForEvents(1,&event);
		ret=clReleaseEvent(event);

	  }else{
		cl_int ret;
		cl_program clpgm;
		clpgm = clCreateProgramWithSource(dataHolder.clctx,1, &kernel_source_8, NULL, &ret);

		char clcompileflags[4096]="";
		ret = clBuildProgram(clpgm, 0, NULL, clcompileflags, NULL, NULL);

		cl_kernel clkern = clCreateKernel(clpgm, "scaleScoresForClassSize_ocl", &ret);
		cl_event event;
		size_t global_item_size = blocks.x * threads.x;
		size_t local_item_size = threads.x;

		ret = clSetKernelArg(clkern,0,sizeof(cl_mem),(void*)&resultsHolder.d_scores_ocl);
		ret = clSetKernelArg(clkern,1,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_numOfComparesByClass_ocl);
		ret = clSetKernelArg(clkern,2,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchSize);
		ret = clSetKernelArg(clkern,3,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchStartPos);
		ret = clSetKernelArg(clkern,4,sizeof(int),(void*)&dataHolder.dataTrain.classes);
		ret = clSetKernelArg(clkern,5,sizeof(int),(void*)&dataHolder.classifier.h_J);

		ret = clEnqueueNDRangeKernel(dataHolder.clcmdq,clkern,1,NULL,&global_item_size,&local_item_size,0,NULL,&event);

		ret = clWaitForEvents(1,&event);
		ret=clReleaseEvent(event);
	  }
	}
}

