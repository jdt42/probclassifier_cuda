#include "../printCudaErrorCode.hpp"
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

#include <thrust/copy.h>
#include <thrust/fill.h>
#include <thrust/sequence.h>
#include "../DataStructure.hpp"
#include "../DataHolder.hpp"
#include "../ResultsHolder.hpp"
#pragma OPENCL EXTENSION cl_amd_fp64 : enable
#include <CL/cl.h>

#include <iostream>
__constant__ double prw_param;
__constant__ int N;
__constant__ int L;
__constant__ int classes;
__constant__ int batchSize;
__constant__ int batchStartPosition;
__constant__ int Q;
__constant__ int J;
__constant__ int method_ref;

__device__ float* d_scores_float;
__device__ double* d_scores;
__device__ int *d_in_combo;
__device__ int *d_in_comboBreakPoints;
__device__	int *d_in_tr;
__device__ double *d_inNormalised_tr;
__device__ int *d_inSparseVectorBreakPoints_tr;
__device__ int *d_classMember_tr;
__device__ int *d_kfoldMember_tr;
__device__	int *d_in_ts;
__device__ double *d_inNormalised_ts;
__device__ int *d_inSparseVectorBreakPoints_ts;
__device__ int *d_classMember_ts;
__device__ int *d_kfoldMember_ts;
__device__ double *d_prw_lookupTable;


void setupConstantDataCalculateScores(int *h_N,int *h_L,int *h_classes, double *h_prw_param,
		int *h_batchSize, int *h_batchStartPosition, int *h_Q, int *h_J, int *h_method_ref)
{
	cudaMemcpyToSymbol(prw_param,h_prw_param,sizeof(double));
	cudaMemcpyToSymbol(N,h_N,sizeof(int));
	cudaMemcpyToSymbol(L,h_L,sizeof(int));
	cudaMemcpyToSymbol(classes,h_classes,sizeof(int));
	cudaMemcpyToSymbol(batchSize,h_batchSize,sizeof(int));
	cudaMemcpyToSymbol(batchStartPosition,h_batchStartPosition,sizeof(int));
	cudaMemcpyToSymbol(Q,h_Q,sizeof(int));
	cudaMemcpyToSymbol(J,h_J,sizeof(int));
	cudaMemcpyToSymbol(method_ref,h_method_ref,sizeof(int));

}

__global__ void setUpDevicePointersCalculateScores(double *scores, float *scores_float, int *in_combo, int *in_comboBreakPoints,
		int *in_tr, double *inNormalised_tr, int *in_SparseVectorBreakPoints_tr, int *classMember_tr, int *kfoldMember_tr,
		int *in_ts, double *inNormalised_ts, int *in_SparseVectorBreakPoints_ts, int *classMember_ts, int *kfoldMember_ts,
		double *prw_lookupTable, int h_method_ref)
{
	d_scores_float = (float*) scores_float;
	d_scores = (double*) scores;
	d_in_combo = (int*) in_combo;
	d_in_comboBreakPoints = (int*) in_comboBreakPoints;
	d_in_tr = (int*) in_tr;
	d_inNormalised_tr = (double*) inNormalised_tr;
	d_inSparseVectorBreakPoints_tr = (int*) in_SparseVectorBreakPoints_tr,
	d_classMember_tr = (int*) classMember_tr;
	d_kfoldMember_tr = (int*) kfoldMember_tr;
	d_in_ts = (int*) in_ts;
	d_inNormalised_ts = (double*) inNormalised_ts;
	d_inSparseVectorBreakPoints_ts = (int*) in_SparseVectorBreakPoints_ts,
	d_classMember_ts = (int*) classMember_ts;
	d_kfoldMember_ts = (int*) kfoldMember_ts;

	if(method_ref==1)
	{
		d_prw_lookupTable = (double*) prw_lookupTable;
	}

}

/**
 * Calculates the scores using prw for each data point for each combination in opencl
 */
#define STRINGIFY(A) #A
const char* kernel_source_15 = STRINGIFY(

\n#pragma OPENCL EXTENSION cl_khr_fp64 : enable\n
__kernel void calculateScoresPrw_ocl(__global double* d_scores, __global double* d_prw_lookupTable, __global double* d_inNormalised_tr, __global double* d_inNormalised_ts, __global int* d_in_comboBreakPoints,__global int* d_in_combo,
		__global int* d_kfoldMember_tr,__global int* d_kfoldMember_ts, __global int* d_classMember_tr, const int batchSize, const int batchStartPosition, const int J, const int N, const int L, const int classes, const double prw_param)
{
	for (int rootId=get_group_id(0); rootId<batchSize; rootId+=get_num_groups(0)) {
		int masterRootId = rootId + batchStartPosition;

		for (int comboId=get_local_id(0); comboId<J; comboId+=get_local_size(0)) {
			for (int targetId=0; targetId<N; targetId++) {

				//only proceed if root and target are from different parts of the kfold split
				if (d_kfoldMember_ts[masterRootId] != d_kfoldMember_tr[targetId]) {
					double score = 0.0;
					double diffSum = 0.0;

					for(int i=d_in_comboBreakPoints[comboId]; i<d_in_comboBreakPoints[comboId+1]; i++) {
						int featureNum = d_in_combo[i];
						//double featureDiff = abs(d_in_ts[masterRootId*L + featureNum] - d_in_tr[targetId*L + featureNum]);
						//double featureDiff = abs(d_inNormalised_ts[masterRootId*L + featureNum] - d_inNormalised_tr[targetId*L + featureNum]);
						double featureDiff = d_inNormalised_ts[masterRootId*L + featureNum] - d_inNormalised_tr[targetId*L + featureNum];
						//if(featureDiff < 0.0){
						//	featureDiff = featureDiff * -1.0;
						//}
						diffSum = diffSum + (featureDiff*featureDiff);
					}

		//			double intPart;
					//if(modf(diffSum,&intPart)==0 && diffSum<256){
					//	score = d_prw_lookupTable[(int) intPart];
					//}else{
						score = exp(-1.0 * (diffSum / (2.0*prw_param*prw_param)));
		//			}

					d_scores[rootId*classes*J + classes*comboId + d_classMember_tr[targetId]] =
								d_scores[rootId*classes*J + classes*comboId + d_classMember_tr[targetId]] +
								score;

				}
			}
		}
	}
}
);


/**
 * Calculates the scores using prw for each data point for each combination with a sparse representation in opencl
 */
 	  const char* kernel_source_16 = STRINGIFY(
\n#pragma OPENCL EXTENSION cl_khr_fp64 : enable\n
__kernel void calculateScoresPrwSparse_ocl(__global double* d_scores, __global double* d_prw_lookupTable, __global int* d_in_tr, __global int* d_in_ts, __global int* d_in_comboBreakPoints,
		__global int* d_in_combo, __global int* d_classMember_tr,__global int* d_kfoldMember_tr,__global int* d_kfoldMember_ts, __global int* d_inSparseVectorBreakPoints_tr, __global int* d_inSparseVectorBreakPoints_ts,
		const int batchSize, const int batchStartPosition, const int J, const int N, const int L, const int classes, const double prw_param) {

	for (int rootId = get_group_id(0); rootId < batchSize; rootId += get_num_groups(0)) {
		int masterRootId = rootId + batchStartPosition;

		for (int comboId = get_local_id(0); comboId < J; comboId += get_local_size(0)) {
			for (int targetId=0; targetId < N; targetId++) {
				//int classId = d_classMember[targetId];

				//only proceed if root and target are from different parts of the kfold split
				if (d_kfoldMember_ts[masterRootId] != d_kfoldMember_tr[targetId]) {
					double score = 0.0;
					int diffSum = 0.0;

					__global int* rootPointer = &d_in_ts[d_inSparseVectorBreakPoints_ts[rootId + batchStartPosition]];
					__global int * targetPointer = &d_in_tr[d_inSparseVectorBreakPoints_tr[targetId]];

					//calculate the distance between the root and target feature vector subset
					for(int i=d_in_comboBreakPoints[comboId]; i<d_in_comboBreakPoints[comboId+1]; i++) {
						int featureNum = d_in_combo[i];

						int featureRoot=0;
						int featureTarget=0;

						while(*rootPointer<=featureNum && rootPointer<&d_in_ts[d_inSparseVectorBreakPoints_ts[masterRootId+1]]) {
							if (*rootPointer==featureNum) {
								featureRoot=1;
							}
							rootPointer++;
						}

						while(*targetPointer<=featureNum && targetPointer<&d_in_tr[d_inSparseVectorBreakPoints_tr[targetId+1]]) {
							if (*targetPointer==featureNum) {
								featureTarget=1;
							}
							targetPointer++;
						}

						double featureDiff = (featureRoot - featureTarget);
						diffSum = diffSum + (featureDiff * featureDiff);

					}

					score = exp(-1.0 * (diffSum / (2.0*prw_param*prw_param)));
					d_scores[rootId * classes * J + classes * comboId + d_classMember_tr[targetId]] =
								d_scores[rootId * classes * J + classes * comboId + d_classMember_tr[targetId]] +
								score;

				}

			}
		}
	}
}
);



/**
 * Calculates the scores using rascal for each data point for each combination in cuda
 */
const char* kernel_source_19 = STRINGIFY(
	  __kernel void calculateScoresRascal_ocl(__global float* d_scores_float, __global int* d_in_tr, __global int* d_in_ts, __global int* d_in_comboBreakPoints,__global int* d_in_combo,
			 __global int* d_classMember_tr, __global int* d_kfoldMember_tr,__global int* d_kfoldMember_ts,
			 const int batchSize, const int batchStartPosition, const int J, const int N, const int L, const int classes) {

	for (int rootId = get_group_id(0); rootId < batchSize; rootId += get_num_groups(0)) {
			int masterRootId = rootId + batchStartPosition;

			for (int comboId = get_local_id(0); comboId < J; comboId += get_local_size(0)) {
				for (int targetId=0; targetId < N; targetId++) {
					int classId = d_classMember_tr[targetId];

					//only proceed if root and target are from different parts of the kfold split
					if (d_kfoldMember_ts[masterRootId] != d_kfoldMember_tr[targetId]) {
						int diffSum = 0;

						for(int i=d_in_comboBreakPoints[comboId]; i<d_in_comboBreakPoints[comboId+1]; i++) {
							int featureNum = d_in_combo[i];
							int featureDiff = (d_in_ts[masterRootId * L + featureNum] - d_in_tr[targetId * L + featureNum]);
							diffSum = diffSum + featureDiff;

							if (diffSum != 0) {
								break;
							}
						}
						if (diffSum == 0) {
							d_scores_float[rootId * classes * J + classes * comboId + classId] =
									d_scores_float[rootId * classes * J + classes * comboId + classId] + 1.0;
						}
					}
				}
			}
		}

}
);

/**
 * Calculates the scores using rascal for each data point for each combination in opencl
 */
 	  const char* kernel_source_20 = STRINGIFY(
__kernel void calculateScoresRascalSparse_ocl(__global float* d_scores_float, __global int* d_in_tr, __global int* d_in_ts, __global int* d_in_comboBreakPoints,
		__global int* d_in_combo, __global int* d_classMember_tr,__global int* d_kfoldMember_tr,__global int* d_kfoldMember_ts, __global int* d_inSparseVectorBreakPoints_tr, __global int* d_inSparseVectorBreakPoints_ts,
		const int batchSize, const int batchStartPosition, const int J, const int N, const int L, const int classes) {

 		 for (int rootId = get_group_id(0); rootId < batchSize; rootId += get_num_groups(0)) {
 		 		int masterRootId = rootId + batchStartPosition;
 		 		for (int comboId = get_local_id(0); comboId < J; comboId += get_local_size(0)) {
 		 			for (int targetId=0; targetId < N; targetId++) {
 		 				int classId = d_classMember_tr[targetId];

 		 				//only proceed if root and target are from different parts of the kfold split
 		 				if (d_kfoldMember_ts[masterRootId] != d_kfoldMember_tr[targetId]) {
 		 					int diffSum = 0;

 		 					__global int * rootPointer = &d_in_ts[d_inSparseVectorBreakPoints_ts[masterRootId]];
 		 					__global int * targetPointer = &d_in_tr[d_inSparseVectorBreakPoints_tr[targetId]];

 		 					//calculate the distance between the root and target feature vector subset
 		 					for(int i=d_in_comboBreakPoints[comboId]; i<d_in_comboBreakPoints[comboId+1]; i++) {
 		 						int featureNum = d_in_combo[i];
 		 						int featureRoot=0;
 		 						int featureTarget=0;

 		 						while(*rootPointer <= featureNum && rootPointer < &d_in_ts[d_inSparseVectorBreakPoints_ts[masterRootId+1]]) {
 		 							if (*rootPointer==featureNum) {
 		 								featureRoot=1;
 		 							}
 		 							rootPointer++;
 		 						}

 		 						while(*targetPointer <= featureNum && targetPointer < &d_in_tr[d_inSparseVectorBreakPoints_tr[targetId+1]]) {
 		 							if (*targetPointer==featureNum) {
 		 								featureTarget=1;
 		 							}
 		 							targetPointer++;
 		 						}

 		 						diffSum = diffSum + (featureRoot - featureTarget);

 		 						//break out of loop for rascal once the subsamples differ
 		 						if (diffSum != 0) {
 		 							break;
 		 						}
 		 					}

 		 					//if subsamples are identical add one
 		 					if (diffSum == 0) {
 		 						d_scores_float[rootId * classes * J + classes * comboId + classId] =
 		 								d_scores_float[rootId * classes * J + classes * comboId + classId] + 1.0;
 		 					}
 		 				}
 		 			}
 		 		}
 		 	}
}
);





/**
 * Calculates the scores using prw for each data point for each combination in cuda
 */
__global__ void calculateScoresPrw() {

	for (int rootId=blockIdx.x; rootId<batchSize; rootId+=gridDim.x) {
		int masterRootId = rootId + batchStartPosition;

		for (int comboId=threadIdx.x; comboId<J; comboId+=blockDim.x) {
			for (int targetId=0; targetId<N; targetId++) {

				//only proceed if root and target are from different parts of the kfold split
				if (d_kfoldMember_ts[masterRootId] != d_kfoldMember_tr[targetId]) {
					double score = 0.0;
					double diffSum = 0.0;

					for(int i=d_in_comboBreakPoints[comboId]; i<d_in_comboBreakPoints[comboId+1]; i++) {
						int featureNum = d_in_combo[i];
						//double featureDiff = abs(d_in_ts[masterRootId*L + featureNum] - d_in_tr[targetId*L + featureNum]);
						double featureDiff = (d_inNormalised_ts[masterRootId*L + featureNum] - d_inNormalised_tr[targetId*L + featureNum]);
						diffSum = diffSum + (featureDiff*featureDiff);
					}

					double intPart;
					if(modf(diffSum,&intPart)==0 && diffSum<256){
						score = d_prw_lookupTable[(int) intPart];
					}else{
						score = exp(-1.0 * (diffSum / (2.0*prw_param*prw_param)));
					}

					d_scores[rootId*classes*J + classes*comboId + d_classMember_tr[targetId]] =
								d_scores[rootId*classes*J + classes*comboId + d_classMember_tr[targetId]] +
								score;

				}
			}
		}
	}
}



/**
 * Calculates the scores using prw for each data point for each combination with a sparse representation in cuda
 */
__global__ void calculateScoresPrwSparse() {
	for (int rootId = blockIdx.x; rootId < batchSize; rootId += gridDim.x) {
		int masterRootId = rootId + batchStartPosition;

		for (int comboId = threadIdx.x; comboId < J; comboId += blockDim.x) {
			for (int targetId=0; targetId < N; targetId++) {
				//int classId = d_classMember[targetId];

				//only proceed if root and target are from different parts of the kfold split
				if (d_kfoldMember_ts[masterRootId] != d_kfoldMember_tr[targetId]) {
					double score = 0.0;
					int diffSum = 0.0;

					int * rootPointer = &d_in_ts[d_inSparseVectorBreakPoints_ts[rootId + batchStartPosition]];
					int * targetPointer = &d_in_tr[d_inSparseVectorBreakPoints_tr[targetId]];

					//calculate the distance between the root and target feature vector subset
					for(int i=d_in_comboBreakPoints[comboId]; i<d_in_comboBreakPoints[comboId+1]; i++) {
						int featureNum = d_in_combo[i];

						int featureRoot=0;
						int featureTarget=0;

						while(*rootPointer<=featureNum && rootPointer<&d_in_ts[d_inSparseVectorBreakPoints_ts[masterRootId+1]]) {
							if (*rootPointer==featureNum) {
								featureRoot=1;
							}
							rootPointer++;
						}

						while(*targetPointer<=featureNum && targetPointer<&d_in_tr[d_inSparseVectorBreakPoints_tr[targetId+1]]) {
							if (*targetPointer==featureNum) {
								featureTarget=1;
							}
							targetPointer++;
						}

						double featureDiff = featureRoot - featureTarget;
						diffSum = diffSum + (featureDiff * featureDiff);
					}
					//double exponent = diffSum / (2.0 * *d_pzw_param * *d_pzw_param);
					double intPart;
					if(modf(diffSum,&intPart)==0 && diffSum<256){
						score = d_prw_lookupTable[(int) intPart];
					}else{
						score = exp(-1.0 * (diffSum / (2.0*prw_param*prw_param)));
					}

					d_scores[rootId * classes * J + classes * comboId + d_classMember_tr[targetId]] =
								d_scores[rootId * classes * J + classes * comboId + d_classMember_tr[targetId]] +
								score;

				}
			}
		}
	}
}



/**
 * Calculates the scores using rascal for each data point for each combination in cuda
 */
__global__ void calculateScoresRascal() {

	for (int rootId = blockIdx.x; rootId < batchSize; rootId += gridDim.x) {
		int masterRootId = rootId + batchStartPosition;

		for (int comboId = threadIdx.x; comboId < J; comboId += blockDim.x) {
			for (int targetId=0; targetId < N; targetId++) {
				int classId = d_classMember_tr[targetId];

				//only proceed if root and target are from different parts of the kfold split
				if (d_kfoldMember_ts[masterRootId] != d_kfoldMember_tr[targetId]) {
					int diffSum = 0;

					for(int i=d_in_comboBreakPoints[comboId]; i<d_in_comboBreakPoints[comboId+1]; i++) {
						int featureNum = d_in_combo[i];
						int featureDiff = abs(d_in_ts[masterRootId * L + featureNum] - d_in_tr[targetId * L + featureNum]);
						diffSum = diffSum + featureDiff;

						if (diffSum > 0) {
							break;
						}
					}
					if (diffSum == 0) {
						d_scores_float[rootId * classes * J + classes * comboId + classId] =
								d_scores_float[rootId * classes * J + classes * comboId + classId] + 1.0;
					}
				}
			}
		}
	}
}


/**
 * Calculates the scores using rascal for each data point for each combination in cuda
 */
__global__ void calculateScoresRascalSparse() {

	for (int rootId = blockIdx.x; rootId < batchSize; rootId += gridDim.x) {
		int masterRootId = rootId + batchStartPosition;
		for (int comboId = threadIdx.x; comboId < J; comboId += blockDim.x) {
			for (int targetId=0; targetId < N; targetId++) {
				int classId = d_classMember_tr[targetId];

				//only proceed if root and target are from different parts of the kfold split
				if (d_kfoldMember_ts[masterRootId] != d_kfoldMember_tr[targetId]) {
					int diffSum = 0;

					int * rootPointer = &d_in_ts[d_inSparseVectorBreakPoints_ts[masterRootId]];
					int * targetPointer = &d_in_tr[d_inSparseVectorBreakPoints_tr[targetId]];

					//calculate the distance between the root and target feature vector subset
					for(int i=d_in_comboBreakPoints[comboId]; i<d_in_comboBreakPoints[comboId+1]; i++) {
						int featureNum = d_in_combo[i];
						int featureRoot=0;
						int featureTarget=0;

						while(*rootPointer <= featureNum && rootPointer < &d_in_ts[d_inSparseVectorBreakPoints_ts[masterRootId+1]]) {
							if (*rootPointer==featureNum) {
								featureRoot=1;
							}
							rootPointer++;
						}

						while(*targetPointer <= featureNum && targetPointer < &d_in_tr[d_inSparseVectorBreakPoints_tr[targetId+1]]) {
							if (*targetPointer==featureNum) {
								featureTarget=1;
							}
							targetPointer++;
						}

						diffSum = diffSum + abs(featureRoot - featureTarget);

						//break out of loop for rascal once the subsamples differ
						if (diffSum > 0) {
							break;
						}
					}

					//if subsamples are identical add one
					if (diffSum == 0) {
						d_scores_float[rootId * classes * J + classes * comboId + classId] =
								d_scores_float[rootId * classes * J + classes * comboId + classId] + 1.0;
					}
				}
			}
		}
	}
}

/**
 * Calculates the scores using rascal for each data point for each combination in cuda
 */
__global__ void calculateScoresRascalNew() {

	for (int rootId = blockIdx.x; rootId < batchSize; rootId += gridDim.x) {
		int masterRootId = rootId + batchStartPosition;

		for(int targetId = blockIdx.y; targetId < N; targetId += gridDim.y) {

			//only proceed if root and target are from different parts of the kfold split
			if (d_kfoldMember_ts[masterRootId] != d_kfoldMember_tr[targetId]) {
				int diffSum = 0;
				for (int comboId = threadIdx.x; comboId < J; comboId += blockDim.x) {
					for(int i=d_in_comboBreakPoints[comboId]; i<d_in_comboBreakPoints[comboId+1]; i++) {
						int featureNum = d_in_combo[i];
						int featureDiff = abs(d_in_ts[masterRootId * L + featureNum] - d_in_tr[targetId * L + featureNum]);
						diffSum = diffSum + featureDiff;

						if (diffSum > 0) {
							break;
						}
					}
					if (diffSum == 0) {
						//d_scores[rootId * classes * J + classes * comboId + classId] =
						//		d_scores[rootId * classes * J + classes * comboId + classId] + 1.0;
						atomicAdd(&d_scores_float[rootId * classes * J + classes * comboId + d_classMember_tr[targetId]],1.0);
					}
				}
			}
		}
	}
}

/**
 * Calculates the scores using rascal for each data point for each combination in cuda
 */
__global__ void calculateScoresRascalSparseNew() {

	for (int rootId = blockIdx.x; rootId < batchSize; rootId += gridDim.x) {
		int masterRootId = rootId + batchStartPosition;

		for(int targetId = blockIdx.y; targetId < N; targetId += gridDim.y) {
			int classId = d_classMember_tr[targetId];

			//only proceed if root and target are from different parts of the kfold split
			if (d_kfoldMember_ts[masterRootId] != d_kfoldMember_tr[targetId]) {
				int diffSum = 0;

				int * rootPointer = &d_in_ts[d_inSparseVectorBreakPoints_ts[masterRootId]];
				int * targetPointer = &d_in_tr[d_inSparseVectorBreakPoints_tr[targetId]];

				for (int comboId = threadIdx.x; comboId < J; comboId += blockDim.x) {
					for(int i=d_in_comboBreakPoints[comboId]; i<d_in_comboBreakPoints[comboId+1]; i++) {
						int featureNum = d_in_combo[i];
						int featureRoot=0;
						int featureTarget=0;

						while(*rootPointer <= featureNum && rootPointer < &d_in_ts[d_inSparseVectorBreakPoints_ts[masterRootId+1]]) {
							if (*rootPointer==featureNum) {
								featureRoot=1;
							}
							rootPointer++;
						}

						while(*targetPointer <= featureNum && targetPointer < &d_in_tr[d_inSparseVectorBreakPoints_tr[targetId+1]]) {
							if (*targetPointer==featureNum) {
								featureTarget=1;
							}
							targetPointer++;
						}

						diffSum = diffSum + abs(featureRoot - featureTarget);

						//break out of loop for rascal once the subsamples differ
						if (diffSum > 0) {
							break;
						}
					}

					if (diffSum == 0) {
						//d_scores[rootId * classes * J + classes * comboId + classId] =
						//		d_scores[rootId * classes * J + classes * comboId + classId] + 1.0;
						atomicAdd(&d_scores_float[rootId * classes * J + classes * comboId + classId],1.0);
					}
				}
			}
		}
	}
}



extern "C" void calculateScoresWrapper(DataHolder dataHolder, ResultsHolder resultsHolder, dim3 blocks, dim3 threads,dim3 blocksRascal) {

	if(dataHolder.implementation==1){
		setupConstantDataCalculateScores((int*) &dataHolder.dataTrain.N, (int*) &dataHolder.dataTest.L,(int*) &dataHolder.dataTest.classes,(double*)&dataHolder.prwParam,
			(int*) &dataHolder.classifier.h_currentBatchSize, (int*) &dataHolder.classifier.h_currentBatchStartPos,(int*) &dataHolder.classifier.h_q,(int*) &dataHolder.classifier.h_J,
			(int*) &dataHolder.h_method_ref);

		setUpDevicePointersCalculateScores<<<1,1>>>(resultsHolder.d_scores, resultsHolder.d_scores_float,dataHolder.d_in_combo,dataHolder.d_comboBreakPoints,
			dataHolder.dataTrain.d_in, dataHolder.dataTrain.d_inNormalised, dataHolder.dataTrain.d_inSparseVectorBreakPoints, dataHolder.dataTrain.d_classMember, dataHolder.dataTrain.d_kfoldMember,
			dataHolder.dataTest.d_in, dataHolder.dataTest.d_inNormalised, dataHolder.dataTest.d_inSparseVectorBreakPoints, dataHolder.dataTest.d_classMember, dataHolder.dataTest.d_kfoldMember,
			dataHolder.d_prw_lookupTable, dataHolder.h_method_ref);

		if(dataHolder.h_method_ref==1)
		{
			if(dataHolder.dataFormat==0)
			{
				calculateScoresPrw<<<blocks, threads>>>();
			}
			else if(dataHolder.dataFormat==1)
			{
				calculateScoresPrwSparse<<<blocks, threads>>>();
			}
		}
		else if(dataHolder.h_method_ref==0)
		{

			if(dataHolder.dataFormat==0)
			{
				//calculateScoresRascal<<<blocks, threads>>>();
				calculateScoresRascalNew<<<blocksRascal, threads>>>();

			}
			else if(dataHolder.dataFormat==1)
			{
				//calculateScoresRascalSparse<<<blocks, threads>>>();
				calculateScoresRascalSparseNew<<<blocksRascal, threads>>>();
			}
		}

		cudaError_t cudaCodeCalculateScores = cudaGetLastError();
		if(cudaCodeCalculateScores!=cudaSuccess)
		{	printCudaErrorCode("error in calculate scores kernel: ", cudaCodeCalculateScores);}
	}
	else if (dataHolder.implementation==2) {
	  if(dataHolder.h_method_ref==1)
	  {
		if(dataHolder.dataFormat==0)
		{
			cl_int ret;
			cl_program clpgm;

			clpgm = clCreateProgramWithSource(dataHolder.clctx,1, &kernel_source_15, NULL, &ret);

			ret = clBuildProgram(clpgm, 0, NULL, NULL, NULL, NULL);

			cl_kernel clkern = clCreateKernel(clpgm, "calculateScoresPrw_ocl", &ret);
			cl_event event;
			size_t global_item_size = blocks.x * threads.x;
			size_t local_item_size = threads.x;

			ret = clSetKernelArg(clkern,0,sizeof(cl_mem),(void*)&resultsHolder.d_scores_ocl);
			ret = clSetKernelArg(clkern,1,sizeof(cl_mem),(void*)&dataHolder.d_prw_lookupTable_ocl);
			ret = clSetKernelArg(clkern,2,sizeof(cl_mem),(void*)&dataHolder.dataTrain.d_inNormalised_ocl);
			ret = clSetKernelArg(clkern,3,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_inNormalised_ocl);
			ret = clSetKernelArg(clkern,4,sizeof(cl_mem),(void*)&dataHolder.d_comboBreakPoints_ocl);
			ret = clSetKernelArg(clkern,5,sizeof(cl_mem),(void*)&dataHolder.d_in_combo_ocl);
			ret = clSetKernelArg(clkern,6,sizeof(cl_mem),(void*)&dataHolder.dataTrain.d_kfoldMember_ocl);
			ret = clSetKernelArg(clkern,7,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_kfoldMember_ocl);
			ret = clSetKernelArg(clkern,8,sizeof(cl_mem),(void*)&dataHolder.dataTrain.d_classMember_ocl);
			ret = clSetKernelArg(clkern,9,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchSize);
			ret = clSetKernelArg(clkern,10,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchStartPos);
			ret = clSetKernelArg(clkern,11,sizeof(int),(void*)&dataHolder.classifier.h_J);
			ret = clSetKernelArg(clkern,12,sizeof(int),(void*)&dataHolder.dataTrain.N);
			ret = clSetKernelArg(clkern,13,sizeof(int),(void*)&dataHolder.dataTest.L);
			ret = clSetKernelArg(clkern,14,sizeof(int),(void*)&dataHolder.dataTrain.classes);
			ret = clSetKernelArg(clkern,15,sizeof(double),(void*)&dataHolder.prwParam);

			ret = clEnqueueNDRangeKernel(dataHolder.clcmdq,clkern,1,NULL,&global_item_size,&local_item_size,0,NULL,&event);
			ret = clWaitForEvents(1,&event);
			ret=clReleaseEvent(event);

		}
		else if(dataHolder.dataFormat==1)
		{
			cl_int ret;
			cl_program clpgm;
			clpgm = clCreateProgramWithSource(dataHolder.clctx,1, &kernel_source_16, NULL, &ret);
			char clcompileflags[4096]="";
			ret = clBuildProgram(clpgm, 0, NULL, clcompileflags, NULL, NULL);
			cl_kernel clkern = clCreateKernel(clpgm, "calculateScoresPrwSparse_ocl", &ret);
			cl_event event;
			size_t global_item_size = blocks.x * threads.x;

			size_t local_item_size = threads.x;
			if (ret != CL_SUCCESS)
			{
			   size_t len;
			   char buffer[2048];

			   printf("Error: Failed to build program executable\n"); //[8]
			   clGetProgramBuildInfo(clpgm, dataHolder.max_device_id, CL_PROGRAM_BUILD_LOG,sizeof(buffer), buffer, &len);
			   printf("%s\n", buffer);
			   exit(1);
			}

			ret = clSetKernelArg(clkern,0,sizeof(cl_mem),(void*)&resultsHolder.d_scores_ocl);

			ret = clSetKernelArg(clkern,1,sizeof(cl_mem),(void*)&dataHolder.d_prw_lookupTable_ocl);
			ret = clSetKernelArg(clkern,2,sizeof(cl_mem),(void*)&dataHolder.dataTrain.d_in_ocl);
			ret = clSetKernelArg(clkern,3,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_in_ocl);
			ret = clSetKernelArg(clkern,4,sizeof(cl_mem),(void*)&dataHolder.d_comboBreakPoints_ocl);
			ret = clSetKernelArg(clkern,5,sizeof(cl_mem),(void*)&dataHolder.d_in_combo_ocl);
			ret = clSetKernelArg(clkern,6,sizeof(cl_mem),(void*)&dataHolder.dataTrain.d_classMember_ocl);
			ret = clSetKernelArg(clkern,7,sizeof(cl_mem),(void*)&dataHolder.dataTrain.d_kfoldMember_ocl);
			ret = clSetKernelArg(clkern,8,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_kfoldMember_ocl);
			ret = clSetKernelArg(clkern,9,sizeof(cl_mem),(void*)&dataHolder.dataTrain.d_inSparseVectorBreakPoints_ocl);
			ret = clSetKernelArg(clkern,10,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_inSparseVectorBreakPoints_ocl);
			ret = clSetKernelArg(clkern,11,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchSize);
			ret = clSetKernelArg(clkern,12,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchStartPos);
			ret = clSetKernelArg(clkern,13,sizeof(int),(void*)&dataHolder.classifier.h_J);
			ret = clSetKernelArg(clkern,14,sizeof(int),(void*)&dataHolder.dataTrain.N);
			ret = clSetKernelArg(clkern,15,sizeof(int),(void*)&dataHolder.dataTest.L);
			ret = clSetKernelArg(clkern,16,sizeof(int),(void*)&dataHolder.dataTrain.classes);
			ret = clSetKernelArg(clkern,17,sizeof(double),(void*)&dataHolder.prwParam);

			ret = clEnqueueNDRangeKernel(dataHolder.clcmdq,clkern,1,NULL,&global_item_size,&local_item_size,0,NULL,&event);
			ret = clWaitForEvents(1,&event);
			ret=clReleaseEvent(event);

		}
	  }
	  else if(dataHolder.h_method_ref==0)
	  {
		if(dataHolder.dataFormat==0)
		{
			cl_int ret;
			cl_program clpgm;
			clpgm = clCreateProgramWithSource(dataHolder.clctx,1, &kernel_source_19, NULL, &ret);
			char clcompileflags[4096]="";
			ret = clBuildProgram(clpgm, 0, NULL, clcompileflags, NULL, NULL);
			cl_kernel clkern = clCreateKernel(clpgm, "calculateScoresRascal_ocl", &ret);
			cl_event event;
			size_t global_item_size = blocks.x * threads.x;
			size_t local_item_size = threads.x;

			ret = clSetKernelArg(clkern,0,sizeof(cl_mem),(void*)&resultsHolder.d_scores_float_ocl);
			ret = clSetKernelArg(clkern,1,sizeof(cl_mem),(void*)&dataHolder.dataTrain.d_in_ocl);
			ret = clSetKernelArg(clkern,2,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_in_ocl);
			ret = clSetKernelArg(clkern,3,sizeof(cl_mem),(void*)&dataHolder.d_comboBreakPoints_ocl);
			ret = clSetKernelArg(clkern,4,sizeof(cl_mem),(void*)&dataHolder.d_in_combo_ocl);
			ret = clSetKernelArg(clkern,5,sizeof(cl_mem),(void*)&dataHolder.dataTrain.d_classMember_ocl);
			ret = clSetKernelArg(clkern,6,sizeof(cl_mem),(void*)&dataHolder.dataTrain.d_kfoldMember_ocl);
			ret = clSetKernelArg(clkern,7,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_kfoldMember_ocl);
			ret = clSetKernelArg(clkern,8,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchSize);
			ret = clSetKernelArg(clkern,9,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchStartPos);
			ret = clSetKernelArg(clkern,10,sizeof(int),(void*)&dataHolder.classifier.h_J);
			ret = clSetKernelArg(clkern,11,sizeof(int),(void*)&dataHolder.dataTrain.N);
			ret = clSetKernelArg(clkern,12,sizeof(int),(void*)&dataHolder.dataTest.L);
			ret = clSetKernelArg(clkern,13,sizeof(int),(void*)&dataHolder.dataTrain.classes);

			ret = clEnqueueNDRangeKernel(dataHolder.clcmdq,clkern,1,NULL,&global_item_size,&local_item_size,0,NULL,&event);
			ret = clWaitForEvents(1,&event);
			ret=clReleaseEvent(event);

		}
		else if(dataHolder.dataFormat==1)
		{
			cl_int ret;
			cl_program clpgm;
			clpgm = clCreateProgramWithSource(dataHolder.clctx,1, &kernel_source_20, NULL, &ret);
			char clcompileflags[4096]="";
			ret = clBuildProgram(clpgm, 0, NULL, clcompileflags, NULL, NULL);
			cl_kernel clkern = clCreateKernel(clpgm, "calculateScoresRascalSparse_ocl", &ret);
			cl_event event;
			size_t global_item_size = blocks.x * threads.x;
			size_t local_item_size = threads.x;

			ret = clSetKernelArg(clkern,0,sizeof(cl_mem),(void*)&resultsHolder.d_scores_float_ocl);
			ret = clSetKernelArg(clkern,1,sizeof(cl_mem),(void*)&dataHolder.dataTrain.d_in_ocl);
			ret = clSetKernelArg(clkern,2,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_in_ocl);
			ret = clSetKernelArg(clkern,3,sizeof(cl_mem),(void*)&dataHolder.d_comboBreakPoints_ocl);
			ret = clSetKernelArg(clkern,4,sizeof(cl_mem),(void*)&dataHolder.d_in_combo_ocl);
			ret = clSetKernelArg(clkern,5,sizeof(cl_mem),(void*)&dataHolder.dataTrain.d_classMember_ocl);
			ret = clSetKernelArg(clkern,6,sizeof(cl_mem),(void*)&dataHolder.dataTrain.d_kfoldMember_ocl);
			ret = clSetKernelArg(clkern,7,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_kfoldMember_ocl);
			ret = clSetKernelArg(clkern,8,sizeof(cl_mem),(void*)&dataHolder.dataTrain.d_inSparseVectorBreakPoints_ocl);
			ret = clSetKernelArg(clkern,9,sizeof(cl_mem),(void*)&dataHolder.dataTest.d_inSparseVectorBreakPoints_ocl);
			ret = clSetKernelArg(clkern,10,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchSize);
			ret = clSetKernelArg(clkern,11,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchStartPos);
			ret = clSetKernelArg(clkern,12,sizeof(int),(void*)&dataHolder.classifier.h_J);
			ret = clSetKernelArg(clkern,13,sizeof(int),(void*)&dataHolder.dataTrain.N);
			ret = clSetKernelArg(clkern,14,sizeof(int),(void*)&dataHolder.dataTest.L);
			ret = clSetKernelArg(clkern,15,sizeof(int),(void*)&dataHolder.dataTrain.classes);
			ret = clEnqueueNDRangeKernel(dataHolder.clcmdq,clkern,1,NULL,&global_item_size,&local_item_size,0,NULL,&event);
			ret = clWaitForEvents(1,&event);
			ret=clReleaseEvent(event);

		}
	}
  }
}

