#include "../printCudaErrorCode.hpp"
#include "../DataStructure.hpp"
#include "../DataHolder.hpp"
#include "../ResultsHolder.hpp"

__constant__ int classes;
__constant__ int batchSize;
__constant__ int batchStartPosition;

__constant__ int J;
__constant__ int J_step;
__constant__ int J_min;
__constant__ int J_numOfSteps;

__device__ float* d_scores_float;
__device__ double* d_scores;
__device__ float* d_class_votes_float;
__device__ double* d_class_votes;
__device__ int* d_randoms;

void setupConstantDataCalculateClassVotesCummulativeScores(int *h_classes, int *h_batchSize,
		int *h_batchStartPosition,int *h_J, int *h_J_step, int *h_J_min, int *h_J_numOfSteps)
{
	cudaMemcpyToSymbol(classes,h_classes,sizeof(int));
	cudaMemcpyToSymbol(batchSize,h_batchSize,sizeof(int));
	cudaMemcpyToSymbol(batchStartPosition,h_batchStartPosition,sizeof(int));

	cudaMemcpyToSymbol(J,h_J,sizeof(int));
	cudaMemcpyToSymbol(J_step,h_J_step,sizeof(int));
	cudaMemcpyToSymbol(J_min,h_J_min,sizeof(int));
	cudaMemcpyToSymbol(J_numOfSteps,h_J_numOfSteps,sizeof(int));
}

__global__ void setUpDevicePointersCalculateClassVotesCummulativeScores(int *randoms,
		float *class_votes_float, float *scores_float,
		double *class_votes, double *scores)
{
	d_scores_float = (float*) scores_float;
	d_scores = (double*) scores;
	d_class_votes_float = (float*) class_votes_float;
	d_class_votes = (double*) class_votes;
	d_randoms = (int*) randoms;
}


	/**
	 * Calculates the votes for each class on a cummulative score basis.  I.e. each random subsample
	 * assessed over all other data points the ratio of the votes to each class.  The voting system is
	 * like proportional representation.  This method is better where there are many classes as it enables
	 * all classes to be ranked. In opencl.
	 */
#define STRINGIFY(A) #A

const char* kernel_source_10 = STRINGIFY(
\n#pragma OPENCL EXTENSION cl_khr_fp64 : enable\n
__kernel void calculateClassVotesCummulativeScores_ocl(__global double* d_class_votes, __global double* d_scores, const int batchSize, const int J, const int J_min,
 const int J_step, const int J_numOfSteps, const int classes) {

	for (int rootId = get_group_id(0); rootId < batchSize; rootId += get_num_groups(0)) {
		int d_stepNum = 0;

		for (int comboId = 0; comboId < J; comboId++) {
			if (comboId > (J_min - 1)) {
				d_stepNum = ((comboId - J_min) / J_step) + 1;
			}

			for (int classId = 0; classId < classes; classId++) {
				double currentScore = d_scores[rootId * J * classes
						+ comboId * classes + classId];
				double oldClassVote = d_class_votes[rootId * J_numOfSteps
						* classes + d_stepNum * classes + classId];
				double newClassVote = oldClassVote + currentScore;
				d_class_votes[rootId * J_numOfSteps * classes
						+ d_stepNum * classes + classId] = newClassVote;
			}
		}
	}

}
);

	/**
	 * Calculates the votes for each class on a cummulative score basis.  I.e. each random subsample
	 * assessed over all other data points the ratio of the votes to each class.  The voting system is
	 * like proportional representation.  This method is better where there are many classes as it enables
	 * all classes to be ranked. In cuda.
	 */
__global__ void calculateClassVotesCummulativeScores() {
	int blockId = blockIdx.x;

	for (int rootId = blockId; rootId < batchSize; rootId += gridDim.x) {
		int d_stepNum = 0;

		for (int comboId = 0; comboId < J; comboId++) {
			if (comboId > (J_min - 1)) {
				d_stepNum = ((comboId - J_min) / J_step) + 1;
			}

			for (int classId = 0; classId < classes; classId++) {
				double currentScore = d_scores[rootId * J * classes
						+ comboId * classes + classId];
				double oldClassVote = d_class_votes[rootId * J_numOfSteps
						* classes + d_stepNum * classes + classId];
				double newClassVote = oldClassVote + currentScore;
				d_class_votes[rootId * J_numOfSteps * classes
						+ d_stepNum * classes + classId] = newClassVote;
			}
		}
	}
}





/**
 * Calculates the votes for each class on a cummulative score basis.  I.e. each random subsample
 * assessed over all other data points the ratio of the votes to each class.  The voting system is
 * like proportional representation.  This method is better where there are many classes as it enables
 * all classes to be ranked. In opencl.
 */
const char* kernel_source_11 = STRINGIFY(
 __kernel void calculateClassVotesFloatCummulativeScores_ocl(__global float* d_class_votes_float, __global float* d_scores_float, const int batchSize, const int J, const int J_min,
 const int J_step, const int J_numOfSteps, const int classes) {

	for (int rootId = get_group_id(0); rootId < batchSize; rootId += get_num_groups(0)) {
		int d_stepNum = 0;

		for (int comboId = 0; comboId < J; comboId++) {
			if (comboId > (J_min - 1)) {
				d_stepNum = ((comboId - J_min) / J_step) + 1;
			}

			for (int classId = 0; classId < classes; classId++) {
				float currentScore = d_scores_float[rootId * J * classes
					+ comboId * classes + classId];
				float oldClassVote = d_class_votes_float[rootId * J_numOfSteps
					* classes + d_stepNum * classes + classId];
				float newClassVote = oldClassVote + currentScore;
				d_class_votes_float[rootId * J_numOfSteps * classes
					+ d_stepNum * classes + classId] = newClassVote;
			}

		}

	}

}
);




/**
 * Calculates the votes for each class on a cummulative score basis.  I.e. each random subsample
 * assessed over all other data points the ratio of the votes to each class.  The voting system is
 * like proportional representation.  This method is better where there are many classes as it enables
 * all classes to be ranked. In cuda.
 */
__global__ void calculateClassVotesFloatCummulativeScores() {
int blockId = blockIdx.x;

for (int rootId = blockId; rootId < batchSize; rootId += gridDim.x) {
	int d_stepNum = 0;

	for (int comboId = 0; comboId < J; comboId++) {
		if (comboId > (J_min - 1)) {
			d_stepNum = ((comboId - J_min) / J_step) + 1;
		}

		for (int classId = 0; classId < classes; classId++) {
			float currentScore = d_scores_float[rootId * J * classes
					+ comboId * classes + classId];
			float oldClassVote = d_class_votes_float[rootId * J_numOfSteps
					* classes + d_stepNum * classes + classId];
			float newClassVote = oldClassVote + currentScore;
			d_class_votes_float[rootId * J_numOfSteps * classes
					+ d_stepNum * classes + classId] = newClassVote;
		}
	}
}
}





extern "C" void calculateClassVotesCummulativeScoresWrapper(DataHolder dataHolder, ResultsHolder resultsHolder,dim3 blocks,
		dim3 threads) {
	if(dataHolder.implementation==1){
		setupConstantDataCalculateClassVotesCummulativeScores((int*) &dataHolder.dataTrain.classes, (int*) &dataHolder.classifier.h_currentBatchSize,
			(int*) &dataHolder.classifier.h_currentBatchStartPos,(int*) &dataHolder.classifier.h_J, (int*) &dataHolder.classifier.h_J_step, (int*) &dataHolder.classifier.h_J_min,
			(int*) &dataHolder.classifier.h_J_numOfSteps);

		setUpDevicePointersCalculateClassVotesCummulativeScores<<<1,1>>>(dataHolder.d_randoms, resultsHolder.d_class_votes_float, resultsHolder.d_scores_float, resultsHolder.d_class_votes, resultsHolder.d_scores);

		if(dataHolder.h_method_ref==0 || dataHolder.h_method_ref==2)
		{
			calculateClassVotesFloatCummulativeScores<<<blocks, threads>>>();
		}
		else
		{
			calculateClassVotesCummulativeScores<<<blocks, threads>>>();
		}
		cudaError_t cudaCodeCalculateClassVotesCummulativeScores = cudaGetLastError();
		if(cudaCodeCalculateClassVotesCummulativeScores!=cudaSuccess)
		{	printCudaErrorCode("error in calculate class votes cummulative scores kernel: ", cudaCodeCalculateClassVotesCummulativeScores);}
	}
	else if (dataHolder.implementation==2) {
	  if(dataHolder.h_method_ref==0 || dataHolder.h_method_ref==2)
	  {
		cl_int ret;
		cl_program clpgm;
		clpgm = clCreateProgramWithSource(dataHolder.clctx,1, &kernel_source_11, NULL, &ret);
		char clcompileflags[4096]="";
		ret = clBuildProgram(clpgm, 0, NULL, clcompileflags, NULL, NULL);
		cl_kernel clkern = clCreateKernel(clpgm, "calculateClassVotesFloatCummulativeScores_ocl", &ret);
		cl_event event;
		size_t global_item_size = blocks.x * threads.x;
		size_t local_item_size = threads.x;

		
		ret = clSetKernelArg(clkern,0,sizeof(cl_mem),(void*)&resultsHolder.d_class_votes_float_ocl);
		ret = clSetKernelArg(clkern,1,sizeof(cl_mem),(void*)&resultsHolder.d_scores_float_ocl);
		ret = clSetKernelArg(clkern,2,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchSize);
		ret = clSetKernelArg(clkern,3,sizeof(int),(void*)&dataHolder.classifier.h_J);
		ret = clSetKernelArg(clkern,4,sizeof(int),(void*)&dataHolder.classifier.h_J_min);
		ret = clSetKernelArg(clkern,5,sizeof(int),(void*)&dataHolder.classifier.h_J_step);
		ret = clSetKernelArg(clkern,6,sizeof(int),(void*)&dataHolder.classifier.h_J_numOfSteps);
		ret = clSetKernelArg(clkern,7,sizeof(int),(void*)&dataHolder.dataTrain.classes);
		
		ret = clEnqueueNDRangeKernel(dataHolder.clcmdq,clkern,1,NULL,&global_item_size,&local_item_size,0,NULL,&event);
		ret = clWaitForEvents(1,&event);
		ret=clReleaseEvent(event);

      }else{
		cl_int ret;
		cl_program clpgm;
		clpgm = clCreateProgramWithSource(dataHolder.clctx,1, &kernel_source_10, NULL, &ret);
		char clcompileflags[4096]="";
		ret = clBuildProgram(clpgm, 0, NULL, clcompileflags, NULL, NULL);
		cl_kernel clkern = clCreateKernel(clpgm, "calculateClassVotesCummulativeScores_ocl", &ret);
		cl_event event;
		size_t global_item_size = blocks.x * threads.x;
		size_t local_item_size = threads.x;
		cout << "error: " << ret << endl;
		ret = clSetKernelArg(clkern,0,sizeof(cl_mem),(void*)&resultsHolder.d_class_votes_ocl);
		ret = clSetKernelArg(clkern,1,sizeof(cl_mem),(void*)&resultsHolder.d_scores_ocl);
		ret = clSetKernelArg(clkern,2,sizeof(int),(void*)&dataHolder.classifier.h_currentBatchSize);
		ret = clSetKernelArg(clkern,3,sizeof(int),(void*)&dataHolder.classifier.h_J);
		ret = clSetKernelArg(clkern,4,sizeof(int),(void*)&dataHolder.classifier.h_J_min);
		ret = clSetKernelArg(clkern,5,sizeof(int),(void*)&dataHolder.classifier.h_J_step);
		ret = clSetKernelArg(clkern,6,sizeof(int),(void*)&dataHolder.classifier.h_J_numOfSteps);
		ret = clSetKernelArg(clkern,7,sizeof(int),(void*)&dataHolder.dataTrain.classes);

		ret = clEnqueueNDRangeKernel(dataHolder.clcmdq,clkern,1,NULL,&global_item_size,&local_item_size,0,NULL,&event);
		ret = clWaitForEvents(1,&event);
		ret=clReleaseEvent(event);

	  }
	}
}
