/*
 * ResultsHolder.cpp
 *
 *  Created on: 10 Jul 2013
 *      Author: jdt42
 */

#include "ResultsHolder.hpp"
#include "DataHolder.hpp"
#include <boost/lexical_cast.hpp>

using namespace std;

void ResultsHolder::updateResultsHolder(DataHolder& dataHolder)
{
	double mccBestForQ = -1;
	int mccBestForQstep = 0;
	int JBestForQ = 0;

	ofstream resultsFile;
	if (!dataHolder.testCase) {
		resultsFile.open((dataHolder.resultsPath + dataHolder.dataFileName + "_"
			+ dataHolder.currentMethodName + "_" + "results.txt").c_str(),
			ios::out | ios::app);
	}
	for (int step=0; step<dataHolder.classifier.h_J_numOfSteps; step++) {
		double currentMccForQ = h_mcc_results[step];
		int currentJ = dataHolder.classifier.h_J_min + step*dataHolder.classifier.h_J_step;

		if (currentMccForQ > mccBestForQ) {
			mccBestForQ = currentMccForQ;
			mccBestForQstep = step;
			JBestForQ = currentJ;
		}
		if (!dataHolder.testCase) {
			resultsFile << currentJ << ", " << dataHolder.classifier.h_q << ", " << currentMccForQ << endl;
		}
		if (dataHolder.verbose != 0) {
			cout << "Best classifier: j=" << currentJ << ", q=" << dataHolder.classifier.h_q
								<< ", " << currentMccForQ << endl << endl;
		}
	}
	if (!dataHolder.testCase) {
		resultsFile.close();
	}

	//update container for best probabilistic results
	if (mccBestForQ > mccBest) {
		mccBest = mccBestForQ;
		qBest = dataHolder.classifier.h_q;
		jBest = JBestForQ;
		cout << "mccBest " << mccBest << endl;
		cout << "qBest " << qBest << endl;
		cout << "jBest " << jBest << endl;

		for (int rootId=0; rootId<dataHolder.dataTest.N; rootId++) {
			//cout << "JT: hello: " << rootId << endl;
			for (int classId=0; classId<dataHolder.dataTrain.classes; classId++) {
				//cout << "JT: hello: " << classId << endl;
				//cout << "JT: hello: " << resultsHolder.h_class_prob_master[25] << endl;
				classProbBest[rootId*dataHolder.dataTrain.classes + classId] =
					h_class_prob_master[rootId*dataHolder.classifier.h_J_numOfSteps*dataHolder.dataTrain.classes
						+ mccBestForQstep*dataHolder.dataTrain.classes + classId];
			}
		}
	}
	/**
		// copy the updated ResultsHolder back to the ResultsHolderMap
		resultsHolderMap.erase(resultsHolder.method);
		resultsHolderMap.insert(std::map<string, ResultsHolder>::value_type(resultsHolder.method, resultsHolder));
	**/

}


void ResultsHolder::printMccBest(DataHolder& dataHolder) {

	int h_N = dataHolder.dataTest.N;
	int h_classes = dataHolder.dataTrain.classes;

	string bestMccResultsPath = dataHolder.resultsPath
			+ dataHolder.dataFileName + "_" + method + "_"
			+ boost::lexical_cast<string>(qBest) + "_"
			+ boost::lexical_cast<string>(jBest)
			+ "_best.csv";
	//cout << bestMccResultsPath << endl;
	//cout << "qBest " << qBest << endl;
	//cout << "jBest " << jBest << endl;

	ofstream bestMccResultsFile;
	bestMccResultsFile.open(bestMccResultsPath.c_str());

	bestMccResultsFile << "itemId,correctClass";
	for(int classId=0; classId<h_classes; classId++)
	{
		bestMccResultsFile << "," << dataHolder.dataTrain.h_classIdKeySetVector[classId];
	}
	bestMccResultsFile << endl;
	for (int n=0; n<h_N; n++) {
		//bestMccResultsFile << "class:" << dataHolder.dataTest.h_classIdVector[n] << ",\t";
		//bestMccResultsFile << "itemdId:" << dataHolder.dataTest.h_itemIdVector[n] << ",\t";
		//bestMccResultsFile << "results:";
		bestMccResultsFile << dataHolder.dataTest.h_itemIdVector[n] << "," << dataHolder.dataTest.h_classIdVector[n];

		for (int classId=0; classId<h_classes; classId++) {
			bestMccResultsFile << "," << classProbBest[n*h_classes + classId];
		}
		bestMccResultsFile << endl;

	}
	bestMccResultsFile.close();
}










/**
void ResultsHolder::printMccBest(DataHolder& dataHolder) {

	int h_N = dataHolder.dataTest.N;
	int h_classes = dataHolder.dataTrain.classes;

	string bestMccResultsPath = dataHolder.resultsPath
			+ dataHolder.dataFileName + "_" + method + "_"
			+ boost::lexical_cast<string>(qBest) + "_"
			+ boost::lexical_cast<string>(jBest)
			+ "_best";
	//cout << bestMccResultsPath << endl;
	//cout << "qBest " << qBest << endl;
	//cout << "jBest " << jBest << endl;

	ofstream bestMccResultsFile;
	bestMccResultsFile.open(bestMccResultsPath.c_str());

	for (int n=0; n<h_N; n++) {
		bestMccResultsFile << "class:" << dataHolder.dataTest.h_classIdVector[n] << ",\t";
		bestMccResultsFile << "itemdId:" << dataHolder.dataTest.h_itemIdVector[n] << ",\t";
		bestMccResultsFile << "results:";

		for (int classId=0; classId<h_classes; classId++) {
			bestMccResultsFile << dataHolder.dataTrain.h_classIdKeySetVector[classId] << "="
					<< classProbBest[n*h_classes + classId];

			if (classId!=h_classes - 1) {
				bestMccResultsFile << " , ";
			}
		}
		bestMccResultsFile << endl;
	}
	bestMccResultsFile.close();
}
**/
void ResultsHolder::print_h_scores(DataHolder& dataHolder) {

	int classes = dataHolder.dataTrain.classes;
	int J = dataHolder.classifier.h_J;
	int N = dataHolder.dataTest.N;

	cout << endl;
	cout << "h_scores" << endl;
	cout << "rootId,comboId: scores" << endl;

	for (int itemIdRoot=0; itemIdRoot<N; itemIdRoot++) {
		//string itemId=dataHolder.dataTest.h_itemIdVector[itemIdRoot];
		for (int comboId=0; comboId<J; comboId++) {
			//cout << itemIdRoot << "," << itemId << "," <<  comboId << ": ";
			cout << itemIdRoot << "," <<  comboId << ": ";
			for (int classId = 0; classId < classes; classId++) {
				if(dataHolder.h_method_ref==0 || dataHolder.h_method_ref==2){
					cout << h_scores_float[itemIdRoot*J*classes	+ comboId*classes + classId] << " ";
				}else{
					cout << h_scores[itemIdRoot*J*classes + comboId*classes + classId] << " ";
				}
			}
			cout << endl;
		}
	}
}

void ResultsHolder::print_h_class_votes(DataHolder& dataHolder) {
	int classes = dataHolder.dataTrain.classes;
	int N = dataHolder.dataTest.N;
	int J_numOfSteps = dataHolder.classifier.h_J_numOfSteps;
	int J_min = dataHolder.classifier.h_J_min;
	int J_step = dataHolder.classifier.h_J_step;

	cout << endl;
	cout << "h_votes" << endl;
	cout << "rootId: votes" << endl;
	for (int stepNum = 0; stepNum<J_numOfSteps; stepNum++) {
		cout << "Number of iterations = " << J_min + (stepNum * J_step)	<< endl;
		for (int itemIdRoot=0; itemIdRoot<N; itemIdRoot++) {
			cout << itemIdRoot << ": ";
			for (int classId=0; classId<classes; classId++) {
				if(dataHolder.h_method_ref==0 || dataHolder.h_method_ref==2){
					cout << h_class_votes_float[itemIdRoot*J_numOfSteps*classes + stepNum*classes + classId] << " ";
				}else{
					cout << h_class_votes[itemIdRoot*J_numOfSteps*classes + stepNum*classes + classId] << " ";
				}
			}
			cout << endl;
		}
	}
}

void ResultsHolder::print_h_class_prob(DataHolder& dataHolder) {
	int classes = dataHolder.dataTrain.classes;
	int N = dataHolder.dataTest.N;
	int J_numOfSteps = dataHolder.classifier.h_J_numOfSteps;
	int J_min = dataHolder.classifier.h_J_min;
	int J_step = dataHolder.classifier.h_J_step;

	cout << endl;
	cout << "h_prob" << endl;
	cout << "rootId: prob" << endl;
	for (int stepNum=0; stepNum<J_numOfSteps; stepNum++) {
		cout << "Number of iterations = " << J_min + (stepNum*J_step) << endl;

		for (int itemIdRoot = 0; itemIdRoot < N; itemIdRoot++) {
			cout << itemIdRoot << ": ";
			for (int classId = 0; classId<classes; classId++) {
				cout << h_class_prob[itemIdRoot*J_numOfSteps*classes + stepNum*classes + classId] << " ";
			}
			cout << endl;
		}
	}
}

void ResultsHolder::print_confusion_matrix(DataHolder& dataHolder) {
	int classes = dataHolder.dataTrain.classes;
	int J_numOfSteps = dataHolder.classifier.h_J_numOfSteps;
	int J_min = dataHolder.classifier.h_J_min;
	int J_step = dataHolder.classifier.h_J_step;

	cout << "h_confusion_matrix" << endl;

	for (int stepNum = 0; stepNum<J_numOfSteps; stepNum++) {
		cout << "Number of iterations = " << J_min + (stepNum*J_step) << endl;
		for (int trueClass = 0; trueClass<classes; trueClass++) {
			for (int predClass = 0; predClass < classes; predClass++) {
				cout << h_confusion_matrix[stepNum*classes*classes + trueClass*classes + predClass] << ",";
			}
			cout << endl;
		}
	}
}





ResultsHolder::ResultsHolder() {
	mccBest = 0.0;
	qBest = 0.0;
	jBest = 0.0;
	classProbBest = new float(0.0);
}

ResultsHolder::~ResultsHolder() {
	// TODO Auto-generated destructor stub
}

