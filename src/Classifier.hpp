/*
 * Classifier.hpp
 *
 *  Created on: 5 Dec 2013
 *      Author: jdt42
 */

#ifndef CLASSIFIER_HPP_
#define CLASSIFIER_HPP_

class Classifier {
public:

	/**
	 * Current value of q
	 */
	int h_q;
	/**
	* Host variable of minimum j value
	*/
	int h_J_min;

	/**
	* Host variable of j step
	*/
	int h_J_step;

	/**
	* Host variable of current j value
	*/
	int h_J;

	/**
    * Host variable of number of j steps
	*/
	int h_J_numOfSteps;

	/**
	 * Host variable representing the number of batches.  For large data sets the
	 * data must be split up into batches according to GPU memory limitations
	 */
	int h_numOfBatches;
	/**
	 * Host variable representing the batch size, i.e. the number of feature vectors
	 * in each batch
	 */
	int h_batchSize;
	/**
	 * Host pointer to array listing the size of all batches
	 */
	int *h_batchSizes;
	/**
	 * Host pointer to array listing the position of the feature vector in the full list of
	 * feature vectors for the first item in each batch.  This is used to locate the start
	 * of the batch on the gpu
	 */
	int *h_batchStartPositions;

	/**
	 * Current batchNum
	 */
	int h_currentBatchNum;
/**
 * current batch start position
 */
	int h_currentBatchStartPos;
	/**
	 * current batch size
	 */
	int h_currentBatchSize;

	Classifier();
	virtual ~Classifier();
};

#endif /* CLASSIFIER_HPP_ */
