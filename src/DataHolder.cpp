/*
 * DataHolder.cpp
 *
 *  Created on: 27 Jun 2013
 *      Author: jdt42
 */
#include <stdio.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <vector>
#include <set>
#include <list>
#include <map>
#include "DataStructure.hpp"
#include "Classifier.hpp"
#include "FeatureComboGenerator.hpp"
#include "CorrelationCalculator.hpp"
#include <boost/lexical_cast.hpp>
#include "DataHolder.hpp"
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <CL/cl.h>

using namespace std;
using namespace boost;

void DataHolder::readPrecomputedCombos()
{
	if(comboMethod==1)
	{
		std::vector<int> comboVector;
		std::vector<int> comboSizesVector;
		string token;

		if(verbose!=0){
			cout << "reading combos from file" << endl;
		}
		string line;
		ifstream myfile(comboPath.c_str());
		int numOfCombos=0;

		if (myfile.is_open()) {
			while (myfile.good()) {
				getline(myfile, line);
				if (strlen(line.c_str()) >= 1) {
					//cout <<line << endl;
					numOfCombos++;
					istringstream iss(line);
					int size=0;
					while (getline(iss, token, ',')) {
						//cout << token << endl;
						comboVector.push_back(boost::lexical_cast<int>(token));
						size++;
					}
					comboSizesVector.push_back(size);
					//cout << endl;
				}
			}
		}
		myfile.close();

		j_max=numOfCombos;
		classifier.h_J=numOfCombos;

		//if combos are all the same size then set q_min and q_max, otherwise set them to zero
		int firstComboSize=comboSizesVector[0];
		bool allCombosSameSize=true;
		for(uint i=1; i<comboSizesVector.size(); i++){
			if(firstComboSize!=comboSizesVector[i]){
				allCombosSameSize=false;
				q_min=0;
				q_max=0;
				q_step=1;
				break;
			}
		}
		if(allCombosSameSize)
		{
			q_min=firstComboSize;
			q_max=firstComboSize;
			q_step=1;
		}

		COMBO_ARRAY_SIZE = comboVector.size();
		COMBO_ARRAY_BYTES = COMBO_ARRAY_SIZE * sizeof(int);
		COMBO_BREAKPOINTS_SIZE = numOfCombos + 1;
		COMBO_BREAKPOINTS_BYTES = COMBO_BREAKPOINTS_SIZE * sizeof(int);
		h_in_combo = (int*) malloc(COMBO_ARRAY_BYTES);
		h_comboBreakPoints = (int*) malloc(COMBO_BREAKPOINTS_BYTES);

		h_comboBreakPoints[0]=0;
		for(uint j=0; j<comboSizesVector.size(); j++){
			h_comboBreakPoints[j+1]=h_comboBreakPoints[j]+comboSizesVector[j];
			//cout << h_comboBreakPoints[j+1] << endl;
		}

		for(uint i=0; i<comboVector.size(); i++){
			h_in_combo[i]=comboVector[i];
			//cout << h_in_combo[i]<<",";
		}
	}
}


void DataHolder::write_h_feature_count_array(DataStructure& data){
	string path = resultsPath + dataFileName + "_featureCountModel.dat";
	int L = data.L;
	ofstream featureCountFile;
	featureCountFile.open(path.c_str());

	for (int classRef = 0; classRef<data.classes; classRef++) {
		string classId = data.h_classIdKeySetVector[classRef];
		featureCountFile << classId;
		for (int j=0; j<L; j++) {
			featureCountFile << "," << data.h_feature_count_array[classRef*L + j];
		}
		featureCountFile << endl;
	}
	featureCountFile.close();
}

void DataHolder::calculateDataFileName() {
	int lastSeparatorPos = 0;
	int lastPeriodPos = 0;
	for (uint i = 0; i < dataPath.size(); i++) {
		if (dataPath.at(i) == '\\' || dataPath.at(i) == '/') {
			lastSeparatorPos = i;
		}
	}
	dataFileName = dataPath.substr(lastSeparatorPos + 1,
			dataPath.size() - lastSeparatorPos - 1);

	for (uint i = 0; i < dataFileName.size(); i++) {
		if (dataFileName.at(i) == '.') {
			lastPeriodPos = i;
		}
	}

	if (lastPeriodPos > 0) {
		dataFileName = dataFileName.substr(0, lastPeriodPos);
	}

}

void DataHolder::readRunSpecification() {
	string line;
	string field;
	string setting;

	ifstream myfile(runSpecPath.c_str());
	if (myfile.is_open()) {
		while (myfile.good()) {
			getline(myfile, line);
			trim(line);
			size_t pos = 0;
			size_t length = line.length();

			//cout << "line: " << line;
			//cout <<"\nlength: " << length << "\n";

			if (length > 1 && line.substr(0, 2).compare("//") != 0) {
				pos = line.find("=");
				field = line.substr(0, pos);
				setting = line.substr(pos + 1, length - 1);

				//remove carriage return from end of line
				if ((setting[setting.size() - 1] == '\r')
						|| (setting[setting.size() - 1] == '\n')) {
					setting.resize(setting.size() - 1);
				}

				//cout << "field: " << field << endl;
				//cout << "setting: " << setting << endl;

				//generate a bool value if the setting string is true or false
				bool settingBool;
				if (setting.compare("true") == 0
						|| setting.compare("TRUE") == 0) {
					settingBool = true;
				} else if (setting.compare("false") == 0
						|| setting.compare("FALSE") == 0) {
					settingBool = false;
				}
				if (field.compare("verbose") == 0) {
					verbose = boost::lexical_cast<int>(setting);
				}else if (field.compare("dataFormat") == 0) {
					dataFormat = boost::lexical_cast<int>(setting);
					dataTrain.dataFormat=dataFormat;
					dataTest.dataFormat=dataFormat;
				}else if (field.compare("device") == 0) {
					device = boost::lexical_cast<int>(setting);
				}else if (field.compare("implementation") == 0) {
					implementation = boost::lexical_cast<int>(setting);
				}else if (field.compare("dataPath") == 0) {
					dataPath = setting;
				}else if (field.compare("testPath") == 0) {
					testPath = setting;
				}else if (field.compare("comboPath") == 0) {
					comboPath = setting;
				}else if (field.compare("resultsPath") == 0) {
					resultsPath = formatResultsPath(setting);

				}else if (field.compare("testClassesKnown") == 0) {
					testClassesKnown = settingBool;
				}else if (field.compare("runRascal") == 0) {
					runRascal = settingBool;
				}else if (field.compare("runNb") == 0) {
					runNb = settingBool;
				}else if (field.compare("runPrw") == 0) {
					runPrw = settingBool;
				}else if (field.compare("prwParam") == 0) {
					prwParam = boost::lexical_cast<double>(setting);
				}else if (field.compare("comboMethod") == 0) {
					comboMethod = boost::lexical_cast<int>(setting);
				}else if (field.compare("classifierMethod") == 0) {
					classifierMethod = boost::lexical_cast<int>(setting);
				}else if (field.compare("samplingMethod") == 0) {
					samplingMethod = boost::lexical_cast<int>(setting);
				}else if (field.compare("kfoldNum") == 0) {
					kfoldNum = boost::lexical_cast<int>(setting);
				}else if (field.compare("usePredefinedKfoldSplit") == 0) {
					usePredefinedKfoldMember = settingBool;
				}else if (field.compare("kfoldSplitPath") == 0) {
					dataTrain.kfoldSplitPath = setting;
				}else if (field.compare("q_min") == 0) {
					q_min = boost::lexical_cast<int>(setting);
				}else if (field.compare("q_max") == 0) {
					q_max = boost::lexical_cast<int>(setting);
				}else if (field.compare("q_step") == 0) {
					q_step = boost::lexical_cast<int>(setting);
				}else if (field.compare("j_min") == 0) {
					j_min = boost::lexical_cast<int>(setting);
				}else if (field.compare("j_max") == 0) {
					j_max = boost::lexical_cast<int>(setting);
				}else if (field.compare("j_step") == 0) {
					j_step = boost::lexical_cast<int>(setting);
				}else if (field.compare("testCase") == 0) {
					testCase = settingBool;
				}else{
					cout << "field unmatched" << endl;
				}
			}
		}
		//cout << "dataPath: " << dataPath << endl;
		//cout << "resultsPath: " << resultsPath << endl;
		//cout << "runPCRASD: " << runPCRASD << endl;
		//cout << "pzwParam: " << pzwParam << endl;
		//cout << "subStringIterStep: " << subStringIterStep << endl;
		myfile.close();
	}

	else
		cout << "Unable to open file" << endl;
}

string DataHolder::formatResultsPath(string path)
{
	string formattedPath;
	char lastChar = path.at(path.length()-1);
	if((path.find('/')!= std::string::npos) && lastChar != '/'){
		formattedPath = path + '/';
	}
	else if((path.find('\\')!= std::string::npos) && lastChar != '\\'){
			formattedPath = path + '\\';
	}
	else{
		formattedPath = path;
	}
	return formattedPath;
}

void DataHolder::checkValueOfQstep(){
	//set q step to 1 if entered by the user as 0 so that the for loop in ProbClassifierManager will terminate
	if (q_step == 0) {
		q_step = 1;
	}
}

cl_device_id DataHolder::findMaxDeviceOpenCl() {

	cl_device_id max_device_id = NULL;
	cl_long max_device_memory = 0;
	unsigned int i, j;	//iterator variables for loops
	cl_platform_id platforms[32];	//an array to hold the IDs of all the platforms, hopefuly there won't be more than 32
	cl_uint num_platforms;	//this number will hold the number of platforms on this machine
	char vendor[1024];	//this string will hold a platforms vendor
	char version[1024];	//this string will hold a platforms version
	cl_device_id devices[32];	//this variable holds the number of devices for each platform, hopefully it won't be more than 32 per platform
	cl_uint num_devices;	//this number will hold the number of devices on this machine
	char deviceName[1024];	//this string will hold the devices name
	cl_uint numberOfCores;	//this variable holds the number of cores of on a device
	cl_long amountOfMemory;	//this variable holds the amount of memory on a device
	cl_uint clockFreq;	//this variable holds the clock frequency of a device
	cl_ulong maxAlocatableMem;	//this variable holds the maximum allocatable memory
	cl_ulong localMem;	//this variable holds local memory for a device
	cl_bool available;	//this variable holds if the device is available
	//get the number of platforms

	clGetPlatformIDs (32, platforms, &num_platforms);
	//printf("\nNumber of platforms:\t%u\n\n", num_platforms);

	for(i = 0; i < num_platforms; i++)
	{
		//printf("Platform:\t\t%u\n\n", i);
		clGetPlatformInfo (platforms[i], CL_PLATFORM_VENDOR, sizeof(vendor), vendor, NULL);
		//printf("\tPlatform Vendor:\t%s\n", vendor);
		clGetPlatformInfo (platforms[i], CL_PLATFORM_VERSION, sizeof(version), version, NULL);
		//printf("\tPlatform Version:\t%s\n", version);
		clGetDeviceIDs (platforms[i], CL_DEVICE_TYPE_ALL, sizeof(devices), devices, &num_devices);
		//printf("\tNumber of devices:\t%u\n\n", num_devices);
		for(j = 0; j < num_devices; j++)
		{
			//scan in device information
			clGetDeviceInfo(devices[j], CL_DEVICE_NAME, sizeof(deviceName), deviceName, NULL);
			clGetDeviceInfo(devices[j], CL_DEVICE_VENDOR, sizeof(vendor), vendor, NULL);
			clGetDeviceInfo(devices[j], CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(numberOfCores), &numberOfCores, NULL);
			clGetDeviceInfo(devices[j], CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(amountOfMemory), &amountOfMemory, NULL);
			clGetDeviceInfo(devices[j], CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(clockFreq), &clockFreq, NULL);
			clGetDeviceInfo(devices[j], CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof(maxAlocatableMem), &maxAlocatableMem, NULL);
			clGetDeviceInfo(devices[j], CL_DEVICE_LOCAL_MEM_SIZE, sizeof(localMem), &localMem, NULL);
			clGetDeviceInfo(devices[j], CL_DEVICE_AVAILABLE, sizeof(available), &available, NULL);

			if(amountOfMemory > max_device_memory)
			{
				max_device_memory = amountOfMemory;
				max_device_id = devices[j];
				device=j;
			}
		}
	}

	return max_device_id;
}

int DataHolder::findMaxDeviceCuda() {
	int num_devices, max_device;
	cudaGetDeviceCount(&num_devices);
	if (num_devices >= 1) {
		double maxComputeLevel = 0.0;
		for (int device = 0; device < num_devices; device++) {
			cudaDeviceProp properties;
			cudaGetDeviceProperties(&properties, device);
			double computeLevel = properties.major + properties.minor;

			if (verbose != 0) {
				cout << "Device Num: " << device << ", Name: "
						<< properties.name << ", Compute Level: "
						<< computeLevel << endl;
			}

			if (maxComputeLevel <= computeLevel && strcmp(properties.name, "Tesla M2090") != 0)
			{
				maxComputeLevel = computeLevel;
				max_device = device;
			}
		}
	}
	return max_device;
}

void DataHolder::setDeviceDataOpenCl(){

	if(device==-1) {
		max_device_id = findMaxDeviceOpenCl();
	}

	if (verbose!=0) {
		cout << "Device selected: " << device << endl;
	}
	cl_int ret;
	clctx = clCreateContext(NULL, 1, &max_device_id, NULL, NULL, &ret);
	clcmdq = clCreateCommandQueue(clctx, max_device_id, 0, &ret);
	//cudaSetDevice(device);
	//cudaDeviceProp properties;
	//cudaGetDeviceProperties(&properties, device);
	size_t max_work_items;
	size_t max_work_item_sizes[3];
	cl_uint device_address_bits;
	cl_uint max_work_item_dimensions;
	char deviceExtensions[4096];
	clGetDeviceInfo(max_device_id, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(gpuGlobalMemory), &gpuGlobalMemory, NULL);
	clGetDeviceInfo(max_device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(max_work_items), &max_work_items, NULL);
	clGetDeviceInfo(max_device_id, CL_DEVICE_ADDRESS_BITS, sizeof(device_address_bits), &device_address_bits, NULL);
	clGetDeviceInfo(max_device_id, CL_DEVICE_EXTENSIONS, sizeof(deviceExtensions), &deviceExtensions, NULL);
	//clGetDeviceInfo(max_device_id, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(max_work_item_dimensions), &max_work_item_dimensions, NULL);
	//clGetDeviceInfo(max_device_id, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(max_work_item_sizes), &max_work_item_sizes, NULL);
	//gpuGlobalMemory = properties.totalGlobalMem;
	//maxBlocks = properties.maxGridSize[1];
	//maxThreads = properties.maxThreadsDim[1];
	cout << deviceExtensions << endl;
	maxBlocks = (int) max_work_items;
	maxThreads = pow(2,device_address_bits/2)-1;
	if (verbose != 0) {
		cout << "maxBlocks: " << maxBlocks << endl;
		cout << "maxThreads: " << maxThreads << endl;
		cout << "global Memory: " << gpuGlobalMemory << endl;
		//cout << "maxWorkItemDimensionss: " << max_work_item_dimensions << endl;
	}
}

void DataHolder::setDeviceDataCuda(){
	if(device==-1) {
		device = findMaxDeviceCuda();
	}

	if (verbose!=0) {
		cout << "Device selected: " << device << endl;
	}
	cudaSetDevice(device);
	cudaDeviceProp properties;
	cudaGetDeviceProperties(&properties, device);

	gpuGlobalMemory = properties.totalGlobalMem;
	maxBlocks = properties.maxGridSize[1];
	maxThreads = properties.maxThreadsDim[1];
	if (verbose != 0) {
		cout << "maxBlocks: " << maxBlocks << endl;
		cout << "maxThreads: " << maxThreads << endl;
		cout << "global Memory: " << gpuGlobalMemory << endl;
	}
}

void DataHolder::setupDataTrain() {
	dataTrain.trainOrTest=0;
	dataTrain.readData(dataPath);
	dataTrain.createkfoldMember(samplingMethod, kfoldNum, usePredefinedKfoldMember);

	string kfoldMemberPath = resultsPath + dataFileName	+ "_kfoldMember.dat";
	dataTrain.printKfoldMember(kfoldMemberPath);

	h_inMaxTrainAndTest = (int*) malloc(dataTrain.MIN_MAX_ARRAY_BYTES);
	h_inMinTrainAndTest = (int*) malloc(dataTrain.MIN_MAX_ARRAY_BYTES);
	if(dataFormat==0) {
		for(int i=0; i<dataTrain.L; i++) {
			h_inMaxTrainAndTest[i] = dataTrain.h_inMax[i];
			h_inMinTrainAndTest[i] = dataTrain.h_inMin[i];
		}
	}
}

void DataHolder::setupDataTest() {
	dataTest.trainOrTest=1;

	if(samplingMethod==2){
		dataTest.trainOrTest=1;
		dataTest.classesKnown=testClassesKnown;
		dataTest.readData(testPath);
		dataTest.classes = dataTrain.classes;
		dataTest.createkfoldMember(samplingMethod, kfoldNum, usePredefinedKfoldMember);
		if(dataFormat==0){
			for(int i=0; i<dataTrain.L; i++){
				h_inMaxTrainAndTest[i] = max(h_inMaxTrainAndTest[i], dataTest.h_inMax[i]);
				h_inMinTrainAndTest[i] = min(h_inMinTrainAndTest[i], dataTest.h_inMin[i]);
			}
		}
	}else{
		dataTest.N = dataTrain.N;
		dataTest.L = dataTrain.L;
		dataTest.classes = dataTrain.classes;

		dataTest.h_in = dataTrain.h_in;
		dataTest.h_inSparseVectorBreakPoints = dataTrain.h_inSparseVectorBreakPoints;
		dataTest.h_kfoldMember = dataTrain.h_kfoldMember;
		dataTest.h_classMember = dataTrain.h_classMember;

		dataTest.h_classIdVector = dataTrain.h_classIdVector;
		dataTest.h_itemIdVector = dataTrain.h_itemIdVector;
		dataTrain.h_classIdVector.clear();
		dataTrain.h_itemIdVector.clear();

		dataTest.IN_ARRAY_NORMALISED_BYTES = dataTrain.IN_ARRAY_NORMALISED_BYTES;
		dataTest.MIN_MAX_ARRAY_BYTES = dataTrain.MIN_MAX_ARRAY_BYTES;
		dataTest.IN_ARRAY_SIZE = dataTrain.IN_ARRAY_SIZE;
		dataTest.IN_ARRAY_BYTES = dataTrain.IN_ARRAY_BYTES;
		dataTest.CLASS_PROB_ARRAY_BEST_SIZE = dataTrain.CLASS_PROB_ARRAY_BEST_SIZE;
		dataTest.CLASS_PROB_ARRAY_BEST_BYTES = dataTrain.CLASS_PROB_ARRAY_BEST_BYTES;
		dataTest.CLASS_ARRAY_BYTES = dataTrain.CLASS_ARRAY_BYTES;
		dataTest.SPARSE_ARRAY_BYTES = dataTrain.SPARSE_ARRAY_BYTES;
		dataTest.NUM_COMPARES_ARRAY_SIZE = dataTrain.NUM_COMPARES_ARRAY_SIZE;
		dataTest.NUM_COMPARES_ARRAY_BYTES = dataTrain.NUM_COMPARES_ARRAY_BYTES;
		dataTest.FEATURE_COUNT_ARRAY_BY_CLASS_SIZE = dataTrain.FEATURE_COUNT_ARRAY_BY_CLASS_SIZE;
		dataTest.FEATURE_COUNT_ARRAY_BY_CLASS_BYTES = dataTrain.FEATURE_COUNT_ARRAY_BY_CLASS_BYTES;
	}
}

void DataHolder::generateNormlisedInArraysForPrw(){
	if(runPrw && dataFormat==0)	{
		//declare and populate h_inNormalised
		dataTrain.h_inNormalised = (double*) malloc(dataTrain.IN_ARRAY_NORMALISED_BYTES);
		for (int i=0; i<dataTrain.IN_ARRAY_SIZE; i++) {
			int pos = i % dataTrain.L;
			if (h_inMaxTrainAndTest[pos] == 0 && h_inMinTrainAndTest[pos] == 0) {
				dataTrain.h_inNormalised[i] = 0;
			} else {
			dataTrain.h_inNormalised[i] = ((double) (dataTrain.h_in[i] - h_inMinTrainAndTest[pos]))
				/ ((double) (h_inMaxTrainAndTest[pos] - h_inMinTrainAndTest[pos]));
			}
		}

		if(samplingMethod==2){
			dataTest.h_inNormalised = (double*) malloc(dataTest.IN_ARRAY_NORMALISED_BYTES);
			for (int i=0; i<dataTest.IN_ARRAY_SIZE; i++) {
				int pos = i % dataTrain.L;
				if (h_inMaxTrainAndTest[pos] == 0 && h_inMinTrainAndTest[pos] == 0) {
					dataTest.h_inNormalised[i] = 0;
				} else {
					dataTest.h_inNormalised[i] = ((double) (dataTest.h_in[i] - h_inMinTrainAndTest[pos]))
							/ ((double) (h_inMaxTrainAndTest[pos] - h_inMinTrainAndTest[pos]));
				}
			}
		}else{
			dataTest.h_inNormalised = dataTrain.h_inNormalised;
		}
	}
}

void DataHolder::printDataInformationToStdOut(){
	if (verbose != 0) {
		cout << "implementation: " << implementation << endl;
		cout << "Number of feature vectors: " << dataTrain.N << endl;
		cout << "Number of features: " << dataTrain.L << endl;
		cout << "Number of classes: " << dataTrain.classes << endl;
		for (std::map<string, std::vector<int> >::iterator it =	dataTrain.h_classIdMap.begin();
					it != dataTrain.h_classIdMap.end(); it++) {
			string classId = (*it).first;
			std::vector<int> positionsForClass = (*it).second;
			cout << "Number of vectors in class " << classId << ": " << positionsForClass.size() << endl;
		}
	}
}

void DataHolder::generateRandomNumbersArray(){
	RANDOMS_SIZE = 10000;
	RANDOMS_BYTES = RANDOMS_SIZE * sizeof(int);

	if (verbose != 0) {
		cout << "Generating random numbers ...." << endl;
	}

	srand(time(NULL));
	h_randoms = (int*) malloc(RANDOMS_BYTES);
	for (int i=0; i<RANDOMS_SIZE; i++) {
		int r = rand() % 1000;
		h_randoms[i] = r;
	}
}

void DataHolder::generatePrecomputedPrwArray(){

	PRW_LOOKUPTABLE_SIZE = 256;
	PRW_LOOKUPTABLE_BYTES = PRW_LOOKUPTABLE_SIZE * sizeof(double);

	if(runPrw){
		h_prw_lookupTable = (double*) malloc(PRW_LOOKUPTABLE_BYTES);
		for(int i=0; i<PRW_LOOKUPTABLE_SIZE; i++){
			h_prw_lookupTable[i]=exp(-1.0 * (i / (2.0 * prwParam * prwParam)));
			//cout << i << ": " << h_prw_lookupTable[i] << ",";
		}
	}
}

void runOpenClTestCode() {

	cl_platform_id platform_id = NULL;
	cl_device_id device_id = NULL;
	cl_context clctx = NULL;
	cl_command_queue clcmdq = NULL;

	cl_uint ret_num_devices;
	cl_uint ret_num_platforms;
	cl_int ret;

	ret = CL_SUCCESS;
	ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);
	ret = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_DEFAULT, 1, &device_id, &ret_num_devices);

	clctx = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);
	    //cl_context clctx=clCreateContextFromType(0, CL_DEVICE_TYPE_ALL, NULL, NULL, &clerr);
	    //size_t parmsz;
	    //clerr=clGetContextInfo(clctx, CL_CONTEXT_DEVICES, 0, NULL, &parmsz);
	    //cl_device_id* cldevs=(cl_device_id *) malloc(parmsz);
	    //clerr = clGetContextInfo(clctx, CL_CONTEXT_DEVICES, parmsz, cldevs, NULL);


	clcmdq = clCreateCommandQueue(clctx, device_id, 0, &ret);
	#define STRINGIFY(A) #A
	const char* kernel_source = STRINGIFY(

	float function_example(float a, float b)
	{
	    return a + b;
	}

	__kernel void test(__global float* a, __global float* b, __global float* c)
	{
	        unsigned int i = get_global_id(0);
	        //c[i]=7;
	        //c[i] = a[i] + b[i];
	        c[i] = function_example(a[i], b[i]);
	    }
	    );

	    cl_program clpgm;
	    clpgm = clCreateProgramWithSource(clctx,1, &kernel_source, NULL, &ret);

	    char clcompileflags[4096];
	    ret = clBuildProgram(clpgm, 0, NULL, clcompileflags, NULL, NULL);
	    cl_kernel clkern = clCreateKernel(clpgm, "test", &ret);

	    float *a_host;
	    float *b_host;
	    float *c_host;

	    a_host = (float *)malloc(3*sizeof(float));
	    b_host = (float *)malloc(3*sizeof(float));
	    c_host = (float *)malloc(3*sizeof(float));
	    for(int i=0; i<3; i++)
	    {
	      a_host[i]=1.0;
	      b_host[i]=4.0;
	      c_host[i]=0.0;
	    }
	    cl_mem a_device = clCreateBuffer(clctx, CL_MEM_READ_WRITE, 3*sizeof(float), NULL, NULL);
	    cl_mem b_device = clCreateBuffer(clctx, CL_MEM_READ_WRITE, 3*sizeof(float), NULL, NULL);
	    cl_mem c_device = clCreateBuffer(clctx, CL_MEM_READ_WRITE, 3*sizeof(float), NULL, NULL);


	    ret = clEnqueueWriteBuffer(clcmdq, a_device, CL_TRUE, 0, 3*sizeof(float), a_host, 0, NULL, NULL);
	    ret = clEnqueueWriteBuffer(clcmdq, b_device, CL_TRUE, 0, 3*sizeof(float), b_host, 0, NULL, NULL);
	    ret = clEnqueueWriteBuffer(clcmdq, c_device, CL_TRUE, 0, 3*sizeof(float), c_host, 0, NULL, NULL);

	    ret = clSetKernelArg(clkern,0,sizeof(cl_mem),(void*)&a_device);
	    ret = clSetKernelArg(clkern,1,sizeof(cl_mem),(void*)&b_device);
	    ret = clSetKernelArg(clkern,2,sizeof(cl_mem),(void*)&c_device);
	    cout << ret << endl;
	    cl_event event;
	    size_t global_item_size = 3;
	    size_t local_item_size = 1;
	    ret = clEnqueueNDRangeKernel(clcmdq,clkern,1,NULL,&global_item_size,&local_item_size,0,NULL,&event);
	    ret = clWaitForEvents(1,&event);
	    ret=clReleaseEvent(event);

	    clEnqueueReadBuffer(clcmdq,c_device,CL_TRUE,0,3*sizeof(float), c_host,0,NULL,NULL);
	    clReleaseMemObject(a_device);
	    clReleaseMemObject(b_device);
	    clReleaseMemObject(c_device);
	    for(int i=0; i<3; i++)
	    {
	    	cout << c_host[i] << endl;
	    }

}

void DataHolder::importData() {

	calculateDataFileName();
	if(implementation==1)
	{
		setDeviceDataCuda();
	}
	else if(implementation==2)
	{
		setDeviceDataOpenCl();
	}

	setupDataTrain();
	setupDataTest();
	generateNormlisedInArraysForPrw();
	checkValueOfQstep();
	printDataInformationToStdOut();
	generateRandomNumbersArray();
	generatePrecomputedPrwArray();
	readPrecomputedCombos();
}




void DataHolder::print_h_in_combo() {
	cout << endl;
	cout << "h_in_combo" << endl;
	for (int j = 0; j < classifier.h_J; j++) {
		cout << "combo " << j << ": ";
		for(int i=h_comboBreakPoints[j]; i<h_comboBreakPoints[j+1]; i++) {
			cout << h_in_combo[i] << " ";
		}
		cout << endl;
	}
}

void DataHolder::copyTrainDevicePointersToTestDevicePointers(DataStructure& source, DataStructure& target)
{
	if(implementation==1){
		target.d_in = (int*) source.d_in;
		target.d_inNormalised = (double*) source.d_inNormalised;
		target.d_classMember = (int*) source.d_classMember;
		target.d_kfoldMember = (int*) source.d_kfoldMember;
		target.d_inSparseVectorBreakPoints = (int*) source.d_inSparseVectorBreakPoints;
	}
	else if(implementation==2){
		target.d_in_ocl = (cl_mem) source.d_in_ocl;
		target.d_inNormalised_ocl = (cl_mem) source.d_inNormalised_ocl;
		target.d_classMember_ocl = (cl_mem) source.d_classMember_ocl;
		target.d_kfoldMember_ocl = (cl_mem) source.d_kfoldMember_ocl;
		target.d_inSparseVectorBreakPoints_ocl = (cl_mem) source.d_inSparseVectorBreakPoints_ocl;
	}
}


DataHolder::DataHolder(string path) {

	runSpecPath=path;
	testClassesKnown=true;
	/**
	 * set to -1 to show that it is not yet set.  Is either selected by user or the max device
	 * is found
	 */
	device=-1;

	//default is fully enumerated data
	dataFormat=0;
	//default is to generate combos
	comboMethod=0;
	//default to print all
	verbose = 1;
	//default to parallel;
	implementation = 1;

	classifierMethod = 0;
	samplingMethod = 0;

	maxThreads = 0;

	q_step = 0;
	q_max = 0;
	q_min = 0;

	RANDOMS_SIZE = 0;
	RANDOMS_BYTES = 0;

	prwParam = 0.0;

	kfoldNum = 0;
	runRascal = false;
	runNb = false;
	runPrw = false;

	usePredefinedKfoldMember = false;

	maxBlocks = 0;

}

DataHolder::~DataHolder() {

}

