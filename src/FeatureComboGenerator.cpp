/*
 * FeatureComboGenerator.cpp
 *
 *  Created on: 28 Jun 2013
 *      Author: jdt42
 */
#include <stdio.h>
#include <stdlib.h>
#include "FeatureComboGenerator.hpp"
#include <math.h>
#include <iostream>
#include <string>
#include <list>
#include <vector>
#include <set>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;


void FeatureComboGenerator::generateCombosFromFeatureCounts()
{
	std::vector<int> comboVector;
	std::vector<int> comboSizesVector;
	srand(time(NULL));
	int L = dataHolder.dataTrain.L;
	int combosGenerated = 0;
	int combosRequired = dataHolder.classifier.h_J;

	//for(int i=0; i<L; i++){
	//	comboVector.push_back(i);
	//	comboSizesVector.push_back(1);
	//}


	while(combosGenerated < combosRequired)
	{
		std::set<int> combo;

		for (std::map<string, std::vector<int> >::iterator it =	dataHolder.dataTrain.h_classIdMap.begin();
							it != dataHolder.dataTrain.h_classIdMap.end(); it++) {
			string classId = (*it).first;
			//cout << classId << endl;

			std::vector<int> positionsForClass = (*it).second;
			float classSize = positionsForClass.size();
			//cout << classSize << endl;

			std::map<string, int>::iterator mapIter = dataHolder.dataTrain.h_classIdToRefMap.find(classId);
			int classRef = (*mapIter).second;

			for(int featureNum=0; featureNum<L; featureNum++)
			{
				float featureCount = dataHolder.dataTrain.h_feature_count_array[classRef*L + featureNum];

				float featureFrequency = featureCount / classSize;

				float r = (rand() % 1000)/1000.0;
				//cout << featureNum << ", " << featureCount << ", " << featureFrequency << ", " << r << endl;
				if(featureFrequency > r){
					combo.insert(featureNum);
				}
			}
			if(combo.size()>0){
				combosGenerated++;
				//cout << "combosGenerated: " << combosGenerated << ", " << combo.size() << endl;
				comboSizesVector.push_back(combo.size());

				for(set<int>::iterator it=combo.begin(); it!=combo.end(); it++)
				{
					comboVector.push_back(*it);
				}

			}
			combo.clear();
			if(combosGenerated>=combosRequired){
				break;
			}
		}
	}

	dataHolder.COMBO_ARRAY_SIZE = comboVector.size();
	dataHolder.COMBO_ARRAY_BYTES = dataHolder.COMBO_ARRAY_SIZE * sizeof(int);
	dataHolder.COMBO_BREAKPOINTS_SIZE = dataHolder.classifier.h_J + 1;
	dataHolder.COMBO_BREAKPOINTS_BYTES = dataHolder.COMBO_BREAKPOINTS_SIZE * sizeof(int);
	dataHolder.h_in_combo = (int*) malloc(dataHolder.COMBO_ARRAY_BYTES);
	dataHolder.h_comboBreakPoints = (int*) malloc(dataHolder.COMBO_BREAKPOINTS_BYTES);

	dataHolder.h_comboBreakPoints[0]=0;

	for(uint i=0; i<comboVector.size(); i++)
	{
		dataHolder.h_in_combo[i] = comboVector[i];
	}

	for(uint i=0; i<comboSizesVector.size(); i++)
	{
		dataHolder.h_comboBreakPoints[i+1] = dataHolder.h_comboBreakPoints[i]+comboSizesVector[i];
	}

/**
	for(int j=0; j<combosRequired; j++)
	{
		for(int k=dataHolder.h_comboBreakPoints[j]; k<dataHolder.h_comboBreakPoints[j+1]; k++)
		{
			cout << dataHolder.h_in_combo[k] << ",";
		}
		cout << endl;
	}
**/
}


void FeatureComboGenerator::generateCombos() {

	dataHolder.COMBO_ARRAY_SIZE = dataHolder.classifier.h_J * dataHolder.classifier.h_q;
	dataHolder.COMBO_ARRAY_BYTES = dataHolder.COMBO_ARRAY_SIZE * sizeof(int);
	dataHolder.COMBO_BREAKPOINTS_SIZE = dataHolder.classifier.h_J + 1;
	dataHolder.COMBO_BREAKPOINTS_BYTES = dataHolder.COMBO_BREAKPOINTS_SIZE * sizeof(int);
	dataHolder.h_in_combo = (int*) malloc(dataHolder.COMBO_ARRAY_BYTES);
	dataHolder.h_comboBreakPoints = (int*) malloc(dataHolder.COMBO_BREAKPOINTS_BYTES);


	int q = dataHolder.classifier.h_q;
	int combosGenerated = 0;
	uint combosRequired = dataHolder.classifier.h_J;
	//cout << "q: " << q << "\n";
	//cout << "combosRequired: " << combosRequired << "\n";
	//cout << "combosGenerated: " << combosGenerated << "\n";

	srand(time(NULL));

	std::vector<int> featuresOld;
	std::vector<int> featuresNew;
	std::set<int> combo;
	std::set< set<int> > allCombos;

	while(allCombos.size() < combosRequired)
	{
		//cout << "\n" << "combosGenerated: " << combosGenerated << "\n";;
		for(int i=0; i<dataHolder.dataTrain.L; i++)
		{
			featuresOld.push_back(i);
		}

		for(int i=0; i<q; i++)
		{
			int r = rand() % featuresOld.size();
			//cout << r << "\n";
			int feature = featuresOld.at(r);

			//cout << feature << "\n";
			for (vector<int>::iterator it=featuresOld.begin(); it!=featuresOld.end(); it++)
			{
				if(*it!=feature)
				{
					featuresNew.push_back(*it);
				}
			}
			combo.insert(feature);

			featuresOld = featuresNew;
			featuresNew.clear();

		}

		combosGenerated = combosGenerated + 1;
		allCombos.insert(combo);

		//cout<<"\ncombo\n";
		for(set<int>::iterator it=combo.begin(); it!=combo.end(); it++)
		{
			//cout << *it << ", ";
		}

		//cout<<"\nfeatures\n";
		for(uint i=0; i<featuresOld.size(); i++)
		{
			//cout << i << ":" << featuresOld.at(i) << ", ";
		}
		featuresOld.clear();
		combo.clear();
	}

	int comboNum = 0;
	dataHolder.h_comboBreakPoints[0]=0;
	//cout<<"\nallCombos\n";
	for (set< set<int> >::iterator it1=allCombos.begin(); it1!=allCombos.end(); it1++)
	{
			std::set<int> thisCombo;
			thisCombo = *it1;
			int featureNum = 0;

			for(set<int>::iterator it2=thisCombo.begin(); it2!=thisCombo.end(); it2++)
			{
				//cout << *it2 << ", ";
				dataHolder.h_in_combo[comboNum*q + featureNum ] = *it2;
				featureNum++;
			}
			dataHolder.h_comboBreakPoints[comboNum+1]=dataHolder.h_comboBreakPoints[comboNum]+featureNum;
			//cout << endl;
			comboNum++;
			thisCombo.clear();
	}
	allCombos.clear();
}

int FeatureComboGenerator::nCr(int n, int r) {
   if(r>n) {printf("FATAL ERROR"); return 0;}
   if(n==0 || r==0 || n==r)
   {
      return 1;
   }
   else
   {
      return (int)lround( ((float)n/(float)(n-r)/(float)r) * exp(lgamma(n) - lgamma(n-r) - lgamma(r)));
   }
}

void FeatureComboGenerator::printCombos() {
	int J=dataHolder.classifier.h_J;

	ofstream featureComboFile;
	string featureComboPath = dataHolder.resultsPath + dataHolder.dataFileName	+ "_combos.dat";
	featureComboFile.open(featureComboPath.c_str());

	for(int j=0; j<J; j++)
	{
		bool printComma=false;
		for(int k=dataHolder.h_comboBreakPoints[j]; k<dataHolder.h_comboBreakPoints[j+1]; k++)
		{
			if(printComma){
				featureComboFile << ",";
			}else{
				printComma=true;
			}
			featureComboFile << dataHolder.h_in_combo[k];
		}
		featureComboFile << endl;
	}
	featureComboFile.close();
}

FeatureComboGenerator::FeatureComboGenerator(DataHolder& data) : dataHolder(data)  {
	// TODO Auto-generated constructor stub

}

FeatureComboGenerator::~FeatureComboGenerator() {
	// TODO Auto-generated destructor stub
}

