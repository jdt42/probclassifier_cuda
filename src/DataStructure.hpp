/*
 * DataStructure.hpp
 *
 *  Created on: 29 Nov 2013
 *      Author: jdt42
 */

#include <stdio.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <vector>
#include <set>
#include <list>
#include <map>
#include <CL/cl.h>

#ifndef DATASTRUCTURE_HPP_
#define DATASTRUCTURE_HPP_

using namespace std;

class DataStructure {
public:

	std::vector<int> inSparseVectorBreakPoints;
	std::vector<int> inVector;

	/**
	 * Explicit file path to data file
	 */
	string dataPath;
	/**
	 * 0=train, 1=test
	 */
	int trainOrTest;

	/**
	 * determines whether the classes are known
	 */
	bool classesKnown;
	/**
	 * Number of data items
	 */
	int N;
	/**
	 * Length of each feature vector
	 */
	int L;
	/**
	 * Number of classes
	 */
	int classes;

	/**
	 * Specifies whether the input vectors are fully enumerated dataFormat=0 or in sparse binary format
	 * dataFormat=1
	 */
	int dataFormat;

	/**
	* Host pointer to array containing break points of all sparse input feature vectors
	*/
	int* h_inSparseVectorBreakPoints;
	/**
	 * Vector containing original class ids
	 */
	std::vector<string> h_classIdVector;


	/**
	 * Contains item ids
	 */
	std::vector<string> h_itemIdVector;



	/**
	 * Host pointer to array containing k-fold id of all input feature vectors
	 */
	int *h_kfoldMember;

	/**
	 * Set of all class Ids
	 */
	std::set<string> h_classIdSet;
	/**
	 * Maps classId to item refs, where item refs refer to the data items as they are
	 * read in from the source data file.
	 */
	std::map<string, std::vector<int> > h_classIdMap;
	/**
	 * If kfoldCV is being run, map
	 */
	std::map<string, int> h_kfoldMemberMap;
	/**
	 * A vector of all class id names in order of class ref
	 */
	std::vector<string> h_classIdKeySetVector;
	/**
	 * A vector of all class sizes in order of class ref
	 */
	std::vector<int> h_classSizeKeySetVector;


	/**
	 * A map of class names to class reference number
	 */
	std::map<string, int> h_classIdToRefMap;

	/**
	 * Host pointer to array containing all input feature vectors
	 */
	int* h_in;

	/**
	 * Host pointer to array containing class id of all input feature vectors
	 */
	int *h_classMember;

	/**
	 * Host pointer to array containing all normalised feature vectors
	 */
	double *h_inNormalised;

	/**
	 * Host pointer to array listing minimum value for each feature across all feature vectors
	 */
	int *h_inMin;

	/**
	 * Host pointer to array listing maximum value for each feature across all feature vectors
	 */
	int *h_inMax;

	/**
	* Device pointer to array containing all input feature vectors
	*/
	int *d_in;

	/**
	* Opencl device pointer to array containing all input feature vectors
	*/
	cl_mem d_in_ocl;

	/**
	* Device pointer to array containing all break points of sparse input feature vectors
	*/
	int *d_inSparseVectorBreakPoints;

	/**
	* Opencl device pointer to array containing all break points of sparse input feature vectors
	*/
	cl_mem d_inSparseVectorBreakPoints_ocl;

	/**
	* Device pointer to array containing all normalised feature vectors
	*/
	double *d_inNormalised;

	/**
	* Opencl device pointer to array containing all normalised feature vectors
	*/
	cl_mem d_inNormalised_ocl;


	/**
	* Device pointer to array containing class id of all input feature vectors
	*/
	int *d_classMember;

	/**
	* Opencl device pointer to array containing class id of all input feature vectors
	*/
	cl_mem d_classMember_ocl;

	/**
	* Device pointer to array containing k-fold id of all input feature vectors
	*/
	int *d_kfoldMember;

	/**
	* Opencl device pointer to array containing k-fold id of all input feature vectors
	*/
	cl_mem d_kfoldMember_ocl;

	/**
	 * Host pointer to array listing the number of times each input vector is compared to
	* feature vectors in other classes.  This array is used to scale the results arrays
	* generated.
	 */
	int *h_numOfComparesByClass;

	/**
	* Device pointer to array listing the number of times each input vector is compared to
	* feature vectors in other classes.  This array is used to scale the results arrays
	* generated.
	*/
	int *d_numOfComparesByClass;

	/**
	* Opencl device pointer to array listing the number of times each input vector is compared to
	* feature vectors in other classes.  This array is used to scale the results arrays
	* generated.
	*/
	cl_mem d_numOfComparesByClass_ocl;


	/**
	 * Host pointer to array listing the a count of the features each feature vector has in
	 * common with feature vectors in other classes.
	 */
	int *h_common_features_nb;

	/**
	* Device pointer to array listing the a count of the features each feature vector has in
	* common with feature vectors in other classes.
	*/
	int *d_common_features_nb;

	/**
	* Device pointer to array listing the a count of the features each feature vector has in
	* common with feature vectors in other classes.
	*/
	cl_mem d_common_features_nb_ocl;

	/**
	 * Host pointer to array storing the number of 1's for each class for each feature
	 */
	int *h_feature_count_array;

	/**
	 * Device pointer to array storing the number of 1's for each class for each feature
	 */
	int *d_feature_count_array;

	/**
	 * Opencl device pointer to array storing the number of 1's for each class for each feature
	 */
	cl_mem d_feature_count_array_ocl;

	/**
	 * Size of array showing minimum or maximum values of each feature
	 */
	int MIN_MAX_ARRAY_BYTES;
	/**
	 * Length of the array holding all input vectors
	 */
	int IN_ARRAY_SIZE;
	/**
	 * Length of the array holding all input vectors
	 */
	int IN_ARRAY_SIZE_TEST;
	/**
	* Size of the array holding all input vectors
	*/
	int IN_ARRAY_BYTES;
	/**
	* Size of the array holding all input vectors
	*/
	int IN_ARRAY_BYTES_TEST;

	/**
	 * Size of array with normalised input vectors
	 */
	int IN_ARRAY_NORMALISED_BYTES;
	/**
	 * Size of array with normalised input vectors
	 */
	int IN_ARRAY_NORMALISED_BYTES_TEST;

	/**
	 * Length of array showing probability of each data point being a member of each class
	 */
	int CLASS_PROB_ARRAY_BEST_SIZE;
	/**
	 * Size of array showing probability of each data point being a member of each class
	 */
	int CLASS_PROB_ARRAY_BEST_BYTES;
	/**
	 * Size of array listing class of each data point
	 */
	int CLASS_ARRAY_BYTES;
	/**
	 * Size of array listing break points of sparse vectors
	 */
	int SPARSE_ARRAY_BYTES;
	/**
	 * Length of array showing the number of times each data point is compared to data points in other
	 * classes
	 */
	int NUM_COMPARES_ARRAY_SIZE;
	/**
	 * Size of array showing the number of times each data point is compared to data points in other
	 * classes
	 */
	int NUM_COMPARES_ARRAY_BYTES;

	/**
	 * Length of array counting the number of 1's in each feature for each class - useful for a fixed training set
	 */
	int FEATURE_COUNT_ARRAY_BY_CLASS_SIZE;
	/**
	 * Size of array counting the number of 1's in each feature for each class - useful for a fixed training set
	 */
	int FEATURE_COUNT_ARRAY_BY_CLASS_BYTES;

	/**
	 * Explict path to a file showing the kfold split, if a predefined split is to be used
	 */
	string kfoldSplitPath;

	void print_h_in();
	void print_h_inNormalised();
	void print_h_numOfComparesByClass();
	void print_h_feature_count_array();
	void setConstants();
	void readData(string path);

	/**
	 * Read k-fold split data if a predefined k-fold split is being used
	 */
	void readKfoldDataMember();

	/**
	 * Generate k-fold split data is a new split is being generated
	 */
	void generateKfoldMember(int);


	void createkfoldMember(int, int, bool);

	void printKfoldMember(string);

	void generateMinAndMaxFeatureArrays();

	void generateClassMemberData();

	void populate_h_in();
	void populate_h_inSparseVectorBreakPoints();

	void analyseDataLine(string&);

	DataStructure();
	virtual ~DataStructure();
};

#endif /* DATASTRUCTURE_HPP_ */
