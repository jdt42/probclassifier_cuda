/*
 * FeatureComboGenerator.h
 *
 *  Created on: 28 Jun 2013
 *      Author: jdt42
 */

#include "DataHolder.hpp"

#ifndef FEATURECOMBOGENERATOR_H_
#define FEATURECOMBOGENERATOR_H_

class FeatureComboGenerator {
public:

	/**
	 * Dataholder stores data required for classifers
	 */
	DataHolder& dataHolder;
	/**
	 * Generates combimations from feature counts
	 */
	void generateCombosFromFeatureCounts();

	/**
	 * Generates the random combinations for a given subsample length q
	 */
	void generateCombos();
	/**
	 * Calculates n choose r to determine the maximum number of combinations for a given vector
	 * length and subsample length.  This is used to determine whether to randomly generate subsamples
	 * or fully enumerate all combinations if this is feasible
	 */
	int nCr(int, int);
	/**
	 * Prints the combinations to an output file
	 */
	void printCombos();

	FeatureComboGenerator(DataHolder&);
	virtual ~FeatureComboGenerator();
};

#endif /* FEATURECOMBOGENERATOR_H_ */
